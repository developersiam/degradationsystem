﻿using DegradationSystemBLL;
using DegradationSystemWinApp.MVVM;
using DomainModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace DegradationSystemWinApp.ViewModel.Staff
{
    public class vm_EditTestTime : ObservableObject, IRequestFocus
    {
        public event EventHandler<FocusRequestedEventArgs> FocusRequested;
        protected virtual void OnFocusRequested(string propertyName)
        {
            FocusRequested?.Invoke(this, new FocusRequestedEventArgs(propertyName));
        }

        public vm_EditTestTime()
        {
            _timeMode = "HH";
            RaisePropertyChangedEvent(nameof(TimeMode));
        }


        #region Properties
        private SieveDegradation _testCase;

        public SieveDegradation TestCase
        {
            get { return _testCase; }
            set
            {
                _testCase = value;

                _hh = _testCase.Ttime.Hours;
                _mm = _testCase.Ttime.Minutes;

                RaisePropertyChangedEvent(nameof(TestCase));
                RaisePropertyChangedEvent(nameof(HH));
                RaisePropertyChangedEvent(nameof(MM));
            }
        }

        private int _hh;

        public int HH
        {
            get { return _hh; }
            set { _hh = value; }
        }

        private int _mm;

        public int MM
        {
            get { return _mm; }
            set { _mm = value; }
        }

        private string _timeMode;

        public string TimeMode
        {
            get { return _timeMode; }
            set { _timeMode = value; }
        }
        #endregion



        #region Command
        private ICommand _onPlusCommand;

        public ICommand OnPlusCommand
        {
            get { return _onPlusCommand ?? (_onPlusCommand = new RelayCommand(OnPlus)); }
            set { _onPlusCommand = value; }
        }

        private void OnPlus(object obj)
        {
            switch (_timeMode)
            {
                case "H":
                    if (_hh >= 23)
                        _hh = 00;
                    else
                        _hh = _hh + 1;
                    RaisePropertyChangedEvent(nameof(HH));
                    break;
                case "M":
                    if (_mm >= 50)
                        _mm = 0;
                    else
                        _mm = _mm + 10;
                    RaisePropertyChangedEvent(nameof(MM));
                    break;
                default:
                    break;
            }
        }

        private ICommand _onMinusCommand;

        public ICommand OnMinusCommand
        {
            get { return _onMinusCommand ?? (_onMinusCommand = new RelayCommand(OnMinus)); }
            set { _onMinusCommand = value; }
        }

        private void OnMinus(object obj)
        {
            switch (_timeMode)
            {
                case "H":
                    if (_hh <= 0)
                        _hh = 23;
                    else
                        _hh = _hh - 1;
                    RaisePropertyChangedEvent(nameof(HH));
                    break;
                case "M":
                    if (_mm <= 0)
                        _mm = 50;
                    else
                        _mm = _mm - 10;
                    RaisePropertyChangedEvent(nameof(MM));
                    break;
                default:
                    break;
            }
        }

        private ICommand _onHourClickCommand;

        public ICommand OnHourClickCommand
        {
            get { return _onHourClickCommand ?? (_onHourClickCommand = new RelayCommand(OnHourClick)); }
            set { _onHourClickCommand = value; }
        }

        private void OnHourClick(object obj)
        {
            _timeMode = "H";
            RaisePropertyChangedEvent(nameof(TimeMode));
        }

        private ICommand _onMinClickCommand;

        public ICommand OnMinClickCommand
        {
            get { return _onMinClickCommand ?? (_onHourClickCommand = new RelayCommand(OnMinClick)); }
            set { _onMinClickCommand = value; }
        }

        private void OnMinClick(object obj)
        {
            _timeMode = "M";
            RaisePropertyChangedEvent(nameof(TimeMode));
        }

        private ICommand _onSaveCommand;

        public ICommand OnSaveCommand
        {
            get { return _onSaveCommand ?? (_onSaveCommand = new RelayCommand(Save)); }
            set { _onSaveCommand = value; }
        }

        private void Save(object obj)
        {
            try
            {
                TimeSpan ttime = TimeSpan.Parse(_hh + ":" + _mm);
                BLService.DegradationBL()
                    .ChangeTestTime(_testCase.Pdno, _testCase.CaseNo, ttime, user_setting.User.Username);

                MessageBoxHelper.Info("บันทึกสำเร็จ");

                _testCase = BLService.DegradationBL()
                    .GetByCaseNo(_testCase.Pdno, _testCase.CaseNo);
                RaisePropertyChangedEvent(nameof(TestCase));
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }
        #endregion
    }
}
