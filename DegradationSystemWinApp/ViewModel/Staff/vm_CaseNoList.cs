﻿using DegradationSystemBLL;
using DegradationSystemWinApp.MVVM;
using DegradationSystemWinApp.View.Staff;
using DomainModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace DegradationSystemWinApp.ViewModel.Staff
{
    public class vm_CaseNoList : ObservableObject, IRequestFocus
    {
        public event EventHandler<FocusRequestedEventArgs> FocusRequested;
        protected virtual void OnFocusRequested(string propertyName)
        {
            FocusRequested?.Invoke(this, new FocusRequestedEventArgs(propertyName));
        }

        public vm_CaseNoList()
        {
            _pdList = BLService.pdBL().GetByPdSetupDef();
            _totalRecord = _pdList.Count();

            RaisePropertyChangedEvent(nameof(PdList));
            RaisePropertyChangedEvent(nameof(TotalRecord));
        }



        #region Properties
        private string _pdno;

        public string Pdno
        {
            get { return _pdno; }
            set
            {
                _pdno = value;
                pdListBinding();
            }
        }

        private int _totalRecord;

        public int TotalRecord
        {
            get { return _totalRecord; }
            set { _totalRecord = value; }
        }

        private CaseNoList _window;

        public CaseNoList Window
        {
            get { return _window; }
            set { _window = value; }
        }

        private pd _pd;

        public pd Pd
        {
            get { return _pd; }
            set { _pd = value; }
        }
        #endregion



        #region List
        private List<pd> _pdList;

        public List<pd> PdList
        {
            get { return _pdList; }
            set { _pdList = value; }
        }
        #endregion



        #region Command
        private ICommand _onSelectedCommand;

        public ICommand OnSelectedCommand
        {
            get { return _onSelectedCommand ?? (_onSelectedCommand = new RelayCommand(OnSelected)); }
            set { _onSelectedCommand = value; }
        }

        private void OnSelected(object obj)
        {
            try
            {
                if (obj == null)
                    throw new ArgumentException("ไม่พบ case นี้ในระบบ โปรดแจ้งแผนกไอทีเพื่อตรวจสอบ");

                _pd = (pd)obj;

                if (_pd.frompdno != _pdno)
                    throw new ArgumentException("หมายเลข pdno ที่เลือก ไม่ตรงกับของ test case ก่อนหน้านี้");

                RaisePropertyChangedEvent(nameof(Pd));
                _window.Close();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }
        #endregion


        #region Function
        private void pdListBinding()
        {
            _pdList = BLService.pdBL().GetByPdno(_pdno);
            RaisePropertyChangedEvent(nameof(PdList));
        }
        #endregion
    }
}
