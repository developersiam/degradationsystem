﻿using DegradationSystemWinApp.MVVM;
using DegradationSystemWinApp.View.Staff;
using DomainModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace DegradationSystemWinApp.ViewModel.Staff
{
    public class vm_EditMenu : ObservableObject, IRequestFocus
    {
        public event EventHandler<FocusRequestedEventArgs> FocusRequested;
        protected virtual void OnFocusRequested(string propertyName)
        {
            FocusRequested?.Invoke(this, new FocusRequestedEventArgs(propertyName));
        }

        public vm_EditMenu()
        {

        }


        #region Properties
        private SieveDegradation _testCase;

        public SieveDegradation TestCase
        {
            get { return _testCase; }
            set { _testCase = value; }
        }

        #endregion


        #region Command
        private ICommand _onEditSizeCommand;

        public ICommand OnEditSizeCommand
        {
            get { return _onEditSizeCommand ?? (_onEditSizeCommand = new RelayCommand(EditSize)); }
            set { _onEditSizeCommand = value; }
        }

        private void EditSize(object obj)
        {
            if (_testCase == null)
                return;

            var vm = new vm_EditSizeContent();
            var window = new EditSizeContent();

            window.DataContext = vm;
            vm.TestCase = _testCase;
            window.ShowDialog();

            _testCase = vm.TestCase;
            RaisePropertyChangedEvent(nameof(TestCase));
        }

        private ICommand _onEditTimeCommand;

        public ICommand OnEditTimeCommand
        {
            get { return _onEditTimeCommand ?? (_onEditTimeCommand = new RelayCommand(EditTime)); }
            set { _onEditTimeCommand = value; }
        }

        private void EditTime(object obj)
        {
            if (_testCase == null)
                return;

            var vm = new vm_EditTestTime();
            var window = new EditTestTime();

            window.DataContext = vm;
            vm.TestCase = _testCase;
            window.ShowDialog();

            _testCase = vm.TestCase;
            RaisePropertyChangedEvent(nameof(TestCase));
        }

        private ICommand _onEditCaseNoCommand;

        public ICommand OnEditCaseNoCommand
        {
            get { return _onEditCaseNoCommand ?? (_onEditCaseNoCommand = new RelayCommand(EditCaseNo)); }
            set { _onEditCaseNoCommand = value; }
        }

        private void EditCaseNo(object obj)
        {
            if (_testCase == null)
                return;

            var vm = new vm_EditCaseNo();
            var window = new EditCaseNo();

            window.DataContext = vm;
            vm.TestCase = _testCase;
            window.ShowDialog();

            _testCase = vm.TestCase;
            RaisePropertyChangedEvent(nameof(TestCase));
        }
        #endregion
    }
}
