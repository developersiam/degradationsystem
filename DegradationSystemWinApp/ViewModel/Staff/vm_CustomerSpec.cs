﻿using DegradationSystemBLL;
using DegradationSystemWinApp.MVVM;
using DomainModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace DegradationSystemWinApp.ViewModel.Staff
{
    public struct ExcelType
    {
        public short TypeCode { get; set; }
    }

    public class vm_CustomerSpec : ObservableObject, IRequestFocus
    {
        public event EventHandler<FocusRequestedEventArgs> FocusRequested;
        protected virtual void OnFocusRequested(string propertyName)
        {
            FocusRequested?.Invoke(this, new FocusRequestedEventArgs(propertyName));
        }


        public vm_CustomerSpec()
        {
            _crop = Convert.ToInt16(DateTime.Now.Year);
            RaisePropertyChangedEvent(nameof(Crop));
            CustomerListBinding();

            var list = new List<ExcelType>();
            list.Add(new ExcelType { TypeCode = 1 });
            list.Add(new ExcelType { TypeCode = 2 });

            _excelTypeList = list;
            RaisePropertyChangedEvent(nameof(ExcelTypeList));

            _cusSpec = new SieveCustomerSpec();
            RaisePropertyChangedEvent(nameof(CusSpec));
        }



        #region Properties

        private SieveCustomerSpec _cusSpec;

        public SieveCustomerSpec CusSpec
        {
            get { return _cusSpec; }
            set { _cusSpec = value; }
        }

        #endregion



        #region Customer Spec Properties

        private short _crop;

        public short Crop
        {
            get { return _crop; }
            set
            {
                _crop = value;
                CustomerListBinding();
            }
        }

        private string _customerCode;

        public string CustomerCode
        {
            get { return _customerCode; }
            set
            {
                _customerCode = value;
                PackedGradeListBinding();
            }
        }

        private string _packedGrade;

        public string PackedGrade
        {
            get { return _packedGrade; }
            set
            {
                _packedGrade = value;
                CusSpecBinding();
            }
        }

        private short _excelType;

        public short ExcelType
        {
            get { return _excelType; }
            set { _excelType = value; }
        }

        private string _core;

        public string Core
        {
            get { return _core; }
            set { _core = value; }
        }

        private string _over1Target;

        public string Over1Target
        {
            get { return _over1Target; }
            set { _over1Target = value; }
        }

        private string _over1Min;

        public string Over1Min
        {
            get { return _over1Min; }
            set { _over1Min = value; }
        }

        private string _over1Max;

        public string Over1Max
        {
            get { return _over1Max; }
            set { _over1Max = value; }
        }

        private string _over12Target;

        public string Over12Target
        {
            get { return _over12Target; }
            set { _over12Target = value; }
        }

        private string _over12Min;

        public string Over12Min
        {
            get { return _over12Min; }
            set { _over12Min = value; }
        }

        private string _over12Max;

        public string Over12Max
        {
            get { return _over12Max; }
            set { _over12Max = value; }
        }

        private string _total12Target;

        public string Total12Target
        {
            get { return _total12Target; }
            set { _total12Target = value; }
        }

        private string _total12Min;

        public string Total12Min
        {
            get { return _total12Min; }
            set { _total12Min = value; }
        }

        private string _total12Max;

        public string Total12Max
        {
            get { return _total12Max; }
            set { _total12Max = value; }
        }

        private string _over14Target;

        public string Over14Target
        {
            get { return _over14Target; }
            set { _over14Target = value; }
        }

        private string _over14Min;

        public string Over14Min
        {
            get { return _over14Min; }
            set { _over14Min = value; }
        }

        private string _over14Max;

        public string Over14Max
        {
            get { return _over14Max; }
            set { _over14Max = value; }
        }

        private string _total14Target;

        public string Total14Target
        {
            get { return _total14Target; }
            set { _total14Target = value; }
        }

        private string _total14Min;

        public string Total14Min
        {
            get { return _total14Min; }
            set { _total14Min = value; }
        }

        private string _total14Max;

        public string Total14Max
        {
            get { return _total14Max; }
            set { _total14Max = value; }
        }

        private string _over18Target;

        public string Over18Target
        {
            get { return _over18Target; }
            set { _over18Target = value; }
        }

        private string _over18Min;

        public string Over18Min
        {
            get { return _over18Min; }
            set { _over18Min = value; }
        }

        private string _over18Max;

        public string Over18Max
        {
            get { return _over18Max; }
            set { _over18Max = value; }
        }

        private string _panTarget;

        public string PanTarget
        {
            get { return _panTarget; }
            set { _panTarget = value; }
        }

        private string _panMin;

        public string PanMin
        {
            get { return _panMin; }
            set { _panMin = value; }
        }

        private string _panMax;

        public string PanMax
        {
            get { return _panMax; }
            set { _panMax = value; }
        }

        private string _thru14Target;

        public string Thru14Target
        {
            get { return _thru14Target; }
            set { _thru14Target = value; }
        }

        private string _thru14Min;

        public string Thru14Min
        {
            get { return _thru14Min; }
            set { _thru14Min = value; }
        }

        private string _thru14Max;

        public string Thru14Max
        {
            get { return _thru14Max; }
            set { _thru14Max = value; }
        }

        private string _stemOBJTarget;

        public string StemOBJTarget
        {
            get { return _stemOBJTarget; }
            set { _stemOBJTarget = value; }
        }

        private string _stemOBJMin;

        public string StemOBJMin
        {
            get { return _stemOBJMin; }
            set { _stemOBJMin = value; }
        }

        private string _stemOBJMax;

        public string StemOBJMax
        {
            get { return _stemOBJMax; }
            set { _stemOBJMax = value; }
        }

        private string _stem7SlotTarget;

        public string Stem7SlotTarget
        {
            get { return _stem7SlotTarget; }
            set { _stem7SlotTarget = value; }
        }

        private string _stem7SlotMin;

        public string Stem7SlotMin
        {
            get { return _stem7SlotMin; }
            set { _stem7SlotMin = value; }
        }

        private string _stem7SlotMax;

        public string Stem7SlotMax
        {
            get { return _stem7SlotMax; }
            set { _stem7SlotMax = value; }
        }

        private string _stem12SlotTarget;

        public string Stem12SlotTarget
        {
            get { return _stem12SlotTarget; }
            set { _stem12SlotTarget = value; }
        }

        private string _stem12SlotMin;

        public string Stem12SlotMin
        {
            get { return _stem12SlotMin; }
            set { _stem12SlotMin = value; }
        }

        private string _stem12SlotMax;

        public string Stem12SlotMax
        {
            get { return _stem12SlotMax; }
            set { _stem12SlotMax = value; }
        }

        private string _fiberTarget;

        public string FiberTarget
        {
            get { return _fiberTarget; }
            set { _fiberTarget = value; }
        }

        private string _fiberMin;

        public string FiberMin
        {
            get { return _fiberMin; }
            set { _fiberMin = value; }
        }

        private string _fiberMax;

        public string FiberMax
        {
            get { return _fiberMax; }
            set { _fiberMax = value; }
        }

        private string _totalStemTarget;

        public string TotalStemTarget
        {
            get { return _totalStemTarget; }
            set { _totalStemTarget = value; }
        }

        private string _totalStemMin;

        public string TotalStemMin
        {
            get { return _totalStemMin; }
            set { _totalStemMin = value; }
        }

        private string _totalStemMax;

        public string TotalStemMax
        {
            get { return _totalStemMax; }
            set { _totalStemMax = value; }
        }
        #endregion



        #region List
        private List<customer> _customerList;

        public List<customer> CustomerList
        {
            get { return _customerList; }
            set { _customerList = value; }
        }

        private List<packedgrade> _packedGradeList;

        public List<packedgrade> PackedGradeList
        {
            get { return _packedGradeList; }
            set { _packedGradeList = value; }
        }

        private List<packedgrade> _cropPackedGradeList;

        public List<packedgrade> CropPackedGradeList
        {
            get { return _cropPackedGradeList; }
            set { _cropPackedGradeList = value; }
        }

        private List<ExcelType> _excelTypeList;

        public List<ExcelType> ExcelTypeList
        {
            get { return _excelTypeList; }
            set { _excelTypeList = value; }
        }
        #endregion



        #region Command
        private ICommand _onSaveCommand;

        public ICommand OnSaveCommand
        {
            get { return _onSaveCommand ?? (_onSaveCommand = new RelayCommand(OnSave)); }
            set { _onSaveCommand = value; }
        }

        private void OnSave(object obj)
        {
            try
            {
                if (string.IsNullOrEmpty(_customerCode))
                    throw new ArgumentException("โปรดระบุ customer");

                if (string.IsNullOrEmpty(_packedGrade))
                    throw new ArgumentException("โปรดระบุ packedgrade");

                if (_excelType <= 0)
                    throw new ArgumentException("โปรดระบุ excel type");

                /// Binding properties to model.
                /// 
                var model = new SieveCustomerSpec
                {
                    Crop = _crop,
                    Customer = _customerCode,
                    PackGrade = _packedGrade,
                    TargetCor = _core,
                    TargetOver1_Target = _over1Target,
                    TargetOver1_min = _over1Min,
                    TargetOver1_max = _over1Max,
                    TargetOver1_2_Target = _over12Target,
                    TargetOver1_2_min = _over12Min,
                    TargetOver1_2_max = _over12Max,
                    TargetTOT1_2_Target = _total12Target,
                    TargetTOT1_2_min = _total12Min,
                    TargetTOT1_2_max = _total12Max,
                    TargetOver1_4_Target = _over14Target,
                    TargetOver1_4_min = _over14Min,
                    TargetOver1_4_max = _over14Max,
                    TargetTOT1_4_Target = _total14Target,
                    TargetTOT1_4_min = _total14Min,
                    TargetTOT1_4_max = _total14Max,
                    TargetOver1_8_Target = _over18Target,
                    TargetOver1_8_min = _over18Min,
                    TargetOver1_8_max = _over18Max,
                    TargetPan_Target = _panTarget,
                    TargetPan_min = _panMin,
                    TargetPan_max = _panMax,
                    TargetThru1_4_Target = _thru14Target,
                    TargetThru1_4_min = _thru14Min,
                    TargetThru1_4_max = _thru14Max,
                    TargetStemOBJ_Target = _stemOBJTarget,
                    TargetStemOBJ_min = _stemOBJMin,
                    TargetStemOBJ_max = _stemOBJMax,
                    TargetStem_7_Target = _stem7SlotTarget,
                    TargetStem_7_min = _stem7SlotMin,
                    TargetStem_7_max = _stem7SlotMax,
                    TargetStem_12_Target = _stem12SlotTarget,
                    TargetStem_12_min = _stem12SlotMin,
                    TargetStem_12_max = _stem12SlotMax,
                    TargetStem_Fiber_Target = _fiberTarget,
                    TargetStem_Fiber_min = _fiberMin,
                    TargetStem_Fiber_max = _fiberMax,
                    TargetTOTStem_Target = _totalStemTarget,
                    TargetTOTStem_min = _totalStemMin,
                    TargetTOTStem_max = _totalStemMax,
                    TypeOfExcel = _excelType,
                    SieveUser = user_setting.User.Username,
                    ModifiedDate = DateTime.Now
                };

                ///Get a customer spec from database for adding or editting.
                ///
                var cusSpecModel = BLService.CustomerSpecBL()
                    .GetByPackGrade(_crop, _customerCode, _packedGrade);

                if (cusSpecModel == null)
                    BLService.CustomerSpecBL().Add(model);
                else
                    BLService.CustomerSpecBL().Edit(model);

                MessageBoxHelper.Info("บันทึกสำเร็จ");
                CusSpecBinding();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private ICommand _onDeleteCommand;

        public ICommand OnDeleteCommand
        {
            get { return _onDeleteCommand ?? (_onDeleteCommand = new RelayCommand(OnDelete)); }
            set { _onDeleteCommand = value; }
        }

        private void OnDelete(object obj)
        {
            try
            {
                if (MessageBoxHelper.Question("ท่านต้องการลบข้อมูลนี้ใช่หรือไม่?") == MessageBoxResult.No)
                    return;

                BLService.CustomerSpecBL().Delete(_crop, _customerCode, _packedGrade);
                MessageBoxHelper.Info("ลบข้อมูลสำเร็จ");

                CusSpecBinding();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private ICommand _onRefreshCommand;

        public ICommand OnRefreshCommand
        {
            get { return _onRefreshCommand ?? (_onRefreshCommand = new RelayCommand(OnRefresh)); }
            set { _onRefreshCommand = value; }
        }

        private void OnRefresh(object obj)
        {
            try
            {
                CusSpecBinding();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        #endregion



        #region Funtion
        private void CusSpecBinding()
        {
            try
            {
                _cusSpec = BLService.CustomerSpecBL()
                    .GetByPackGrade(_crop, _customerCode, _packedGrade);

                if (_cusSpec == null)
                {
                    Clear();
                    return;
                }

                _core = _cusSpec.TargetCor;
                _excelType = Convert.ToInt16(_cusSpec.TypeOfExcel);
                _over1Target = _cusSpec.TargetOver1_Target;
                _over1Min = _cusSpec.TargetOver1_min;
                _over1Max = _cusSpec.TargetOver1_max;
                _over12Target = _cusSpec.TargetOver1_2_Target;
                _over12Min = _cusSpec.TargetOver1_2_min;
                _over12Max = _cusSpec.TargetOver1_2_max;
                _over14Target = _cusSpec.TargetOver1_4_Target;
                _over14Min = _cusSpec.TargetOver1_4_min;
                _over14Max = _cusSpec.TargetOver1_4_max;
                _over18Target = _cusSpec.TargetOver1_8_Target;
                _over18Min = _cusSpec.TargetOver1_8_min;
                _over18Max = _cusSpec.TargetOver1_8_max;
                _total12Target = _cusSpec.TargetTOT1_2_Target;
                _total12Min = _cusSpec.TargetTOT1_2_min;
                _total12Max = _cusSpec.TargetTOT1_2_max;
                _total14Target = _cusSpec.TargetTOT1_4_Target;
                _total14Min = _cusSpec.TargetTOT1_4_min;
                _total14Max = _cusSpec.TargetTOT1_4_max;
                _panTarget = _cusSpec.TargetPan_Target;
                _panMin = _cusSpec.TargetPan_min;
                _panMax = _cusSpec.TargetPan_max;
                _stemOBJTarget = _cusSpec.TargetStemOBJ_Target;
                _stemOBJMin = _cusSpec.TargetStemOBJ_min;
                _stemOBJMax = _cusSpec.TargetStemOBJ_max;
                _stem7SlotTarget = _cusSpec.TargetStem_7_Target;
                _stem7SlotMin = _cusSpec.TargetStem_7_min;
                _stem7SlotMax = _cusSpec.TargetStem_7_max;
                _stem12SlotTarget = _cusSpec.TargetStem_12_Target;
                _stem12SlotMin = _cusSpec.TargetStem_12_min;
                _stem12SlotMax = _cusSpec.TargetStem_12_max;
                _fiberTarget = _cusSpec.TargetStem_Fiber_Target;
                _fiberMin = _cusSpec.TargetStem_Fiber_min;
                _fiberMax = _cusSpec.TargetStem_Fiber_max;
                _totalStemTarget = _cusSpec.TargetTOTStem_Target;
                _totalStemMin = _cusSpec.TargetTOTStem_min;
                _totalStemMax = _cusSpec.TargetTOTStem_max;

                RaisePropertyChangedEvent(nameof(Core));

                RaisePropertyChangedEvent(nameof(Over1Target));
                RaisePropertyChangedEvent(nameof(Over1Min));
                RaisePropertyChangedEvent(nameof(Over1Max));

                RaisePropertyChangedEvent(nameof(Over12Target));
                RaisePropertyChangedEvent(nameof(Over12Min));
                RaisePropertyChangedEvent(nameof(Over12Max));

                RaisePropertyChangedEvent(nameof(Over14Target));
                RaisePropertyChangedEvent(nameof(Over14Min));
                RaisePropertyChangedEvent(nameof(Over14Max));

                RaisePropertyChangedEvent(nameof(Over18Target));
                RaisePropertyChangedEvent(nameof(Over18Min));
                RaisePropertyChangedEvent(nameof(Over18Max));

                RaisePropertyChangedEvent(nameof(Total12Target));
                RaisePropertyChangedEvent(nameof(Total12Min));
                RaisePropertyChangedEvent(nameof(Total12Max));

                RaisePropertyChangedEvent(nameof(Total14Target));
                RaisePropertyChangedEvent(nameof(Total14Min));
                RaisePropertyChangedEvent(nameof(Total14Max));

                RaisePropertyChangedEvent(nameof(Thru14Target));
                RaisePropertyChangedEvent(nameof(Thru14Min));
                RaisePropertyChangedEvent(nameof(Thru14Max));

                RaisePropertyChangedEvent(nameof(PanTarget));
                RaisePropertyChangedEvent(nameof(PanMin));
                RaisePropertyChangedEvent(nameof(PanMax));

                RaisePropertyChangedEvent(nameof(StemOBJTarget));
                RaisePropertyChangedEvent(nameof(StemOBJMin));
                RaisePropertyChangedEvent(nameof(StemOBJMax));

                RaisePropertyChangedEvent(nameof(Stem7SlotTarget));
                RaisePropertyChangedEvent(nameof(Stem7SlotMin));
                RaisePropertyChangedEvent(nameof(Stem7SlotMax));

                RaisePropertyChangedEvent(nameof(Stem12SlotTarget));
                RaisePropertyChangedEvent(nameof(Stem12SlotMin));
                RaisePropertyChangedEvent(nameof(Stem12SlotMax));

                RaisePropertyChangedEvent(nameof(FiberTarget));
                RaisePropertyChangedEvent(nameof(FiberMin));
                RaisePropertyChangedEvent(nameof(FiberMax));

                RaisePropertyChangedEvent(nameof(TotalStemTarget));
                RaisePropertyChangedEvent(nameof(TotalStemMin));
                RaisePropertyChangedEvent(nameof(TotalStemMax));

                RaisePropertyChangedEvent(nameof(ExcelType));
                RaisePropertyChangedEvent(nameof(CusSpec));
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void CustomerListBinding()
        {
            try
            {
                _cropPackedGradeList = BLService.packedGradeBL().GetByCrop(_crop);
                _customerList = _cropPackedGradeList
                    .GroupBy(x => x.customer)
                    .Select(x => new customer { code = x.Key })
                    .OrderBy(x => x.code)
                    .ToList();

                RaisePropertyChangedEvent(nameof(CustomerList));
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void PackedGradeListBinding()
        {
            try
            {
                _packedGradeList = _cropPackedGradeList
                    .Where(x => x.customer == _customerCode)
                    .OrderBy(x => x.packedgrade1)
                    .ToList();

                RaisePropertyChangedEvent(nameof(PackedGradeList));
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void Clear()
        {
            _cusSpec = null;

            _core = "";
            _excelType = 1;
            _over1Target = "";
            _over1Min = "";
            _over1Max = "";
            _over12Target = "";
            _over12Min = "";
            _over12Max = "";
            _over14Target = "";
            _over14Min = "";
            _over14Max = "";
            _over18Target = "";
            _over18Min = "";
            _over18Max = "";
            _total12Target = "";
            _total12Min = "";
            _total12Max = "";
            _total14Target = "";
            _total14Min = "";
            _total14Max = "";
            _panTarget = "";
            _panMin = "";
            _panMax = "";
            _stemOBJTarget = "";
            _stemOBJMin = "";
            _stemOBJMax = "";
            _stem7SlotTarget = "";
            _stem7SlotMin = "";
            _stem7SlotMax = "";
            _stem12SlotTarget = "";
            _stem12SlotMin = "";
            _stem12SlotMax = "";
            _fiberTarget = "";
            _fiberMin = "";
            _fiberMax = "";
            _totalStemTarget = "";
            _totalStemMin = "";
            _totalStemMax = "";

            RaisePropertyChangedEvent(nameof(Core));

            RaisePropertyChangedEvent(nameof(Over1Target));
            RaisePropertyChangedEvent(nameof(Over1Min));
            RaisePropertyChangedEvent(nameof(Over1Max));

            RaisePropertyChangedEvent(nameof(Over12Target));
            RaisePropertyChangedEvent(nameof(Over12Min));
            RaisePropertyChangedEvent(nameof(Over12Max));

            RaisePropertyChangedEvent(nameof(Over14Target));
            RaisePropertyChangedEvent(nameof(Over14Min));
            RaisePropertyChangedEvent(nameof(Over14Max));

            RaisePropertyChangedEvent(nameof(Over18Target));
            RaisePropertyChangedEvent(nameof(Over18Min));
            RaisePropertyChangedEvent(nameof(Over18Max));

            RaisePropertyChangedEvent(nameof(Total12Target));
            RaisePropertyChangedEvent(nameof(Total12Min));
            RaisePropertyChangedEvent(nameof(Total12Max));

            RaisePropertyChangedEvent(nameof(Total14Target));
            RaisePropertyChangedEvent(nameof(Total14Min));
            RaisePropertyChangedEvent(nameof(Total14Max));

            RaisePropertyChangedEvent(nameof(Thru14Target));
            RaisePropertyChangedEvent(nameof(Thru14Min));
            RaisePropertyChangedEvent(nameof(Thru14Max));

            RaisePropertyChangedEvent(nameof(PanTarget));
            RaisePropertyChangedEvent(nameof(PanMin));
            RaisePropertyChangedEvent(nameof(PanMax));

            RaisePropertyChangedEvent(nameof(StemOBJTarget));
            RaisePropertyChangedEvent(nameof(StemOBJMin));
            RaisePropertyChangedEvent(nameof(StemOBJMax));

            RaisePropertyChangedEvent(nameof(Stem7SlotTarget));
            RaisePropertyChangedEvent(nameof(Stem7SlotMin));
            RaisePropertyChangedEvent(nameof(Stem7SlotMax));

            RaisePropertyChangedEvent(nameof(Stem12SlotTarget));
            RaisePropertyChangedEvent(nameof(Stem12SlotMin));
            RaisePropertyChangedEvent(nameof(Stem12SlotMax));

            RaisePropertyChangedEvent(nameof(FiberTarget));
            RaisePropertyChangedEvent(nameof(FiberMin));
            RaisePropertyChangedEvent(nameof(FiberMax));

            RaisePropertyChangedEvent(nameof(TotalStemTarget));
            RaisePropertyChangedEvent(nameof(TotalStemMin));
            RaisePropertyChangedEvent(nameof(TotalStemMax));

            RaisePropertyChangedEvent(nameof(ExcelType));
            RaisePropertyChangedEvent(nameof(CusSpec));
        }
        #endregion
    }
}
