﻿using DegradationSystemBLL;
using DegradationSystemWinApp.MVVM;
using DomainModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace DegradationSystemWinApp.ViewModel.Staff
{
    public class vm_EditSizeContent : ObservableObject, IRequestFocus
    {
        public event EventHandler<FocusRequestedEventArgs> FocusRequested;
        protected virtual void OnFocusRequested(string propertyName)
        {
            FocusRequested?.Invoke(this, new FocusRequestedEventArgs(propertyName));
        }

        public vm_EditSizeContent()
        {

        }


        #region Properties
        private SieveDegradation _testCase;

        public SieveDegradation TestCase
        {
            get { return _testCase; }
            set
            {
                _testCase = value;

                _over1 = Convert.ToInt16(_testCase.Over1);
                _over12 = Convert.ToInt16(_testCase.Over1_2);
                _over14 = Convert.ToInt16(_testCase.Over1_4);
                _over18 = Convert.ToInt16(_testCase.Over1_8);
                _pan = Convert.ToInt16(_testCase.Pan);

                RaisePropertyChangedEvent(nameof(TestCase));
                RaisePropertyChangedEvent(nameof(Over1));
                RaisePropertyChangedEvent(nameof(Over12));
                RaisePropertyChangedEvent(nameof(Over14));
                RaisePropertyChangedEvent(nameof(Over18));
                RaisePropertyChangedEvent(nameof(Pan));
            }
        }

        private int _sizeMode;

        public int SizeMode
        {
            get { return _sizeMode; }
            set { _sizeMode = value; }
        }

        private int _weight;

        public int Weight
        {
            get { return _weight; }
            set { _weight = value; }
        }

        private int _over1;

        public int Over1
        {
            get { return _over1; }
            set { _over1 = value; }
        }

        private int _over12;

        public int Over12
        {
            get { return _over12; }
            set { _over12 = value; }
        }

        private int _over14;

        public int Over14
        {
            get { return _over14; }
            set { _over14 = value; }
        }

        private int _over18;

        public int Over18
        {
            get { return _over18; }
            set { _over18 = value; }
        }

        private int _pan;

        public int Pan
        {
            get { return _pan; }
            set { _pan = value; }
        }
        #endregion


        #region Command
        private ICommand _onOver1Command;

        public ICommand OnOver1Command
        {
            get { return _onOver1Command ?? (_onOver1Command = new RelayCommand(OnOver1)); }
            set { _onOver1Command = value; }
        }

        private void OnOver1(object obj)
        {
            try
            {
                _sizeMode = 1;
                RaisePropertyChangedEvent(nameof(SizeMode));
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private ICommand _onOver12Command;

        public ICommand OnOver12Command
        {
            get { return _onOver12Command ?? (_onOver12Command = new RelayCommand(OnOver12)); }
            set { _onOver12Command = value; }
        }

        private void OnOver12(object obj)
        {
            try
            {
                _sizeMode = 2;
                RaisePropertyChangedEvent(nameof(SizeMode));
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private ICommand _onOver14Command;

        public ICommand OnOver14Command
        {
            get { return _onOver14Command ?? (_onOver14Command = new RelayCommand(OnOver14)); }
            set { _onOver14Command = value; }
        }

        private void OnOver14(object obj)
        {
            try
            {
                _sizeMode = 3;
                RaisePropertyChangedEvent(nameof(SizeMode));
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private ICommand _onOver18Command;

        public ICommand OnOver18Command
        {
            get { return _onOver18Command ?? (_onOver18Command = new RelayCommand(OnOver18)); }
            set { _onOver18Command = value; }
        }

        private void OnOver18(object obj)
        {
            try
            {
                _sizeMode = 4;
                RaisePropertyChangedEvent(nameof(SizeMode));
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private ICommand _onPanCommand;

        public ICommand OnPanCommand
        {
            get { return _onPanCommand ?? (_onPanCommand = new RelayCommand(OnPan)); }
            set { _onPanCommand = value; }
        }

        private void OnPan(object obj)
        {
            try
            {
                _sizeMode = 5;
                RaisePropertyChangedEvent(nameof(SizeMode));
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private ICommand _onSaveCommand;

        public ICommand OnSaveCommand
        {
            get { return _onSaveCommand ?? (_onSaveCommand = new RelayCommand(OnSave)); }
            set { _onSaveCommand = value; }
        }

        private void OnSave(object obj)
        {
            try
            {
                if (_testCase == null)
                    throw new ArgumentException("ไม่พบ Test Case ที่ต้องการบันทึกข้อมูล");

                BLService.DegradationBL()
                    .ChangeSizeContent(_testCase.Pdno,
                    _testCase.CaseNo,
                    _over1,
                    _over12,
                    _over14,
                    _over18,
                    _pan,
                    user_setting.User.Username);

                MessageBoxHelper.Info("บันทึกสำเร็จ");

                _over1 = 0;
                _over12 = 0;
                _over14 = 0;
                _over18 = 0;
                _pan = 0;
                _sizeMode = 1;
                _weight = 0;

                RaisePropertyChangedEvent(nameof(pd));
                RaisePropertyChangedEvent(nameof(Over1));
                RaisePropertyChangedEvent(nameof(Over12));
                RaisePropertyChangedEvent(nameof(Over14));
                RaisePropertyChangedEvent(nameof(Over18));
                RaisePropertyChangedEvent(nameof(Pan));
                RaisePropertyChangedEvent(nameof(SizeMode));
                RaisePropertyChangedEvent(nameof(Weight));
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private ICommand _onOkayCommand;

        public ICommand OnOkayCommand
        {
            get { return _onOkayCommand ?? (_onOkayCommand = new RelayCommand(OnOkay)); }
            set { _onOkayCommand = value; }
        }

        private void OnOkay(object obj)
        {
            try
            {
                if (_weight <= 0)
                    throw new ArgumentException("น้ำหนักจะต้องมากกว่า 0");

                switch (_sizeMode)
                {
                    case 1:
                        _over1 = _weight;
                        _sizeMode = 2;
                        RaisePropertyChangedEvent(nameof(Over1));
                        RaisePropertyChangedEvent(nameof(SizeMode));
                        break;
                    case 2:
                        _over12 = _weight;
                        _sizeMode = 3;
                        RaisePropertyChangedEvent(nameof(Over12));
                        RaisePropertyChangedEvent(nameof(SizeMode));
                        break;
                    case 3:
                        _over14 = _weight;
                        _sizeMode = 4;
                        RaisePropertyChangedEvent(nameof(Over14));
                        RaisePropertyChangedEvent(nameof(SizeMode));
                        break;
                    case 4:
                        _over18 = _weight;
                        _sizeMode = 5;
                        RaisePropertyChangedEvent(nameof(Over18));
                        RaisePropertyChangedEvent(nameof(SizeMode));
                        break;
                    case 5:
                        _pan = _weight;
                        _sizeMode = 6;
                        RaisePropertyChangedEvent(nameof(Pan));
                        RaisePropertyChangedEvent(nameof(SizeMode));
                        break;
                    default:
                        _sizeMode = 1;
                        RaisePropertyChangedEvent(nameof(SizeMode));
                        break;
                }

                if (_sizeMode == 6)
                    OnFocusRequested(nameof(SizeMode));
                else
                    OnFocusRequested(nameof(Weight));
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }
        #endregion


        #region Function

        #endregion
    }
}
