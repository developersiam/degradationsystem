﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DegradationSystemWinApp.MVVM;
using DegradationSystemBLL;
using DegradationSystemWinApp.View.Staff;
using DomainModel;
using System.Windows.Input;
using System.Windows;
using DegradationSystemBLL.Model;
using DegradationSystemBLL.Helper;

namespace DegradationSystemWinApp.ViewModel.Staff
{
    public class vm_NewGrade : ObservableObject, IRequestFocus
    {
        public event EventHandler<FocusRequestedEventArgs> FocusRequested;
        protected virtual void OnFocusRequested(string propertyName)
        {
            FocusRequested?.Invoke(this, new FocusRequestedEventArgs(propertyName));
        }

        public vm_NewGrade()
        {
            if (user_setting.User.Role == "Size" || user_setting.User.Role == "Stem")
                _pdsetupList = pdsetupHelper.GetByCurrentDateOrDefault()
                    .OrderByDescending(x=>x.pdno)
                    .ToList();
            else
                _pdsetupList = pdsetupHelper
                    .GetByCrop(Convert.ToInt16(DateTime.Now.Year))
                    .OrderByDescending(x => x.pdno)
                    .ToList();
            _totalRecord = _pdsetupList.Count();

            RaisePropertyChangedEvent(nameof(PdSetupList));
            RaisePropertyChangedEvent(nameof(TotalRecord));
        }


        #region Properties
        private int _totalRecord;

        public int TotalRecord
        {
            get { return _totalRecord; }
            set { _totalRecord = value; }
        }

        private NewGrade _window;

        public NewGrade Window
        {
            get { return _window; }
            set { _window = value; }
        }

        private SieveCustomerSpec _customerSpec;

        public SieveCustomerSpec CustomerSpec
        {
            get { return _customerSpec; }
            set { _customerSpec = value; }
        }

        private m_pdsetup _pdsetup;

        public m_pdsetup PdSetup
        {
            get { return _pdsetup; }
            set { _pdsetup = value; }
        }

        private packedgrade _packedgrade;

        public packedgrade PackedGrade
        {
            get { return _packedgrade; }
            set { _packedgrade = value; }
        }

        #endregion


        #region List
        private List<m_pdsetup> _pdsetupList;

        public List<m_pdsetup> PdSetupList
        {
            get { return _pdsetupList; }
            set { _pdsetupList = value; }
        }
        #endregion


        #region Command
        private ICommand _onSelectedCommand;

        public ICommand OnSelectedCommand
        {
            get { return _onSelectedCommand ?? (_onSelectedCommand = new RelayCommand(OnSelected)); }
            set { _onSelectedCommand = value; }
        }

        private void OnSelected(object obj)
        {
            try
            {
                if (obj == null)
                    throw new ArgumentException("ไม่พบ pdno นี้ในระบบ โปรดแจ้งแผนกไอทีเพื่อตรวจสอบ");

                _pdsetup = (m_pdsetup)obj;
                _packedgrade = BLService.packedGradeBL().GetSingle(_pdsetup.packedgrade);

                if (_packedgrade == null)
                    throw new ArgumentException("ไม่พบ packedgrade นี้ในระบบ");

                _customerSpec = BLService.CustomerSpecBL()
                    .GetByPackGrade(Convert.ToInt16(_packedgrade.crop), _packedgrade.customer, _packedgrade.packedgrade1);

                if (_customerSpec == null)
                    throw new ArgumentException("ไม่พบ spec ลูกค้านี้ในระบบ โปรดแจ้งหัวหน้างานเพื่อเพิ่มข้อมูล");

                RaisePropertyChangedEvent(nameof(PdSetup));
                RaisePropertyChangedEvent(nameof(PackedGrade));
                RaisePropertyChangedEvent(nameof(CustomerSpec));

                _window.Close();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }
        #endregion
    }
}
