﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DegradationSystemWinApp.MVVM;
using DomainModel;
using System.IO.Ports;
using System.Windows.Input;
using DegradationSystemBLL;
using DegradationSystemWinApp.View.Staff;
using System.Threading;

namespace DegradationSystemWinApp.ViewModel.Staff
{
    public class vm_SizeContent : ObservableObject, IRequestFocus
    {
        public event EventHandler<FocusRequestedEventArgs> FocusRequested;
        protected virtual void OnFocusRequested(string propertyName)
        {
            FocusRequested?.Invoke(this, new FocusRequestedEventArgs(propertyName));
        }

        public vm_SizeContent()
        {
            _timeMode = "H";
            _hh = 0;
            _mm = 0;
            _isReadOnly = true;
            _isConnectScale = false;

            RaisePropertyChangedEvent(nameof(TimeMode));
            RaisePropertyChangedEvent(nameof(HH));
            RaisePropertyChangedEvent(nameof(MM));
            RaisePropertyChangedEvent(nameof(IsReadOnly));
            RaisePropertyChangedEvent(nameof(IsConnectScale));

            DigitalScaleConnect();
        }


        #region Properties
        private pd _pd;

        public pd pd
        {
            get { return _pd; }
            set { _pd = value; }
        }

        private pdsetup _pdsetup;

        public pdsetup PdSetup
        {
            get { return _pdsetup; }
            set { _pdsetup = value; }
        }

        private packedgrade _packedgrade;

        public packedgrade PackedGrade
        {
            get { return _packedgrade; }
            set { _packedgrade = value; }
        }

        private SieveCustomerSpec _customerSpec;

        public SieveCustomerSpec CustomerSpec
        {
            get { return _customerSpec; }
            set { _customerSpec = value; }
        }

        private short _caseno;

        public short CaseNo
        {
            get { return _caseno; }
            set { _caseno = value; }
        }

        private int _hh;

        public int HH
        {
            get { return _hh; }
            set { _hh = value; }
        }

        private int _mm;

        public int MM
        {
            get { return _mm; }
            set { _mm = value; }
        }

        private string _timeMode;

        public string TimeMode
        {
            get { return _timeMode; }
            set { _timeMode = value; }
        }

        private int _sizeMode;

        public int SizeMode
        {
            get { return _sizeMode; }
            set { _sizeMode = value; }
        }

        private int _weight;

        public int Weight
        {
            get { return _weight; }
            set { _weight = value; }
        }

        private int _over1;

        public int Over1
        {
            get { return _over1; }
            set { _over1 = value; }
        }

        private int _over12;

        public int Over12
        {
            get { return _over12; }
            set { _over12 = value; }
        }

        private int _over14;

        public int Over14
        {
            get { return _over14; }
            set { _over14 = value; }
        }

        private int _over18;

        public int Over18
        {
            get { return _over18; }
            set { _over18 = value; }
        }

        private int _pan;

        public int Pan
        {
            get { return _pan; }
            set { _pan = value; }
        }

        private int _lastCaseNo;

        public int LastCaseNo
        {
            get { return _lastCaseNo; }
            set { _lastCaseNo = value; }
        }

        private TimeSpan _lastTestTime;

        public TimeSpan LastTestTime
        {
            get { return _lastTestTime; }
            set { _lastTestTime = value; }
        }

        private DateTime _lastPackingDate;

        public DateTime LastPackingDate
        {
            get { return _lastPackingDate; }
            set { _lastPackingDate = value; }
        }

        private string _lastPdno;

        public string LastPdno
        {
            get { return _lastPdno; }
            set { _lastPdno = value; }
        }

        private int _lastOver1;

        public int LastOver1
        {
            get { return _lastOver1; }
            set { _lastOver1 = value; }
        }

        private int _lastOver12;

        public int LastOver12
        {
            get { return _lastOver12; }
            set { _lastOver12 = value; }
        }

        private int _lastOver14;

        public int LastOver14
        {
            get { return _lastOver14; }
            set { _lastOver14 = value; }
        }

        private int _lastOver18;

        public int LastOver18
        {
            get { return _lastOver18; }
            set { _lastOver18 = value; }
        }

        private int _lastPan;

        public int LastPan
        {
            get { return _lastPan; }
            set { _lastPan = value; }
        }

        private DateTime _lastSendTime;

        public DateTime LastSendTime
        {
            get { return _lastSendTime; }
            set { _lastSendTime = value; }
        }

        private bool _isConnectScale;

        public bool IsConnectScale
        {
            get { return _isConnectScale; }
            set
            {
                if (user_setting.User.Role == "Manager" || user_setting.User.Role == "Admin")
                {
                    try
                    {
                        if (value == true)
                        {
                            DigitalScaleConnect();
                            _isConnectScale = true;
                            _isReadOnly = true;
                            RaisePropertyChangedEvent(nameof(IsConnectScale));
                            RaisePropertyChangedEvent(nameof(IsReadOnly));
                        }
                        else
                        {
                            Thread CloseDown = new Thread(new ThreadStart(CloseSerialOnExit));
                            CloseDown.Start();

                            _isConnectScale = false;
                            _isReadOnly = false;
                            RaisePropertyChangedEvent(nameof(IsConnectScale));
                            RaisePropertyChangedEvent(nameof(IsReadOnly));
                            OnFocusRequested(nameof(Weight));
                        }
                    }
                    catch (Exception ex)
                    {
                        MessageBoxHelper.Exception(ex);
                    }
                }
                else
                {
                    MessageBoxHelper.Warning("ผู้ที่ยกเลิกการเชื่อมต่อกับเครื่องชั่งได้ จะต้องเป็นผู้ใช้กลุ่ม Manager เท่านั้น");
                    return;
                }
            }
        }

        private bool _isReadOnly;

        public bool IsReadOnly
        {
            get { return _isReadOnly; }
            set { _isReadOnly = value; }
        }

        private bool _isTestCaseChecked;

        public bool IsTestCaseChecked
        {
            get { return _isTestCaseChecked; }
            set { _isTestCaseChecked = value; }
        }

        #endregion



        #region List

        #endregion



        #region Command
        private ICommand _onPlusCommand;

        public ICommand OnPlusCommand
        {
            get { return _onPlusCommand ?? (_onPlusCommand = new RelayCommand(OnPlus)); }
            set { _onPlusCommand = value; }
        }

        private void OnPlus(object obj)
        {
            switch (_timeMode)
            {
                case "H":
                    if (_hh >= 23)
                        _hh = 00;
                    else
                        _hh = _hh + 1;
                    RaisePropertyChangedEvent(nameof(HH));
                    break;
                case "M":
                    if (_mm >= 50)
                        _mm = 0;
                    else
                        _mm = _mm + 10;
                    RaisePropertyChangedEvent(nameof(MM));
                    break;
                default:
                    break;
            }
        }

        private ICommand _onMinusCommand;

        public ICommand OnMinusCommand
        {
            get { return _onMinusCommand ?? (_onMinusCommand = new RelayCommand(OnMinus)); }
            set { _onMinusCommand = value; }
        }

        private void OnMinus(object obj)
        {
            switch (_timeMode)
            {
                case "H":
                    if (_hh <= 0)
                        _hh = 23;
                    else
                        _hh = _hh - 1;
                    RaisePropertyChangedEvent(nameof(HH));
                    break;
                case "M":
                    if (_mm <= 0)
                        _mm = 50;
                    else
                        _mm = _mm - 10;
                    RaisePropertyChangedEvent(nameof(MM));
                    break;
                default:
                    break;
            }
        }

        private ICommand _onHourClickCommand;

        public ICommand OnHourClickCommand
        {
            get { return _onHourClickCommand ?? (_onHourClickCommand = new RelayCommand(OnHourClick)); }
            set { _onHourClickCommand = value; }
        }

        private void OnHourClick(object obj)
        {
            _timeMode = "H";
            RaisePropertyChangedEvent(nameof(TimeMode));
        }

        private ICommand _onMinClickCommand;

        public ICommand OnMinClickCommand
        {
            get { return _onMinClickCommand ?? (_onHourClickCommand = new RelayCommand(OnMinClick)); }
            set { _onMinClickCommand = value; }
        }

        private void OnMinClick(object obj)
        {
            _timeMode = "M";
            RaisePropertyChangedEvent(nameof(TimeMode));
        }

        private ICommand _onNewGradeCommand;

        public ICommand OnNewGradeCommand
        {
            get { return _onNewGradeCommand ?? (_onNewGradeCommand = new RelayCommand(OnNewGrade)); }
            set { _onNewGradeCommand = value; }
        }

        private void OnNewGrade(object obj)
        {
            try
            {
                var vm = new vm_NewGrade();
                var window = new NewGrade();
                window.DataContext = vm;
                vm.Window = window;
                window.ShowDialog();

                if (vm.CustomerSpec == null)
                    throw new ArgumentException("ไม่พบ packedgrade ที่เลือก กรุณาลองเลือกใหม่อีกครั้ง");

                _pdsetup = vm.PdSetup;
                _packedgrade = vm.PackedGrade;
                _customerSpec = vm.CustomerSpec;
                _caseno = 0;

                RaisePropertyChangedEvent(nameof(PdSetup));
                RaisePropertyChangedEvent(nameof(PackedGrade));
                RaisePropertyChangedEvent(nameof(CustomerSpec));
                RaisePropertyChangedEvent(nameof(CaseNo));
                OnFocusRequested(nameof(CaseNo));
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private ICommand _onCaseNoEnterCommand;

        public ICommand OnCaseNoEnterCommand
        {
            get { return _onCaseNoEnterCommand ?? (_onCaseNoEnterCommand = new RelayCommand(OnCaseNoEnter)); }
            set { _onCaseNoEnterCommand = value; }
        }

        private void OnCaseNoEnter(object obj)
        {
            TestCaseBinding();
        }

        private ICommand _onOver1Command;

        public ICommand OnOver1Command
        {
            get { return _onOver1Command ?? (_onOver1Command = new RelayCommand(OnOver1)); }
            set { _onOver1Command = value; }
        }

        private void OnOver1(object obj)
        {
            try
            {
                HasCaseNo();
                _sizeMode = 1;
                RaisePropertyChangedEvent(nameof(SizeMode));
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private ICommand _onOver12Command;

        public ICommand OnOver12Command
        {
            get { return _onOver12Command ?? (_onOver12Command = new RelayCommand(OnOver12)); }
            set { _onOver12Command = value; }
        }

        private void OnOver12(object obj)
        {
            try
            {
                HasCaseNo();
                _sizeMode = 2;
                RaisePropertyChangedEvent(nameof(SizeMode));
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private ICommand _onOver14Command;

        public ICommand OnOver14Command
        {
            get { return _onOver14Command ?? (_onOver14Command = new RelayCommand(OnOver14)); }
            set { _onOver14Command = value; }
        }

        private void OnOver14(object obj)
        {
            try
            {
                HasCaseNo();
                _sizeMode = 3;
                RaisePropertyChangedEvent(nameof(SizeMode));
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private ICommand _onOver18Command;

        public ICommand OnOver18Command
        {
            get { return _onOver18Command ?? (_onOver18Command = new RelayCommand(OnOver18)); }
            set { _onOver18Command = value; }
        }

        private void OnOver18(object obj)
        {
            try
            {
                HasCaseNo();
                _sizeMode = 4;
                RaisePropertyChangedEvent(nameof(SizeMode));
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private ICommand _onPanCommand;

        public ICommand OnPanCommand
        {
            get { return _onPanCommand ?? (_onPanCommand = new RelayCommand(OnPan)); }
            set { _onPanCommand = value; }
        }

        private void OnPan(object obj)
        {
            try
            {
                HasCaseNo();
                _sizeMode = 5;
                RaisePropertyChangedEvent(nameof(SizeMode));
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private ICommand _onSendCommand;

        public ICommand OnSendCommand
        {
            get { return _onSendCommand ?? (_onSendCommand = new RelayCommand(OnSend)); }
            set { _onSendCommand = value; }
        }

        private void OnSend(object obj)
        {
            try
            {
                if (_pdsetup == null)
                    throw new ArgumentException("โปรดเลือก packedgrade โดยกดปุ่ม New Grade");

                if (_caseno <= 0)
                    throw new ArgumentException("caseno จะต้องมากกว่า 0");

                _pd = BLService.pdBL().GetByCaseNo(_pdsetup.pdno, _caseno);
                if (_pd == null)
                    throw new ArgumentException("ไม่พบข้อมูลกล่องยาหมายเลข " + _caseno +
                        " ในระบบ ท้ายเครื่องอาจยังไม่ได้บันทึกข้อมูล โปรดตรวจสอบอีกครั้ง");

                var testCase = BLService.DegradationBL()
                    .GetByCaseNo(_pdsetup.pdno, _caseno);

                if (testCase != null)
                    throw new ArgumentException("มีการบันทึกข้อมูล caseno #" + _caseno + " นี้แล้วในระบบ");

                if (_over1 <= 0 || _over12 <= 0 || _over14 <= 0 || _over18 <= 0)
                    throw new ArgumentException("โปรดใส่ข้อมูลให้ครบถ้วนก่อนกด Send");

                TimeSpan ttime = TimeSpan.Parse(_hh + ":" + _mm);
                BLService.DegradationBL().InputSizeContent(pd.frompdno,
                    Convert.ToInt16(pd.caseno),
                    Convert.ToInt16(pd.crop),
                    pd.customer,
                    pd.grade,
                    ttime,
                    DateTime.Now,
                    _over1,
                    _over12,
                    _over14,
                    _over18,
                    _pan,
                    user_setting.User.Username);

                _lastCaseNo = _caseno;
                _lastOver1 = _over1;
                _lastOver12 = _over12;
                _lastOver14 = _over14;
                _lastOver18 = _over18;
                _lastPan = _pan;
                _lastPackingDate = Convert.ToDateTime(_pd.packingdate);
                _lastPdno = _pdsetup.pdno;
                _lastSendTime = DateTime.Now;
                _lastTestTime = Convert.ToDateTime(DateTime.Now).TimeOfDay;

                MessageBoxHelper.Info("บันทึกสำเร็จ");

                RaisePropertyChangedEvent(nameof(LastCaseNo));
                RaisePropertyChangedEvent(nameof(LastOver1));
                RaisePropertyChangedEvent(nameof(LastOver12));
                RaisePropertyChangedEvent(nameof(LastOver14));
                RaisePropertyChangedEvent(nameof(LastOver18));
                RaisePropertyChangedEvent(nameof(LastPan));
                RaisePropertyChangedEvent(nameof(LastPackingDate));
                RaisePropertyChangedEvent(nameof(LastPdno));
                RaisePropertyChangedEvent(nameof(LastSendTime));
                RaisePropertyChangedEvent(nameof(LastTestTime));

                _pd = null;
                _over1 = 0;
                _over12 = 0;
                _over14 = 0;
                _over18 = 0;
                _pan = 0;
                _sizeMode = 1;
                _weight = 0;

                RaisePropertyChangedEvent(nameof(pd));
                RaisePropertyChangedEvent(nameof(Over1));
                RaisePropertyChangedEvent(nameof(Over12));
                RaisePropertyChangedEvent(nameof(Over14));
                RaisePropertyChangedEvent(nameof(Over18));
                RaisePropertyChangedEvent(nameof(Pan));
                RaisePropertyChangedEvent(nameof(SizeMode));
                RaisePropertyChangedEvent(nameof(Weight));

                OnFocusRequested(nameof(CaseNo));
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private ICommand _onOkayCommand;

        public ICommand OnOkayCommand
        {
            get { return _onOkayCommand ?? (_onOkayCommand = new RelayCommand(OnOkay)); }
            set { _onOkayCommand = value; }
        }

        private void OnOkay(object obj)
        {
            try
            {
                if (_weight <= 0)
                    throw new ArgumentException("น้ำหนักจะต้องมากกว่า 0");

                switch (_sizeMode)
                {
                    case 1:
                        _over1 = _weight;
                        _sizeMode = 2;
                        RaisePropertyChangedEvent(nameof(Over1));
                        RaisePropertyChangedEvent(nameof(SizeMode));
                        break;
                    case 2:
                        _over12 = _weight;
                        _sizeMode = 3;
                        RaisePropertyChangedEvent(nameof(Over12));
                        RaisePropertyChangedEvent(nameof(SizeMode));
                        break;
                    case 3:
                        _over14 = _weight;
                        _sizeMode = 4;
                        RaisePropertyChangedEvent(nameof(Over14));
                        RaisePropertyChangedEvent(nameof(SizeMode));
                        break;
                    case 4:
                        _over18 = _weight;
                        _sizeMode = 5;
                        RaisePropertyChangedEvent(nameof(Over18));
                        RaisePropertyChangedEvent(nameof(SizeMode));
                        break;
                    case 5:
                        _pan = _weight;
                        _sizeMode = 6;
                        RaisePropertyChangedEvent(nameof(Pan));
                        RaisePropertyChangedEvent(nameof(SizeMode));
                        break;
                    default:
                        _sizeMode = 1;
                        RaisePropertyChangedEvent(nameof(SizeMode));
                        break;
                }

                if (_sizeMode == 6)
                    OnFocusRequested(nameof(SizeMode));
                else
                    OnFocusRequested(nameof(Weight));
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private ICommand _onWindowClosedCommand;

        public ICommand OnWindowClosedCommand
        {
            get { return _onWindowClosedCommand ?? (_onWindowClosedCommand = new RelayCommand(OnWindowClose)); }
            set { _onWindowClosedCommand = value; }
        }

        private void OnWindowClose(object obj)
        {
            CloseSerialOnExit();
        }
        #endregion



        #region Funtion
        private void DigitalScaleConnect()
        {
            try
            {
                DigitalScaleHelper.Setup();
                DigitalScaleHelper.serialPort.Open();
                DigitalScaleHelper.serialPort.DataReceived += port_DataReceived;
                _isConnectScale = true;
                RaisePropertyChangedEvent(nameof(IsConnectScale));
            }
            catch (Exception ex)
            {
                if (user_setting.User.Role == "Admin" || user_setting.User.Role == "Manager")
                {
                    _isConnectScale = false;
                    _isReadOnly = false;

                    RaisePropertyChangedEvent(nameof(IsConnectScale));
                    RaisePropertyChangedEvent(nameof(IsReadOnly));
                }
                MessageBoxHelper.Exception(ex);
            }
        }

        private delegate void preventCrossThreading(string str);
        //private preventCrossThreading accessControlFromCentralThread;

        private void displayTextReadIn(string str)
        {
            try
            {
                decimal value;
                if (Decimal.TryParse(str, out value))
                    _weight = Convert.ToInt16(Convert.ToDouble(str) * 1000);
                else
                    _weight = 0;

                RaisePropertyChangedEvent(nameof(Weight));
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void port_DataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            try
            {
                displayTextReadIn(DigitalScaleHelper.GetWeightFromSizeScale());
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void CloseSerialOnExit()
        {
            try
            {
                DigitalScaleHelper.serialPort.DataReceived -= port_DataReceived;
                DigitalScaleHelper.serialPort.Close();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void HasCaseNo()
        {
            if (_caseno <= 0)
                throw new ArgumentException("โปรดระบุ caseno");
        }

        private void TestCaseBinding()
        {
            try
            {
                _isTestCaseChecked = false;
                RaisePropertyChangedEvent(nameof(IsTestCaseChecked));

                if (_pdsetup == null)
                    throw new ArgumentException("โปรดเลือก packedgrade โดยกดปุ่ม New Grade");

                if (_caseno <= 0)
                    throw new ArgumentException("caseno จะต้องมากกว่า 0");

                _pd = BLService.pdBL().GetByCaseNo(_pdsetup.pdno, _caseno);
                if (_pd == null)
                    throw new ArgumentException("ไม่พบข้อมูลกล่องยาหมายเลข " + _caseno +
                        " ในระบบ ท้ายเครื่องอาจยังไม่ได้บันทึกข้อมูล โปรดตรวจสอบอีกครั้ง");

                var testCase = BLService.DegradationBL()
                    .GetByCaseNo(_pdsetup.pdno, _caseno);

                if (testCase != null)
                    throw new ArgumentException("มีการบันทึกข้อมูล caseno #" + _caseno + " นี้แล้วในระบบ");

                _sizeMode = 1;
                _isTestCaseChecked = true;
                RaisePropertyChangedEvent(nameof(SizeMode));
                RaisePropertyChangedEvent(nameof(IsTestCaseChecked));
                OnFocusRequested(nameof(Weight));
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
                OnFocusRequested(nameof(CaseNo));
            }
        }
        #endregion
    }
}
