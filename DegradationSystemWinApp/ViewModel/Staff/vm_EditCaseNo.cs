﻿using DegradationSystemBLL;
using DegradationSystemWinApp.MVVM;
using DegradationSystemWinApp.View.Staff;
using DomainModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace DegradationSystemWinApp.ViewModel.Staff
{
    public class vm_EditCaseNo : ObservableObject, IRequestFocus
    {
        public event EventHandler<FocusRequestedEventArgs> FocusRequested;
        protected virtual void OnFocusRequested(string propertyName)
        {
            FocusRequested?.Invoke(this, new FocusRequestedEventArgs(propertyName));
        }


        public vm_EditCaseNo()
        {

        }


        #region Properties
        private SieveDegradation _testCase;

        public SieveDegradation TestCase
        {
            get { return _testCase; }
            set
            {
                _testCase = value;
                _caseNo = _testCase.CaseNo;
                RaisePropertyChangedEvent(nameof(CaseNo));
            }
        }

        private short _caseNo;

        public short CaseNo
        {
            get { return _caseNo; }
            set { _caseNo = value; }
        }
        #endregion


        #region Command
        private ICommand _onCaseNoGotFocusCommand;

        public ICommand OnCaseNoGotFocusCommand
        {
            get { return _onCaseNoGotFocusCommand ?? (_onCaseNoGotFocusCommand = new RelayCommand(GotFocus)); }
            set { _onCaseNoGotFocusCommand = value; }
        }

        private void GotFocus(object obj)
        {
            try
            {
                var vm = new vm_CaseNoList();
                var window = new CaseNoList();

                window.DataContext = vm;
                vm.Pdno = _testCase.Pdno;
                vm.Window = window;
                window.ShowDialog();

                if (vm.Pd == null)
                {
                    OnFocusRequested(nameof(TestCase));
                    return;
                }                    

                _caseNo = (short)vm.Pd.caseno;
                RaisePropertyChangedEvent(nameof(CaseNo));
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private ICommand _onSaveCommand;

        public ICommand OnSaveCommand
        {
            get { return _onSaveCommand ?? (_onSaveCommand = new RelayCommand(Save)); }
            set { _onSaveCommand = value; }
        }

        private void Save(object obj)
        {
            try
            {
                if (_caseNo <= 0)
                    throw new ArgumentException("Case no จะต้องเป็นได้ตั้งแต่ 1 ขึ้นไป");

                BLService.DegradationBL().ChangeCaseNo(_testCase.Pdno,
                    _testCase.CaseNo,
                    Convert.ToInt16(_caseNo),
                    user_setting.User.Username);

                MessageBoxHelper.Info("บันทึกสำเร็จ");
                TestCaseBinding();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }
        #endregion


        #region Function
        private void TestCaseBinding()
        {
            _testCase = BLService.DegradationBL().GetByCaseNo(_testCase.Pdno, _caseNo);
            RaisePropertyChangedEvent(nameof(TestCase));
        }
        #endregion
    }
}
