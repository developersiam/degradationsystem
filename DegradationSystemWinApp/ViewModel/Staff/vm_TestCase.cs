﻿using DegradationSystemBLL;
using DegradationSystemWinApp.Helper;
using DegradationSystemWinApp.MVVM;
using DegradationSystemWinApp.View.Report;
using DegradationSystemWinApp.View.Staff;
using DomainModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace DegradationSystemWinApp.ViewModel.Staff
{
    public class vm_TestCase : ObservableObject, IRequestFocus
    {
        public event EventHandler<FocusRequestedEventArgs> FocusRequested;
        protected virtual void OnFocusRequested(string propertyName)
        {
            FocusRequested?.Invoke(this, new FocusRequestedEventArgs(propertyName));
        }

        public vm_TestCase()
        {
            _from = DateTime.Now.Date;
            _to = DateTime.Now.Date;

            RaisePropertyChangedEvent(nameof(From));
            RaisePropertyChangedEvent(nameof(To));
            CustomerBinding();
        }

        #region Properties
        private DateTime _from;

        public DateTime From
        {
            get { return _from; }
            set
            {
                _from = value;
                CustomerBinding();
                _testCaseList = null;
                RaisePropertyChangedEvent(nameof(TestCaseList));
            }
        }

        private DateTime _to;

        public DateTime To
        {
            get { return _to; }
            set
            {
                _to = value;
                CustomerBinding();
                _testCaseList = null;
                RaisePropertyChangedEvent(nameof(TestCaseList));
            }
        }

        private string _customerCode;

        public string CustomerCode
        {
            get { return _customerCode; }
            set
            {
                _customerCode = value;
                PackedGradeBinding();
                _testCaseList = null;
                RaisePropertyChangedEvent(nameof(TestCaseList));
            }
        }

        private string _packedGrade;

        public string PackedGrade
        {
            get { return _packedGrade; }
            set
            {
                _packedGrade = value;
                TestCaseListBinding();
            }
        }

        private int _selectedItems;

        public int SelectedItems
        {
            get { return _selectedItems; }
            set { _selectedItems = value; }
        }

        private int _totalRecord;

        public int TotalRecord
        {
            get { return _totalRecord; }
            set { _totalRecord = value; }
        }
        #endregion


        #region List
        private List<SieveDegradation> _testCaseList;

        public List<SieveDegradation> TestCaseList
        {
            get { return _testCaseList; }
            set { _testCaseList = value; }
        }

        private List<customer> _customerList;

        public List<customer> CustomerList
        {
            get { return _customerList; }
            set { _customerList = value; }
        }

        private List<packedgrade> _packedGradeList;

        public List<packedgrade> PackedGradeList
        {
            get { return _packedGradeList; }
            set { _packedGradeList = value; }
        }

        private List<SieveDegradation> _testCaseByDateRange;

        public List<SieveDegradation> TestCaseByDateRange
        {
            get { return _testCaseByDateRange; }
            set { _testCaseByDateRange = value; }
        }

        private List<SieveDegradation> _selectedList;

        public List<SieveDegradation> SelectedList
        {
            get { return _selectedList; }
            set { _selectedList = value; }
        }

        #endregion


        #region Command
        private ICommand _onRefreshCommand;

        public ICommand OnRefreshCommand
        {
            get { return _onRefreshCommand ?? (_onRefreshCommand = new RelayCommand(OnRefresh)); }
            set { _onRefreshCommand = value; }
        }

        private void OnRefresh(object obj)
        {
            TestCaseListBinding();
        }

        private ICommand _onExportCommand;

        public ICommand OnExportCommand
        {
            get { return _onExportCommand ?? (_onExportCommand = new RelayCommand(ExportFile)); }
            set { _onExportCommand = value; }
        }

        private void ExportFile(object obj)
        {
            try
            {
                if (_selectedList == null)
                    throw new ArgumentException("โปรดเลือก test case ที่จะ export ข้อมูลจากตารางด้านล่าง");

                ReportHelper.DegradationReport(_selectedList);
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private ICommand _onViewTargetCommand;

        public ICommand OnViewTargetCommand
        {
            get { return _onViewTargetCommand ?? (_onViewTargetCommand = new RelayCommand(ViewTarget)); ; }
            set { _onViewTargetCommand = value; }
        }

        private void ViewTarget(object obj)
        {
            try
            {
                if (string.IsNullOrEmpty(_customerCode))
                    throw new ArgumentException("โปรดระบุ customer");

                if (string.IsNullOrEmpty(_packedGrade))
                    throw new ArgumentException("โปรดระบุ packedgrade");

                var vm = new vm_CustomerSpec();
                var window = new CustomerSpec();
                window.DataContext = vm;
                vm.Crop = Convert.ToInt16(_from.Year);
                vm.CustomerCode = _customerCode;
                vm.PackedGrade = _packedGrade;
                window.ShowDialog();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private ICommand _onViewReportCommand;

        public ICommand OnViewReportCommand
        {
            get { return _onViewReportCommand ?? (_onViewReportCommand = new RelayCommand(ViewReport)); ; }
            set { _onViewReportCommand = value; }
        }

        private void ViewReport(object obj)
        {
            try
            {
                if (string.IsNullOrEmpty(_customerCode) || string.IsNullOrEmpty(_packedGrade))
                {
                    MessageBoxHelper.Warning("โปรดระบุ customer และ packedgrade ให้ครบถ้วน");
                    return;
                }

                var window = new RPTDEG01();
                window._customerCode = _customerCode;
                window._packedGrade = _packedGrade;
                window._fromDate = _from;
                window._toDate = _to;
                window.ShowDialog();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private ICommand _onDataGridSelectedCommand;

        public ICommand OnDataGridSelectedCommand
        {
            get { return _onDataGridSelectedCommand ?? (_onDataGridSelectedCommand = new RelayCommand(OnDataGridSelected)); }
            set { _onDataGridSelectedCommand = value; }
        }

        private void OnDataGridSelected(object obj)
        {
            try
            {
                System.Collections.IList items = (System.Collections.IList)obj;
                _selectedList = items.Cast<SieveDegradation>().ToList();

                _selectedItems = _selectedList.Count();
                RaisePropertyChangedEvent(nameof(SelectedItems));
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private ICommand _onEditCommand;

        public ICommand OnEditCommand
        {
            get { return _onEditCommand ?? (_onEditCommand = new RelayCommand(Edit)); }
            set { _onEditCommand = value; }
        }

        private void Edit(object obj)
        {
            var item = (SieveDegradation)obj;
            if (item == null)
                return;

            var vm = new vm_EditMenu();
            var window = new EditMenu();

            window.DataContext = vm;
            vm.TestCase = item;
            window.ShowDialog();

            TestCaseListBinding();
        }

        private ICommand _onDeleteCommand;

        public ICommand OnDeleteCommand
        {
            get { return _onDeleteCommand ?? (_onDeleteCommand = new RelayCommand(Delete)); }
            set { _onDeleteCommand = value; }
        }

        private void Delete(object obj)
        {
            if (MessageBoxHelper.Question("ท่านต้องการลบข้อมูลนี้ใช่หรือไม่?")
                == System.Windows.MessageBoxResult.No)
                return;

            var item = (SieveDegradation)obj;
            BLService.DegradationBL().Delete(item.Pdno, item.CaseNo);
            MessageBoxHelper.Info("ลบข้อมูลสำเร็จ");
            TestCaseListBinding();
        }

        private ICommand _onDataGridMouseDoubleClickCommand;

        public ICommand OnDataGridMouseDoubleClickCommand
        {
            get { return _onDataGridMouseDoubleClickCommand ?? (_onDataGridMouseDoubleClickCommand = new RelayCommand(OnMouseDoubleClick)); }
            set { _onDataGridMouseDoubleClickCommand = value; }
        }

        private void OnMouseDoubleClick(object obj)
        {
            try
            {
                var window = new StemContent();
                var vm = new vm_StemContent();
                var item = (SieveDegradation)obj;

                window.DataContext = vm;
                vm.Window = window;
                vm.TestCase = item;
                vm.CusSpec = BLService.CustomerSpecBL()
                    .GetByPackGrade(item.Crop, item.Customer, item.PackGrade);

                window.ShowDialog();
                TestCaseListBinding();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }
        #endregion


        #region Function
        private void TestCaseListBinding()
        {
            try
            {
                if (_from == null ||
                    _to == null ||
                    string.IsNullOrEmpty(_customerCode) ||
                    string.IsNullOrEmpty(_packedGrade))
                    return;

                _testCaseList = BLService.DegradationBL()
                    .GetByPackGrade(_from, _to, _customerCode, _packedGrade);
                _totalRecord = _testCaseList.Count();

                RaisePropertyChangedEvent(nameof(TestCaseList));
                RaisePropertyChangedEvent(nameof(TotalRecord));
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void CustomerBinding()
        {
            try
            {
                if (_from == null || _to == null)
                    return;

                _testCaseByDateRange = BLService.DegradationBL()
                    .GetByDateRange(_from, _to);

                _customerList = _testCaseByDateRange
                    .GroupBy(x => x.Customer)
                    .Select(x => new customer { code = x.Key })
                    .OrderBy(x => x.code)
                    .ToList();

                RaisePropertyChangedEvent(nameof(CustomerList));
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void PackedGradeBinding()
        {
            try
            {
                if (string.IsNullOrEmpty(_customerCode))
                    return;

                _packedGradeList = _testCaseByDateRange
                    .Where(x => x.Customer == _customerCode)
                    .GroupBy(x => x.PackGrade)
                    .Select(x => new packedgrade { packedgrade1 = x.Key })
                    .OrderBy(x => x.packedgrade1)
                    .ToList();

                RaisePropertyChangedEvent(nameof(PackedGradeList));
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }
        #endregion
    }
}
