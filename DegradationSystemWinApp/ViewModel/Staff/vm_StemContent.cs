﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DegradationSystemWinApp.MVVM;
using System.Windows.Input;
using DomainModel;
using System.Threading;
using System.IO.Ports;
using DegradationSystemBLL;
using DegradationSystemWinApp.View.Staff;

namespace DegradationSystemWinApp.ViewModel.Staff
{
    public class vm_StemContent : ObservableObject, IRequestFocus
    {
        public event EventHandler<FocusRequestedEventArgs> FocusRequested;
        protected virtual void OnFocusRequested(string propertyName)
        {
            FocusRequested?.Invoke(this, new FocusRequestedEventArgs(propertyName));
        }

        public StemContent Window { get; set; }

        public vm_StemContent()
        {
            _stemMode = 1;
            _isReadOnly = false;
            _isConnectScale = false;

            RaisePropertyChangedEvent(nameof(StemMode));
            RaisePropertyChangedEvent(nameof(IsReadOnly));
            RaisePropertyChangedEvent(nameof(IsConnectScale));

            OnFocusRequested(nameof(Weight));
        }


        #region Properties
        private decimal _stemOBJ;

        public decimal StemOBJ
        {
            get { return _stemOBJ; }
            set { _stemOBJ = value; }
        }

        private decimal _stem7Slot;

        public decimal Stem7Slot
        {
            get { return _stem7Slot; }
            set { _stem7Slot = value; }
        }

        private decimal _stem12Slot;

        public decimal Stem12Slot
        {
            get { return _stem12Slot; }
            set { _stem12Slot = value; }
        }

        private decimal _stem12Mesh;

        public decimal Stem12Mesh
        {
            get { return _stem12Mesh; }
            set { _stem12Mesh = value; }
        }

        private decimal _stemFiber;

        public decimal StemFiber
        {
            get { return _stemFiber; }
            set { _stemFiber = value; }
        }

        private int _sampleWeight;

        public int SampleWeight
        {
            get { return _sampleWeight; }
            set { _sampleWeight = value; }
        }

        private int _sampleWeightPickedUp;

        public int SampleWeightPickedUp
        {
            get { return _sampleWeightPickedUp; }
            set { _sampleWeightPickedUp = value; }
        }

        private decimal _weight;

        public decimal Weight
        {
            get { return _weight; }
            set { _weight = value; }
        }

        private SieveDegradation _testCase;

        public SieveDegradation TestCase
        {
            get { return _testCase; }
            set
            {
                _testCase = value;
                TestCaseBinding();
            }
        }

        private SieveCustomerSpec _cusSpec;

        public SieveCustomerSpec CusSpec
        {
            get { return _cusSpec; }
            set
            {
                _cusSpec = value;

                if (_cusSpec.TypeOfExcel == 1)
                    _stem12SlotIsEnabled = false;
                else
                    _stem12SlotIsEnabled = true;

                RaisePropertyChangedEvent(nameof(Stem12SlotIsEnabled));
            }
        }

        private bool _stem12SlotIsEnabled;

        public bool Stem12SlotIsEnabled
        {
            get { return _stem12SlotIsEnabled; }
            set { _stem12SlotIsEnabled = value; }
        }

        private int _stemMode;

        public int StemMode
        {
            get { return _stemMode; }
            set { _stemMode = value; }
        }

        private bool _isConnectScale;

        public bool IsConnectScale
        {
            get { return _isConnectScale; }
            set
            {
                if (user_setting.User.Role == "Admin" ||
                    user_setting.User.Role == "Supervisor" ||
                    user_setting.User.Role == "Stem")
                {
                    try
                    {
                        if (value == true)
                        {
                            DigitalScaleConnect();
                            _isConnectScale = true;
                            _isReadOnly = true;
                            RaisePropertyChangedEvent(nameof(IsConnectScale));
                            RaisePropertyChangedEvent(nameof(IsReadOnly));
                        }
                        else
                        {
                            Thread CloseDown = new Thread(new ThreadStart(CloseSerialOnExit));
                            CloseDown.Start();

                            _isConnectScale = false;
                            _isReadOnly = false;
                            RaisePropertyChangedEvent(nameof(IsConnectScale));
                            RaisePropertyChangedEvent(nameof(IsReadOnly));
                            OnFocusRequested(nameof(Weight));
                        }
                    }
                    catch (Exception ex)
                    {
                        MessageBoxHelper.Exception(ex);
                    }
                }
                else
                {
                    MessageBoxHelper.Warning("ผู้ที่ยกเลิกการเชื่อมต่อกับเครื่องชั่งได้ จะต้องเป็นผู้ใช้เฉพาะในกลุ่มที่กำหนดไว้เท่านั้น");
                    return;
                }
            }
        }

        private bool _isReadOnly;

        public bool IsReadOnly
        {
            get { return _isReadOnly; }
            set { _isReadOnly = value; }
        }
        #endregion



        #region Command
        private ICommand _onOBJCommand;

        public ICommand OnOBJCommand
        {
            get { return _onOBJCommand ?? (_onOBJCommand = new RelayCommand(OBJClick)); }
            set { _onOBJCommand = value; }
        }

        private void OBJClick(object obj)
        {
            _stemMode = 1;
            RaisePropertyChangedEvent(nameof(StemMode));
            OnFocusRequested(nameof(Weight));
        }

        private ICommand _on7SlotCommand;

        public ICommand On7SlotCommand
        {
            get { return _on7SlotCommand ?? (_on7SlotCommand = new RelayCommand(Stem7SlotClick)); }
            set { _on7SlotCommand = value; }
        }

        private void Stem7SlotClick(object obj)
        {
            _stemMode = 2;
            RaisePropertyChangedEvent(nameof(StemMode));
            OnFocusRequested(nameof(Weight));
        }

        private ICommand _on12SlotCommand;

        public ICommand On12SlotCommand
        {
            get { return _on12SlotCommand ?? (_on12SlotCommand = new RelayCommand(Stem12SlotClick)); }
            set { _on12SlotCommand = value; }
        }

        private void Stem12SlotClick(object obj)
        {
            _stemMode = 3;
            RaisePropertyChangedEvent(nameof(StemMode));
            OnFocusRequested(nameof(Weight));
        }

        private ICommand _on12MeshCommand;

        public ICommand On12MeshCommand
        {
            get { return _on12MeshCommand ?? (_on12MeshCommand = new RelayCommand(Stem7MeshClick)); }
            set { _on12MeshCommand = value; }
        }

        private void Stem7MeshClick(object obj)
        {
            _stemMode = 4;
            RaisePropertyChangedEvent(nameof(StemMode));
            OnFocusRequested(nameof(Weight));
        }

        private ICommand _onFiberCommand;

        public ICommand OnFiberCommand
        {
            get { return _onFiberCommand ?? (_onFiberCommand = new RelayCommand(Stem12FiberClick)); }
            set { _onFiberCommand = value; }
        }

        private void Stem12FiberClick(object obj)
        {
            _stemMode = 5;
            RaisePropertyChangedEvent(nameof(StemMode));
            OnFocusRequested(nameof(Weight));
        }

        private ICommand _onSaveCommand;

        public ICommand OnSaveCommand
        {
            get { return _onSaveCommand ?? (_onSaveCommand = new RelayCommand(SaveClick)); }
            set { _onSaveCommand = value; }
        }

        private void SaveClick(object obj)
        {
            try
            {
                if (_stemOBJ >= 100)
                    throw new ArgumentException("Stem OBJ ไม่ควนเกิน 100");
                if (_stem7Slot >= 100)
                    throw new ArgumentException("Stem 7 Slot ไม่ควนเกิน 100");
                if (_stem12Slot >= 100)
                    throw new ArgumentException("Stem 12 Slot ไม่ควนเกิน 100");
                if (_stemFiber >= 100)
                    throw new ArgumentException("Stem Fiber ไม่ควนเกิน 100");

                BLService.DegradationBL()
                    .InputStemContent(_testCase.Crop,
                    _testCase.Pdno,
                    _testCase.CaseNo,
                    Convert.ToDouble(_stemOBJ),
                    Convert.ToDouble(_stem7Slot),
                    Convert.ToDouble(_stem12Slot),
                    Convert.ToDouble(_stem12Mesh),
                    Convert.ToDouble(_stemFiber),
                    _sampleWeight,
                    _sampleWeightPickedUp,
                    user_setting.User.Username);

                MessageBoxHelper.Info("บันทึกสำเร็จ");
                Window.Close();
                //_stemMode = 1;
                //TestCaseBinding();

                //RaisePropertyChangedEvent(nameof(StemMode));
                //RaisePropertyChangedEvent(nameof(TestCase));
                //OnFocusRequested(nameof(Weight));
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private ICommand _onWeightEnterCommand;

        public ICommand OnWeightEnterCommand
        {
            get { return _onWeightEnterCommand ?? (_onWeightEnterCommand = new RelayCommand(WeightEnter)); }
            set { _onWeightEnterCommand = value; }
        }

        private void WeightEnter(object obj)
        {
            try
            {
                if (_weight <= 0)
                    throw new ArgumentException("น้ำหนักจะต้องมากกว่า 0");

                switch (_stemMode)
                {
                    case 1:
                        _stemOBJ = _weight;
                        _stemMode = 2;
                        RaisePropertyChangedEvent(nameof(StemOBJ));
                        RaisePropertyChangedEvent(nameof(StemMode));
                        break;
                    case 2:
                        _stem7Slot = _weight;

                        if (_cusSpec.TypeOfExcel == 1)
                            _stemMode = 4;
                        else
                            _stemMode = 3;

                        RaisePropertyChangedEvent(nameof(Stem7Slot));
                        RaisePropertyChangedEvent(nameof(StemMode));
                        break;
                    case 3:
                        _stem12Slot = _weight;
                        _stemMode = 4;
                        RaisePropertyChangedEvent(nameof(Stem12Slot));
                        RaisePropertyChangedEvent(nameof(StemMode));
                        break;
                    case 4:
                        _stem12Mesh = _weight;
                        _stemMode = 5;
                        RaisePropertyChangedEvent(nameof(Stem12Mesh));
                        RaisePropertyChangedEvent(nameof(StemMode));
                        break;
                    case 5:
                        _stemFiber = _weight;
                        _stemMode = 6;
                        RaisePropertyChangedEvent(nameof(StemFiber));
                        RaisePropertyChangedEvent(nameof(StemMode));
                        break;
                    default:
                        _stemMode = 1;
                        RaisePropertyChangedEvent(nameof(StemMode));
                        break;
                }

                _sampleWeight = Convert.ToInt32(_stemOBJ + _stem7Slot + _stem12Slot + _stem12Mesh + _stemFiber);
                RaisePropertyChangedEvent(nameof(SampleWeight));

                if (_stemMode == 6)
                    OnFocusRequested(nameof(StemMode));
                else
                    OnFocusRequested(nameof(Weight));
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private ICommand _onWindowClosedCommand;

        public ICommand OnWindowClosedCommand
        {
            get { return _onWindowClosedCommand ?? (_onWindowClosedCommand = new RelayCommand(OnWindowClose)); }
            set { _onWindowClosedCommand = value; }
        }

        private void OnWindowClose(object obj)
        {
            CloseSerialOnExit();
        }
        #endregion



        #region Funtion
        private void TestCaseBinding()
        {
            _testCase = BLService.DegradationBL()
                .GetByCaseNo(_testCase.Pdno, _testCase.CaseNo);
            RaisePropertyChangedEvent(nameof(TestCase));
        }

        private void DigitalScaleConnect()
        {
            try
            {
                DigitalScaleHelper.Setup();
                DigitalScaleHelper.serialPort.Open();
                DigitalScaleHelper.serialPort.DataReceived += port_DataReceived;
                _isConnectScale = true;
                RaisePropertyChangedEvent(nameof(IsConnectScale));
            }
            catch (Exception ex)
            {
                if (user_setting.User.Role == "Admin" || user_setting.User.Role == "Manager")
                {
                    _isConnectScale = false;
                    _isReadOnly = false;

                    RaisePropertyChangedEvent(nameof(IsConnectScale));
                    RaisePropertyChangedEvent(nameof(IsReadOnly));
                }
                MessageBoxHelper.Exception(ex);
            }
        }

        private delegate void preventCrossThreading(string str);
        //private preventCrossThreading accessControlFromCentralThread;

        private void displayTextReadIn(string str)
        {
            try
            {
                decimal value;
                if (Decimal.TryParse(str, out value))
                    _weight = Convert.ToDecimal(str);
                else
                    _weight = Convert.ToDecimal(0.0);

                RaisePropertyChangedEvent(nameof(Weight));
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void port_DataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            try
            {
                displayTextReadIn(DigitalScaleHelper.GetWeightFromStemScale());
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void CloseSerialOnExit()
        {
            try
            {
                DigitalScaleHelper.serialPort.DataReceived -= port_DataReceived;
                DigitalScaleHelper.serialPort.Close();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }
        #endregion
    }
}
