﻿using DegradationSystemBLL;
using DegradationSystemWinApp.MVVM;
using DomainModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace DegradationSystemWinApp.ViewModel.Admin
{
    public struct Role
    {
        public string RoleName { get; set; }
    }

    public class vm_UserAccount : ObservableObject, IRequestFocus
    {
        public event EventHandler<FocusRequestedEventArgs> FocusRequested;
        protected virtual void OnFocusRequested(string propertyName)
        {
            FocusRequested?.Invoke(this, new FocusRequestedEventArgs(propertyName));
        }

        public vm_UserAccount()
        {
            var list = new List<Role>();
            list.Add(new Role { RoleName = "Size" });
            list.Add(new Role { RoleName = "Stem" });
            list.Add(new Role { RoleName = "Superviser" });
            list.Add(new Role { RoleName = "Admin" });
            _roleList = list;

            RaisePropertyChangedEvent(nameof(RoleList));
            UserListBinding();
            ClearForm();
        }


        #region Properties
        private string _username;

        public string Username
        {
            get { return _username; }
            set { _username = value; }
        }

        private string _password;

        public string Password
        {
            get { return _password; }
            set { _password = value; }
        }

        private string _role;

        public string Role
        {
            get { return _role; }
            set { _role = value; }
        }

        private string _remark;

        public string Remark
        {
            get { return _remark; }
            set { _remark = value; }
        }

        private bool _status;

        public bool Status
        {
            get { return _status; }
            set { _status = value; }
        }

        private bool _isAdd;

        public bool IsAdd
        {
            get { return _isAdd; }
            set { _isAdd = value; }
        }

        private bool _isEdit;

        public bool IsEdit
        {
            get { return _isEdit; }
            set { _isEdit = value; }
        }

        private int _totalRecord;

        public int TotalRecord
        {
            get { return _totalRecord; }
            set { _totalRecord = value; }
        }

        #endregion


        #region List
        private List<Role> _roleList;

        public List<Role> RoleList
        {
            get { return _roleList; }
            set { _roleList = value; }
        }

        private List<SieveUserAccount> _userList;

        public List<SieveUserAccount> UserList
        {
            get { return _userList; }
            set { _userList = value; }
        }

        #endregion


        #region Command
        private ICommand _OnAddCommand;

        public ICommand OnAddCommand
        {
            get { return _OnAddCommand ?? (_OnAddCommand = new RelayCommand(Add)); }
            set { _OnAddCommand = value; }
        }

        private void Add(object obj)
        {
            try
            {
                if (string.IsNullOrEmpty(_username))
                {
                    MessageBoxHelper.Warning("โปรดกรอก username");
                    OnFocusRequested(nameof(Username));
                    return;
                }

                if (string.IsNullOrEmpty(_password))
                {
                    MessageBoxHelper.Warning("โปรดกรอก password");
                    OnFocusRequested(nameof(Password));
                    return;
                }

                if (string.IsNullOrEmpty(_role))
                {
                    MessageBoxHelper.Warning("โปรดระบุ role");
                    OnFocusRequested(nameof(Role));
                    return;
                }

                BLService.UserAccountBL().Add(new SieveUserAccount
                {
                    Username = _username,
                    Password = _password,
                    Role = _role,
                    UserStatus = _status,
                    Remark = _remark,
                    ModifiedDate = DateTime.Now
                });

                MessageBoxHelper.Info("บันทึกสำเร็จ");

                ClearForm();
                UserListBinding();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private ICommand _onEditSelectedCommand;

        public ICommand OnEditSelectedCommand
        {
            get { return _onEditSelectedCommand ?? (_onEditSelectedCommand = new RelayCommand(EditSelected)); }
            set { _onEditSelectedCommand = value; }
        }

        private void EditSelected(object obj)
        {
            try
            {
                var item = (SieveUserAccount)obj;

                _isEdit = true;
                _isAdd = false;
                _username = item.Username;
                _password = item.Password;
                _role = item.Role;
                _remark = item.Remark;
                _status = item.UserStatus;

                RaisePropertyChangedEvent(nameof(IsEdit));
                RaisePropertyChangedEvent(nameof(IsAdd));
                RaisePropertyChangedEvent(nameof(Username));
                RaisePropertyChangedEvent(nameof(Password));
                RaisePropertyChangedEvent(nameof(Role));
                RaisePropertyChangedEvent(nameof(Remark));
                RaisePropertyChangedEvent(nameof(Status));
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private ICommand _onEditCommand;

        public ICommand OnEditCommand
        {
            get { return _onEditCommand ?? (_onEditCommand = new RelayCommand(Edit)); }
            set { _onEditCommand = value; }
        }

        private void Edit(object obj)
        {
            try
            {
                if (string.IsNullOrEmpty(_username))
                    throw new ArgumentException("โปรดระบุ username");

                if (string.IsNullOrEmpty(_password))
                    throw new ArgumentException("โปรดระบุ password");

                if (string.IsNullOrEmpty(_role))
                    throw new ArgumentException("โปรดระบุ role");

                if (MessageBoxHelper.Question("ท่านต้องการแก้ไขข้อมูลนี้ใช่หรือไม่?") ==
                    System.Windows.MessageBoxResult.No)
                    return;

                BLService.UserAccountBL()
                    .Edit(new SieveUserAccount
                    {
                        Username = _username,
                        Password = _password,
                        Role = _role,
                        Remark = _remark,
                        UserStatus = _status,
                        ModifiedDate = DateTime.Now
                    });

                MessageBoxHelper.Info("บันทึกสำเร็จ");

                ClearForm();
                UserListBinding();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private ICommand _onDeleteCommand;

        public ICommand OnDeleteCommand
        {
            get { return _onDeleteCommand ?? (_onDeleteCommand = new RelayCommand(Delete)); }
            set { _onDeleteCommand = value; }
        }

        private void Delete(object obj)
        {
            try
            {
                if (MessageBoxHelper.Question("ท่านต้องการลบข้อมูลนี้ใช่หรือไม่?") ==
                    System.Windows.MessageBoxResult.No)
                    return;
                var item = (SieveUserAccount)obj;
                BLService.UserAccountBL().Delete(item.Username);

                ClearForm();
                UserListBinding();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private ICommand _onClearCommand;

        public ICommand OnClearCommand
        {
            get { return _onClearCommand ?? (_onClearCommand = new RelayCommand(Clear)); }
            set { _onClearCommand = value; }
        }

        private void Clear(object obj)
        {
            ClearForm();
        }

        #endregion


        #region Function
        private void UserListBinding()
        {
            _userList = BLService.UserAccountBL()
                .GetAll()
                .OrderBy(x => x.Username)
                .ToList();
            _totalRecord = _userList.Count();

            RaisePropertyChangedEvent(nameof(UserList));
            RaisePropertyChangedEvent(nameof(TotalRecord));
        }

        private void ClearForm()
        {
            _username = "";
            _password = "";
            _remark = "";
            _isEdit = false;
            _isAdd = true;
            _status = true;

            UserListBinding();

            RaisePropertyChangedEvent(nameof(Username));
            RaisePropertyChangedEvent(nameof(Password));
            RaisePropertyChangedEvent(nameof(Remark));
            RaisePropertyChangedEvent(nameof(IsEdit));
            RaisePropertyChangedEvent(nameof(IsAdd));
            RaisePropertyChangedEvent(nameof(Status));
        }
        #endregion
    }
}
