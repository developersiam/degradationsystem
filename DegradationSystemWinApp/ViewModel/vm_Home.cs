﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DegradationSystemWinApp.MVVM;
using System.Windows.Input;
using DegradationSystemWinApp.View.Staff;
using DegradationSystemWinApp.ViewModel.Staff;
using DegradationSystemWinApp.View.Admin;
using DegradationSystemWinApp.ViewModel.Admin;

namespace DegradationSystemWinApp.ViewModel
{
    public class vm_Home : ObservableObject, IRequestFocus
    {
        public event EventHandler<FocusRequestedEventArgs> FocusRequested;
        protected virtual void OnFocusRequested(string propertyName)
        {
            FocusRequested?.Invoke(this, new FocusRequestedEventArgs(propertyName));
        }

        public vm_Home()
        {

        }


        #region Command
        private ICommand _onSizeCommand;

        public ICommand OnSizeCommand
        {
            get { return _onSizeCommand ?? (_onSizeCommand = new RelayCommand(OnSizeClick)); }
            set { _onSizeCommand = value; }
        }

        private void OnSizeClick(object obj)
        {
            try
            {
                var window = new SizeContent();
                var vm = new vm_SizeContent();
                window.DataContext = vm;
                window.ShowDialog();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private ICommand _onStemCommand;

        public ICommand OnStemCommand
        {
            get { return _onStemCommand ?? (_onStemCommand = new RelayCommand(OnStemClick)); }
            set { _onStemCommand = value; }
        }

        private void OnStemClick(object obj)
        {
            try
            {
                if (user_setting.User.Role == "Admin" ||
                    user_setting.User.Role == "Supervisor" ||
                    user_setting.User.Role == "Stem")
                {
                    var window = new TestCase();
                    window.ShowDialog();
                }
                else
                    throw new ArgumentException("user ของท่านไม่สามารถเข้าใช้เมนูนี้ได้");

            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private ICommand _onCustomerSpecCommand;

        public ICommand OnCustomerSpecCommand
        {
            get { return _onCustomerSpecCommand ?? (_onCustomerSpecCommand = new RelayCommand(OnCustomerClick)); }
            set { _onCustomerSpecCommand = value; }
        }

        private void OnCustomerClick(object obj)
        {
            try
            {
                if (user_setting.User.Role == "Admin" ||
                    user_setting.User.Role == "Supervisor" ||
                    user_setting.User.Role == "Stem")
                {
                    var window = new CustomerSpec();
                    window.DataContext = new vm_CustomerSpec();
                    window.ShowDialog();
                }
                else
                    throw new ArgumentException("user ของท่านไม่สามารถเข้าใช้เมนูนี้ได้");
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private ICommand _onBackwardRecordCommand;

        public ICommand OnBackwardRecordCommand
        {
            get { return _onBackwardRecordCommand ?? (_onBackwardRecordCommand = new RelayCommand(OnBackwardRecord)); }
            set { _onBackwardRecordCommand = value; }
        }

        private void OnBackwardRecord(object obj)
        {
            try
            {
                if (user_setting.User.Role == "Admin" ||
                    user_setting.User.Role == "Supervisor")
                {
                    var window = new SizeContent();
                    var vm = new vm_SizeContent();
                    window.DataContext = vm;
                    window.ShowDialog();
                }
                else
                    throw new ArgumentException("user ของท่านไม่สามารถเข้าใช้เมนูนี้ได้");
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private ICommand _onAdminCommand;

        public ICommand OnAdminCommand
        {
            get { return _onAdminCommand ?? (_onAdminCommand = new RelayCommand(OnAdminClick)); }
            set { _onAdminCommand = value; }
        }

        private void OnAdminClick(object obj)
        {
            try
            {
                if (user_setting.User.Role != "Admin")
                    throw new ArgumentException("user ของท่านไม่สามารถเข้าใช้เมนูนี้ได้");

                var window = new UserAccount();
                window.DataContext = new vm_UserAccount();
                window.ShowDialog();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }
        #endregion


        #region Funtion

        #endregion
    }
}
