﻿using DegradationSystemWinApp.MVVM;
using DegradationSystemWinApp.View;
using MahApps.Metro.Controls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace DegradationSystemWinApp
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : MetroWindow
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void LaunchGitHubSite(object sender, RoutedEventArgs e)
        {
            // Launch the GitHub site...
        }

        private void LogOffMenu_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                user_setting.User = null;
                MainFrame.NavigationService.Navigate(new Login());
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }
    }
}
