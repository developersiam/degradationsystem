﻿using Microsoft.Office.Interop.Excel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DegradationSystemWinApp.Helper
{
    public static class ExcelHelper
    {
        private static Application excelApplication = new Application();
        private static Workbook excelWorkBook;
        private static Worksheet excelWorkSheet;
        private static Range excelRange;

        public static void PutDataToCell(string strCell, string value)
        {
            excelRange = excelWorkSheet.Range[strCell];
            excelWorkSheet.Range[strCell].Value = value;
        }

        public static void NumberFormatN0(string strCell)
        {
            excelRange = excelWorkSheet.Range[strCell];
            excelRange.NumberFormat = "0";
        }

        public static void BoxBorder(string strCell)
        {
            excelRange = excelWorkSheet.Range[strCell];

            //excelWorkSheet.Range[strCell].Borders[XlBordersIndex.xlEdgeLeft].LineStyle = XlLineStyle.xlContinuous;
            //excelWorkSheet.Range[strCell].Borders[XlBordersIndex.xlEdgeRight].Weight = XlLineStyle.xlContinuous;
            //excelWorkSheet.Range[strCell].Borders[XlBordersIndex.xlEdgeTop].Weight = XlLineStyle.xlContinuous;
            //excelWorkSheet.Range[strCell].Borders[XlBordersIndex.xlEdgeBottom].Weight = XlLineStyle.xlContinuous;

            Range cell = excelRange.Cells[1][1];
            Borders border = cell.Borders;
            border[XlBordersIndex.xlEdgeLeft].LineStyle = XlLineStyle.xlContinuous;
            border[XlBordersIndex.xlEdgeTop].LineStyle = XlLineStyle.xlContinuous;
            border[XlBordersIndex.xlEdgeBottom].LineStyle = XlLineStyle.xlContinuous;
            border[XlBordersIndex.xlEdgeRight].LineStyle = XlLineStyle.xlContinuous;
        }

        public static void MergeRight(string from, string to, bool isBorder, bool isCenter)
        {
            excelRange = excelWorkSheet.Range[from, to];
            excelRange.Merge();
            excelRange.VerticalAlignment = XlVAlign.xlVAlignCenter;

            if (isCenter == true)
                excelRange.HorizontalAlignment = XlHAlign.xlHAlignCenter;
            else
                excelRange.HorizontalAlignment = XlHAlign.xlHAlignLeft;

            if (isBorder == true)
            {
                excelRange.Borders[XlBordersIndex.xlEdgeLeft].LineStyle = XlLineStyle.xlContinuous;
                excelRange.Borders[XlBordersIndex.xlEdgeTop].LineStyle = XlLineStyle.xlContinuous;
                excelRange.Borders[XlBordersIndex.xlEdgeBottom].LineStyle = XlLineStyle.xlContinuous;
                excelRange.Borders[XlBordersIndex.xlEdgeRight].LineStyle = XlLineStyle.xlContinuous;
            }
        }

        public static void Open(string filePath)
        {
            excelWorkBook = excelApplication.Workbooks.Open(filePath);
            excelWorkSheet = excelWorkBook.ActiveSheet;
            excelWorkSheet.PageSetup.Zoom = false;
            excelWorkSheet.PageSetup.FitToPagesWide = 1;
            excelApplication.Visible = true;
        }

        public static void Close()
        {
            //excelWorkSheet = null;
            //excelWorkBook = null;
            //excelApplication = null;

            excelWorkBook.Close(0);
            excelApplication.Quit();
        }

        public static void Print(short copies)
        {
            excelWorkSheet.PrintOutEx(Type.Missing,
                Type.Missing,
                copies,
                Type.Missing,
                Type.Missing, 
                Type.Missing,
                Type.Missing,
                Type.Missing,
                Type.Missing);
            excelWorkBook.Close(0);
            excelApplication.Quit();
        }
    }
}
