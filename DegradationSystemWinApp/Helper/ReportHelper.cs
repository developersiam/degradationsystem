﻿using DegradationSystemBLL;
using DegradationSystemWinApp.MVVM;
using DomainModel;
using Microsoft.Office.Interop.Excel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DegradationSystemWinApp.Helper
{
    public static class ReportHelper
    {
        public static void DegradationReport(List<SieveDegradation> list)
        {
            try
            {
                var _packedGrade = BLService.packedGradeBL().GetSingle(list.FirstOrDefault().PackGrade);
                var _customerSpec = BLService.CustomerSpecBL()
                    .GetByPackGrade(Convert.ToInt16(_packedGrade.crop), _packedGrade.customer, _packedGrade.packedgrade1);

                if (_customerSpec.TypeOfExcel == 1)
                {
                    ExcelHelper.Open(@"C:\DegForm\DegradationForm1.xlsx");

                    ExcelHelper.PutDataToCell("C5", _packedGrade.customer);
                    ExcelHelper.PutDataToCell("F5", _packedGrade.packedgrade1);
                    ExcelHelper.PutDataToCell("K5", _packedGrade.crop.ToString());
                    ExcelHelper.PutDataToCell("P5", _packedGrade.type);
                    ExcelHelper.PutDataToCell("S5", _packedGrade.form);
                    ExcelHelper.PutDataToCell("Z5", "DATE : " + list.Max(x => x.Ddate).ToShortDateString());

                    int i = 10;
                    foreach (var item in list)
                    {
                        ExcelHelper.PutDataToCell("A" + i, item.Ddate.ToShortDateString());
                        ExcelHelper.PutDataToCell("B" + i, item.Ttime.ToString());
                        ExcelHelper.PutDataToCell("D" + i, item.Over1.ToString());
                        ExcelHelper.PutDataToCell("F" + i, item.Over1_2.ToString());
                        ExcelHelper.PutDataToCell("I" + i, item.Over1_4.ToString());
                        ExcelHelper.PutDataToCell("L" + i, item.Over1_8.ToString());
                        ExcelHelper.PutDataToCell("N" + i, item.Pan.ToString());
                        ExcelHelper.PutDataToCell("Q" + i, item.Stem_OBJ.ToString());
                        ExcelHelper.PutDataToCell("S" + i, item.Stem_7_slot.ToString());
                        ExcelHelper.PutDataToCell("U" + i, item.Stem_12_slot.ToString());
                        ExcelHelper.PutDataToCell("W" + i, item.Stem_fiber.ToString());
                        ExcelHelper.PutDataToCell("Z" + i, item.CaseNo.ToString());
                        i++;
                    }
                }
                else
                {
                    ExcelHelper.Open(@"C:\DegForm\DegradationForm2.xlsx");

                    ExcelHelper.PutDataToCell("C5", _packedGrade.customer);
                    ExcelHelper.PutDataToCell("G5", _packedGrade.packedgrade1);
                    ExcelHelper.PutDataToCell("L5", _packedGrade.crop.ToString());
                    ExcelHelper.PutDataToCell("Q5", _packedGrade.type);
                    ExcelHelper.PutDataToCell("U5", _packedGrade.form);
                    ExcelHelper.PutDataToCell("AF5", "DATE : " + list.Max(x => x.Ddate).ToShortDateString());

                    int i = 10;
                    foreach (var item in list)
                    {
                        ExcelHelper.PutDataToCell("A" + i, item.Ddate.ToShortDateString());
                        ExcelHelper.PutDataToCell("B" + i, item.Ttime.Hours + ":" + item.Ttime.Minutes);
                        ExcelHelper.PutDataToCell("C" + i, item.SampleWeightPickUP.ToString());
                        ExcelHelper.PutDataToCell("E" + i, item.Over1.ToString());
                        ExcelHelper.PutDataToCell("G" + i, item.Over1_2.ToString());
                        ExcelHelper.PutDataToCell("J" + i, item.Over1_4.ToString());
                        ExcelHelper.PutDataToCell("M" + i, item.Over1_8.ToString());
                        ExcelHelper.PutDataToCell("O" + i, item.Pan.ToString());
                        ExcelHelper.PutDataToCell("R" + i, item.StemSampleWeight.ToString());
                        ExcelHelper.PutDataToCell("S" + i, item.Stem_OBJ.ToString());
                        ExcelHelper.PutDataToCell("U" + i, item.Stem_7_slot.ToString());
                        ExcelHelper.PutDataToCell("W" + i, item.Stem_12_slot.ToString());
                        ExcelHelper.PutDataToCell("Y" + i, item.Stem_12_mesh.ToString());
                        ExcelHelper.PutDataToCell("AA" + i, item.FiberThru12Mesh.ToString());
                        ExcelHelper.PutDataToCell("AF" + i, item.CaseNo.ToString());
                        i++;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
                ExcelHelper.Close();
            }
        }
    }
}
