﻿using DegradationSystemWinApp.MVVM;
using DegradationSystemWinApp.ViewModel.Admin;
using MahApps.Metro.Controls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace DegradationSystemWinApp.View.Admin
{
    /// <summary>
    /// Interaction logic for UserAccount.xaml
    /// </summary>
    public partial class UserAccount : MetroWindow
    {
        public UserAccount()
        {
            InitializeComponent();
        }

        private void MetroWindow_Loaded(object sender, RoutedEventArgs e)
        {
            IRequestFocus focus = (IRequestFocus)DataContext;
            focus.FocusRequested += Focus_FocusRequested;
        }

        private void Focus_FocusRequested(object sender, FocusRequestedEventArgs e)
        {
            var vm = (vm_UserAccount)DataContext;
            switch (e.PropertyName)
            {
                case nameof(vm.Username):
                    UsernameTextBox.Focus();
                    UsernameTextBox.SelectAll();
                    break;
                case nameof(vm.Password):
                    PasswordPasswordBox.Focus();
                    PasswordPasswordBox.SelectAll();
                    break;
                case nameof(vm.Role):
                    RoleComboBox.Focus();
                    break;
            }
        }
    }
}
