﻿using DegradationSystemWinApp.ViewModel.Staff;
using MahApps.Metro.Controls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace DegradationSystemWinApp.View.Staff
{
    /// <summary>
    /// Interaction logic for EditTestTime.xaml
    /// </summary>
    public partial class EditTestTime : MetroWindow
    {
        public EditTestTime()
        {
            InitializeComponent();
            DataContext = new vm_EditTestTime();
        }
    }
}
