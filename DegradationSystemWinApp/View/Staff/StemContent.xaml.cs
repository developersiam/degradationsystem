﻿using DegradationSystemWinApp.MVVM;
using DegradationSystemWinApp.ViewModel.Staff;
using MahApps.Metro.Controls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace DegradationSystemWinApp.View.Staff
{
    /// <summary>
    /// Interaction logic for StemContent.xaml
    /// </summary>
    public partial class StemContent : MetroWindow
    {
        public StemContent()
        {
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            IRequestFocus focus = (IRequestFocus)DataContext;
            focus.FocusRequested += Focus_FocusRequested;
            WeightTextBox.Focus();
        }

        private void Focus_FocusRequested(object sender, FocusRequestedEventArgs e)
        {
            var vm = (vm_StemContent)DataContext;
            switch (e.PropertyName)
            {
                case nameof(vm.Weight):
                    WeightTextBox.Focus();
                    WeightTextBox.SelectAll();
                    break;
                case nameof(vm.SampleWeight):
                    SampleWeightTextBox.Focus();
                    SampleWeightTextBox.SelectAll();
                    break;
                case nameof(vm.SampleWeightPickedUp):
                    SamplePickedUpWeightTextBox.Focus();
                    SamplePickedUpWeightTextBox.SelectAll();
                    break;
                case nameof(vm.StemMode):
                    SaveButton.Focus();
                    break;
            }
        }
    }
}
