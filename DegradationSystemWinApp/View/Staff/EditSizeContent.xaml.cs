﻿using DegradationSystemWinApp.MVVM;
using DegradationSystemWinApp.ViewModel.Staff;
using MahApps.Metro.Controls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace DegradationSystemWinApp.View.Staff
{
    /// <summary>
    /// Interaction logic for EditSizeContent.xaml
    /// </summary>
    public partial class EditSizeContent : MetroWindow
    {
        public EditSizeContent()
        {
            InitializeComponent();
            DataContext = new vm_EditSizeContent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            IRequestFocus focus = (IRequestFocus)DataContext;
            focus.FocusRequested += Focus_FocusRequested;
        }

        private void Focus_FocusRequested(object sender, FocusRequestedEventArgs e)
        {
            var vm = (vm_SizeContent)DataContext;
            switch (e.PropertyName)
            {
                case nameof(vm.Weight):
                    WeightTextBox.Focus();
                    WeightTextBox.SelectAll();
                    break;
                case nameof(vm.SizeMode):
                    SaveButton.Focus();
                    break;
            }
        }
    }
}
