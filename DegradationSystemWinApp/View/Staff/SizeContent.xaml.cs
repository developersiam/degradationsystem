﻿using DegradationSystemWinApp.MVVM;
using DegradationSystemWinApp.ViewModel.Staff;
using MahApps.Metro.Controls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace DegradationSystemWinApp.View.Staff
{
    /// <summary>
    /// Interaction logic for SizeContent.xaml
    /// </summary>
    public partial class SizeContent : MetroWindow
    {
        public SizeContent()
        {
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            IRequestFocus focus = (IRequestFocus)DataContext;
            focus.FocusRequested += Focus_FocusRequested;
        }

        private void Focus_FocusRequested(object sender, FocusRequestedEventArgs e)
        {
            var vm = (vm_SizeContent)DataContext;
            switch (e.PropertyName)
            {
                case nameof(vm.CaseNo):
                    CaseNoSearchTextBox.Focus();
                    CaseNoSearchTextBox.SelectAll();
                    break;
                case nameof(vm.Weight):
                    WeightTextBox.Focus();
                    WeightTextBox.SelectAll();
                    break;
                case nameof(vm.SizeMode):
                    SendButton.Focus();
                    break;
            }
        }
    }
}
