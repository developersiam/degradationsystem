﻿using DegradationSystemBLL;
using DegradationSystemWinApp.MVVM;
using MahApps.Metro.Controls;
using Microsoft.Reporting.WinForms;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace DegradationSystemWinApp.View.Report
{
    /// <summary>
    /// Interaction logic for RPTDEG01.xaml
    /// </summary>
    public partial class RPTDEG01 : MetroWindow
    {
        public string _customerCode { get; set; }
        public string _packedGrade { get; set; }
        public DateTime _fromDate { get; set; }
        public DateTime _toDate { get; set; }

        public RPTDEG01()
        {
            InitializeComponent();
        }

        private void MetroWindow_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                ReportDataSource datasource = new ReportDataSource();
                datasource.Value = BLService.DegradationBL().GetReport(_customerCode, _packedGrade, _fromDate, _toDate);
                datasource.Name = "sp_Sieve_Report_DegradationTestCaseDetails";

                Report.Reset();
                Report.LocalReport.DataSources.Add(datasource);
                Report.LocalReport.ReportEmbeddedResource = "DegradationSystemWinApp.View.Report.RPTDEG01.rdlc";
                Report.RefreshReport();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }
    }
}
