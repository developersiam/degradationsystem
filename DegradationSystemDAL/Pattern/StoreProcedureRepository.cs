﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DomainModel;
using DegradationSystemDAL.EDMX;

namespace DegradationSystemDAL.Pattern
{

    public static class StoreProcedureRepository
    {
        public static List<sp_Sieve_Report_DegradationTestCaseDetails_Result> sp_Sieve_Report_DegradationTestCaseDetails(string customer, string packGrade, DateTime fromDate, DateTime toDate)
        {
            try
            {
                using (StecDBMSEntities _context = new StecDBMSEntities())
                {
                    return _context.sp_Sieve_Report_DegradationTestCaseDetails(customer, packGrade, fromDate, toDate).ToList();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //public static List<sp_Sieve_RPT_DEG001_Result> sp_Sieve_RPT_DEG001(int crop, string customer, string packGrade, string fromDate, string toDate)
        //{
        //    try
        //    {
        //        using (StecDBMSEntities _context = new StecDBMSEntities())
        //        {
        //            return _context.sp_Sieve_RPT_DEG001(crop, customer, packGrade, fromDate, toDate).ToList();
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}

        //public static List<sp_Sieve_SEL_CaseNoByPdno_Result> sp_Sieve_SEL_CaseNoByPdno(string pdno)
        //{
        //    try
        //    {
        //        using (StecDBMSEntities _context = new StecDBMSEntities())
        //        {
        //            return _context.sp_Sieve_SEL_CaseNoByPdno(pdno).ToList();
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}

        //public static int sp_Sieve_SEL_LOG(int crop, string customer, string grade)
        //{
        //    try
        //    {
        //        using (StecDBMSEntities _context = new StecDBMSEntities())
        //        {
        //            return Convert.ToInt16(_context.sp_Sieve_SEL_LOG(crop, customer, grade));
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}

        //public static List<sp_Sieve_SEL_pdsetupByCrop_Result> sp_Sieve_SEL_pdsetupByCrop(int crop)
        //{
        //    try
        //    {
        //        using (StecDBMSEntities _context = new StecDBMSEntities())
        //        {
        //            return _context.sp_Sieve_SEL_pdsetupByCrop(crop).ToList();
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}

        //public static List<sp_Sieve_SEL_pdsetupByDate_Result> sp_Sieve_SEL_pdsetupByDate()
        //{
        //    try
        //    {
        //        using (StecDBMSEntities _context = new StecDBMSEntities())
        //        {
        //            return _context.sp_Sieve_SEL_pdsetupByDate().ToList();
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}

        //public static List<sp_Sieve_SEL_pdsetupBydef_Result> sp_Sieve_SEL_pdsetupBydef()
        //{
        //    try
        //    {
        //        using (StecDBMSEntities _context = new StecDBMSEntities())
        //        {
        //            return _context.sp_Sieve_SEL_pdsetupBydef().ToList();
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}

        //public static List<sp_Sieve_SEL_pdsetupByPdno_Result> sp_Sieve_SEL_pdsetupByPdno(string pdno)
        //{
        //    try
        //    {
        //        using (StecDBMSEntities _context = new StecDBMSEntities())
        //        {
        //            return _context.sp_Sieve_SEL_pdsetupByPdno(pdno).ToList();
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}

        //public static List<sp_Sieve_SEL_SieveDegradation_Result> sp_Sieve_SEL_SieveDegradation(string pdno, short caseNo)
        //{
        //    try
        //    {
        //        using (StecDBMSEntities _context = new StecDBMSEntities())
        //        {
        //            return _context.sp_Sieve_SEL_SieveDegradation(pdno, caseNo).ToList();
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}

        //public static List<sp_Sieve_SEL_SieveDegradation_For_Excel_Result> sp_Sieve_SEL_SieveDegradation_For_Excel(int crop, string customer, string grade)
        //{
        //    try
        //    {
        //        using (StecDBMSEntities _context = new StecDBMSEntities())
        //        {
        //            return _context.sp_Sieve_SEL_SieveDegradation_For_Excel(crop, customer, grade).ToList();
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}

        //public static List<sp_Sieve_SEL_SieveDegradation_For_Export_Result> sp_Sieve_SEL_SieveDegradation_For_Export(string fromDate, string toDate, int crop, string customer, string grade)
        //{
        //    try
        //    {
        //        using (StecDBMSEntities _context = new StecDBMSEntities())
        //        {
        //            return _context.sp_Sieve_SEL_SieveDegradation_For_Export(fromDate, toDate, crop, customer, grade).ToList();
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}

        //public static List<sp_Sieve_SEL_TestCaseByCustomerSpec_Result> sp_Sieve_SEL_TestCaseByCustomerSpec(DateTime fromDate, DateTime toDate, string customer, string packGrade)
        //{
        //    try
        //    {
        //        using (StecDBMSEntities _context = new StecDBMSEntities())
        //        {
        //            return _context.sp_Sieve_SEL_TestCaseByCustomerSpec(fromDate, toDate, customer, packGrade).ToList();
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}

        //public static sp_Sieve_SEL_UserAccount_Result sp_Sieve_SEL_UserAccount(string username)
        //{
        //    try
        //    {
        //        using (StecDBMSEntities _context = new StecDBMSEntities())
        //        {
        //            return _context.sp_Sieve_SEL_UserAccount(username).SingleOrDefault();
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}

        //public static List<sp_Sieve_SELALL_CaseNo_Result> sp_Sieve_SELALL_CaseNo()
        //{
        //    try
        //    {
        //        using (StecDBMSEntities _context = new StecDBMSEntities())
        //        {
        //            return _context.sp_Sieve_SELALL_CaseNo().ToList();
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}

        //public static List<sp_Sieve_SELALL_Customer_Result> sp_Sieve_SELALL_Customer()
        //{
        //    try
        //    {
        //        using (StecDBMSEntities _context = new StecDBMSEntities())
        //        {
        //            return _context.sp_Sieve_SELALL_Customer().ToList();
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}

        //public static List<sp_Sieve_SELALL_CustomerSpec_Result> sp_Sieve_SELALL_CustomerSpec()
        //{
        //    try
        //    {
        //        using (StecDBMSEntities _context = new StecDBMSEntities())
        //        {
        //            return _context.sp_Sieve_SELALL_CustomerSpec().ToList();
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}

        //public static List<sp_Sieve_SELALL_SieveDegradation_Result> sp_Sieve_SELALL_SieveDegradation()
        //{
        //    try
        //    {
        //        using (StecDBMSEntities _context = new StecDBMSEntities())
        //        {
        //            return _context.sp_Sieve_SELALL_SieveDegradation().ToList();
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}

        //public static List<sp_Sieve_SELALL_UserAccount_Result> sp_Sieve_SELALL_UserAccount()
        //{
        //    try
        //    {
        //        using (StecDBMSEntities _context = new StecDBMSEntities())
        //        {
        //            return _context.sp_Sieve_SELALL_UserAccount().ToList();
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}

        //public static void sp_Sieve_INS_DegradationSizeContent(string pdno, short caseNo, int crop, string customer, string packGrade,
        //    TimeSpan ttime, DateTime ddate, double over1, double over12, double over14, double over18, double pan, string sieveUser)
        //{
        //    using (var _context = new StecDBMSEntities())
        //    {
        //        _context.sp_Sieve_INS_DegradationSizeContent(pdno, caseNo, crop, customer, packGrade,
        //            ttime, ddate, over1, over12, over14, over18, pan, sieveUser);
        //    }
        //}

        //public static void sp_Sieve_UPD_SieveDegradation(string pdno, short caseNo,
        //    TimeSpan ttime, DateTime ddate, bool sendStatus, bool approveStatus, double sampleWeight,
        //    double over1, double over1_2, double over1_4, double over1_8,
        //    double over1_p, double over1_2_p, double over1_4_p, double over1_8_p,
        //    double tOT1_2_p, double tOT1_4_p,
        //    double pan, double pan_p, double thru1_4_P,
        //    double stemSampleWeight, double sampleWeightPickUP,
        //    double stem_OBJ, double stem_7_slot, double stem_12_slot, double stem_12_mesh, double fiberThru12Mesh,
        //    double stem_OBJ_p, double stem_7_slot_p, double stem_12_slot_p, double stem_12_mesh_P, double fiberThru12Mesh_P,
        //    double stem_fiber, double stem_fiber_p,
        //    double tOTStem, double avgCost, double tSEFMesh, double newFiber, string sieveUser)
        //{
        //    using (var _context = new StecDBMSEntities())
        //    {
        //        _context.sp_Sieve_UPD_SieveDegradation(pdno, caseNo, ttime, ddate, sendStatus, approveStatus, sampleWeight,
        //            over1, over1_p, over1_2, over1_2_p, tOT1_2_p, over1_4, over1_4_p, tOT1_4_p, over1_8, over1_8_p,
        //            pan, pan_p, thru1_4_P, stemSampleWeight, stem_OBJ, stem_OBJ_p, stem_7_slot, stem_7_slot_p,
        //            stem_12_slot, stem_12_slot_p, stem_12_mesh, stem_12_mesh_P, stem_fiber, stem_fiber_p, tOTStem, avgCost,
        //            sampleWeightPickUP, fiberThru12Mesh, fiberThru12Mesh_P, tSEFMesh, newFiber, sieveUser);
        //    }
        //}

        public static void sp_Sieve_UPD_DegradationTestCase(string excelType, double sampleWeight, double sampleWeightPickUp,
            double over1, double over12, double over14, double over18, double pan, double stemOBJ, double stem7Slot,
            double stem12Slot, double stem12Mesh, double stemFiber, string pdno, short caseNo, string sieveUser, double avgCost)
        {
            using (var _context = new StecDBMSEntities())
            {
                _context.sp_Sieve_UPD_DegradationTestCase(excelType, sampleWeight, sampleWeightPickUp, over1, over12, over14, over18,
                    pan, stemOBJ, stem7Slot, stem12Slot, stem12Mesh, stemFiber, pdno, caseNo, sieveUser, avgCost);
            }
        }

        public static void sp_Sieve_UPD_ChangeCaseNumber(int crop, string customer, string packGrade, string pdNo,
            short oldCaseNumber, short newCaseNumber, string modifiedUser)
        {
            using (var _context = new StecDBMSEntities())
            {
                _context.sp_Sieve_UPD_ChangeCaseNumber(crop, customer, packGrade, pdNo, oldCaseNumber, newCaseNumber, modifiedUser);
            }
        }
    }
}
