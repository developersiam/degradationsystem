﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DomainModel;
using System.Data.Entity;
using DegradationSystemDAL.EDMX;
using DegradationSystemDAL.Pattern;

namespace DegradationSystemDAL.UnitOfWork
{
    public class DegUnitOfWork : IDegUnitOfWork, System.IDisposable
    {
        private readonly StecDBMSEntities _context;
        private IGenericDataRepository<customer> _customerRepo;
        private IGenericDataRepository<packedgrade> _packedgradeRepo;
        private IGenericDataRepository<pd> _pdRepo;
        private IGenericDataRepository<pdsetup> _pdsetupRepo;
        private IGenericDataRepository<SieveCustomerSpec> _sieveCustomerSpecRepo;
        private IGenericDataRepository<SieveDegradation> _sieveDegradationRepo;
        private IGenericDataRepository<SieveDegradationLog> _sieveDegradationLogRepo;
        private IGenericDataRepository<SieveUserAccount> _sieveUserAccountRepo;

        public DegUnitOfWork()
        {
            _context = new StecDBMSEntities();
        }

        public IGenericDataRepository<pd> pdRepo
        {
            get
            {
                return _pdRepo ?? (_pdRepo = new GenericDataRepository<pd>(_context));
            }
        }

        public IGenericDataRepository<customer> customerRepo
        {
            get
            {
                return _customerRepo ?? (_customerRepo = new GenericDataRepository<customer>(_context));
            }
        }

        public IGenericDataRepository<packedgrade> packedgradeRepo
        {
            get
            {
                return _packedgradeRepo ?? (_packedgradeRepo = new GenericDataRepository<packedgrade>(_context));
            }
        }

        public IGenericDataRepository<pdsetup> pdsetupRepo
        {
            get
            {
                return _pdsetupRepo ?? (_pdsetupRepo = new GenericDataRepository<pdsetup>(_context));
            }
        }

        public IGenericDataRepository<SieveCustomerSpec> SieveCustomerSpecRepo
        {
            get
            {
                return _sieveCustomerSpecRepo ?? (_sieveCustomerSpecRepo = new GenericDataRepository<SieveCustomerSpec>(_context));
            }
        }

        public IGenericDataRepository<SieveDegradation> SieveDegradationRepo
        {
            get
            {
                return _sieveDegradationRepo ?? (_sieveDegradationRepo = new GenericDataRepository<SieveDegradation>(_context));
            }
        }

        public IGenericDataRepository<SieveDegradationLog> SieveDegradationLogRepo
        {
            get
            {
                return _sieveDegradationLogRepo ?? (_sieveDegradationLogRepo = new GenericDataRepository<SieveDegradationLog>(_context));
            }
        }

        public IGenericDataRepository<SieveUserAccount> SieveUserAccountRepo
        {
            get
            {
                return _sieveUserAccountRepo ?? (_sieveUserAccountRepo = new GenericDataRepository<SieveUserAccount>(_context));
            }
        }

        public void Save()
        {
            _context.SaveChanges();
        }
        private bool disposed = false;
        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
            }
        }
        public void Dispose()
        {
            Dispose(true);
            System.GC.SuppressFinalize(this);
        }
    }
}