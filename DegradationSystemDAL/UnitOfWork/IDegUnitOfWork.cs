﻿using DegradationSystemDAL.Pattern;
using DomainModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DegradationSystemDAL.UnitOfWork
{
    public interface IDegUnitOfWork
    {
        IGenericDataRepository<customer> customerRepo { get; }
        IGenericDataRepository<packedgrade> packedgradeRepo { get; }
        IGenericDataRepository<pd> pdRepo { get; }
        IGenericDataRepository<pdsetup> pdsetupRepo { get; }
        IGenericDataRepository<SieveCustomerSpec> SieveCustomerSpecRepo { get; }
        IGenericDataRepository<SieveDegradation> SieveDegradationRepo { get; }
        IGenericDataRepository<SieveDegradationLog> SieveDegradationLogRepo { get; }
        IGenericDataRepository<SieveUserAccount> SieveUserAccountRepo { get; }
        void Save();
    }
}
