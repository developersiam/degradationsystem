﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DegradationSystemDAL.UnitOfWork;
using DomainModel;

public interface IpdsetupBL
{
    pdsetup GetByPdno(string pdno);
    pdsetup GetByDef();
    List<pdsetup> GetByCrop(int crop);
    List<pdsetup> GetByDateOrDefult();
}

public class pdsetupBL : IpdsetupBL
{
    DegUnitOfWork uow;

    public pdsetupBL()
    {
        uow = new DegUnitOfWork();
    }

    public pdsetup GetByPdno(string pdno)
    {
        return uow.pdsetupRepo.GetSingle(x => x.pdno == pdno);
    }

    public pdsetup GetByDef()
    {
        return uow.pdsetupRepo.Get(x => x.def == true).SingleOrDefault();
    }

    public List<pdsetup> GetByCrop(int crop)
    {
        return uow.pdsetupRepo.Get(x => x.crop == crop);
    }

    public List<pdsetup> GetByDateOrDefult()
    {
        return uow.pdsetupRepo
            .Get(x => x.date != null)
            .Where(x => x.date.Value.Date == DateTime.Now.Date || x.def == true)
            .ToList();
    }
}
