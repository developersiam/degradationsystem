﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool
//     Changes to this file will be lost if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DegradationSystemDAL.UnitOfWork;
using DegradationSystemDAL.Pattern;
using DomainModel;

public interface IpdBL
{
    List<pd> GetByPdSetupDef();
    List<pd> GetByPdno(string pdno);
    pd GetByCaseNo(string pdno, int caseNo);
}

public class pdBL : IpdBL
{
    DegUnitOfWork uow;

    public pdBL()
    {
        uow = new DegUnitOfWork();
    }

    public List<pd> GetByPdno(string pdno)
    {
        return uow.pdRepo.Get(x => x.frompdno == pdno);
    }

    public pd GetByCaseNo(string pdno, int caseNo)
    {
        return uow.pdRepo
            .Get(x => x.frompdno == pdno &&
            x.caseno == caseNo &&
            x.pdtype == "Lamina")
            .SingleOrDefault();
    }

    public List<pd> GetByPdSetupDef()
    {
        var pdsetup = DegradationSystemBLL.BLService.pdsetupBL().GetByDef();
        return uow.pdRepo.Get(x => x.frompdno == pdsetup.pdno)
            .OrderByDescending(x => x.caseno)
            .Take(250)
            .ToList();
    }
}

