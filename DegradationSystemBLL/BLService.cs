﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DegradationSystemBLL
{
    public static class BLService
    {
        public static IcustomerBL customerBL()
        {
            IcustomerBL obj = new customerBL();
            return obj;
        }

        public static ICustomerSpecBL CustomerSpecBL()
        {
            ICustomerSpecBL obj = new CustomerSpecBL();
            return obj;
        }

        public static IDegradationBL DegradationBL()
        {
            IDegradationBL obj = new DegradationBL();
            return obj;
        }

        public static IpackedGradeBL packedGradeBL()
        {
            IpackedGradeBL obj = new packedGradeBL();
            return obj;
        }

        public static IpdBL pdBL()
        {
            IpdBL obj = new pdBL();
            return obj;
        }

        public static IpdsetupBL pdsetupBL()
        {
            IpdsetupBL obj = new pdsetupBL();
            return obj;
        }

        public static IUserAccountBL UserAccountBL()
        {
            IUserAccountBL obj = new UserAccountBL();
            return obj;
        }
    }
}
