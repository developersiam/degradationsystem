﻿using DegradationSystemBLL.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DegradationSystemBLL.Helper
{
    public static class pdsetupHelper
    {
        public static List<m_pdsetup> GetByCrop(short crop)
        {
            return (from a in BLService.pdsetupBL().GetByCrop(crop)
                    from b in BLService.packedGradeBL().GetByCrop(crop)
                    where a.crop == b.crop && a.packedgrade == b.packedgrade1
                    select new m_pdsetup
                    {
                        date = a.date,
                        pdno = a.pdno,
                        crop = a.crop,
                        type = b.type,
                        customer = b.customer,
                        packedgrade = a.packedgrade,
                        def = a.def
                    })
                    .ToList();
        }

        public static List<m_pdsetup> GetByCurrentDateOrDefault()
        {
            var pdsetupList = BLService.pdsetupBL().GetByDateOrDefult();
            var minCrop = (int)pdsetupList.Min(x => x.crop);
            var maxCrop = (int)pdsetupList.Max(x => x.crop);

            return (from a in pdsetupList
                    from b in BLService.packedGradeBL().GetByCropRange(minCrop, maxCrop)
                    where a.crop == b.crop && a.packedgrade == b.packedgrade1
                    select new m_pdsetup
                    {
                        date = a.date,
                        pdno = a.pdno,
                        crop = a.crop,
                        type = b.type,
                        customer = b.customer,
                        packedgrade = a.packedgrade,
                        def = a.def
                    })
                    .ToList();
        }
    }
}
