﻿Module DegradationSystemModule
    Public crop As Integer
    Public currentUser As String
    Public userRole As String

    Public Comport As String = My.Settings.Comport
    Public BaudRate As Integer = My.Settings.BaudRate
    Public DataBits As Short = My.Settings.DataBits
    Public Parity As String = IIf(My.Settings.Parity = "None", System.IO.Ports.Parity.None, IIf(My.Settings.Parity = "Odd", System.IO.Ports.Parity.Odd, IIf(My.Settings.Parity = "Even", System.IO.Ports.Parity.Even, IIf(My.Settings.Parity = "Mark", System.IO.Ports.Parity.Mark, System.IO.Ports.Parity.Space))))
    Public StopBits As String = IIf(My.Settings.StopBits = "None", System.IO.Ports.StopBits.None, IIf(My.Settings.StopBits = "One", System.IO.Ports.StopBits.One, IIf(My.Settings.StopBits = "Two", System.IO.Ports.StopBits.Two, System.IO.Ports.StopBits.OnePointFive)))

End Module
