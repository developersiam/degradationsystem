﻿Imports MahApps.Metro.Controls.Dialogs
Class MainWindow

    'Private Sub SystemAdminButton_Click(sender As Object, e As RoutedEventArgs) Handles SystemAdminButton.Click
    '    Try
    '        If userRole <> "Admin" Then
    '            MessageBox.Show("สิทธิ์ของท่านไม่ได้รับอนุญาตให้เข้าใช้งานเมนูนี้", "Notify", MessageBoxButton.OK, MessageBoxImage.Warning)
    '            Return
    '        End If

    '        Dim systemAdminUserAccount As New SystemAdminUserAccount

    '        systemAdminUserAccount.ShowDialog()
    '    Catch ex As Exception
    '        MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning)
    '    End Try
    'End Sub

    'Private Sub LogOffButton_Click(sender As Object, e As RoutedEventArgs) Handles LogOffButton.Click
    '    Try
    '        Me.Close()
    '    Catch ex As Exception
    '        MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning)
    '    End Try
    'End Sub

    'Private Sub CustomerSpecButton_Click(sender As Object, e As RoutedEventArgs) Handles CustomerSpecButton.Click
    '    Try
    '        If userRole <> "QCStaff" And userRole <> "Admin" Then
    '            MessageBox.Show("สิทธิ์ของท่านไม่ได้รับอนุญาตให้เข้าใช้งานเมนูนี้", "Notify", MessageBoxButton.OK, MessageBoxImage.Warning)
    '            Return
    '        End If

    '        Dim supervisorCustomerSpecList As New SupervisorCustomerSpecList

    '        supervisorCustomerSpecList.ShowDialog()
    '    Catch ex As Exception
    '        MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning)
    '    End Try
    'End Sub

    'Private Sub StemContentButton_Click(sender As Object, e As RoutedEventArgs) Handles StemContentButton.Click
    '    Try
    '        If userRole <> "DegradationStaff" And userRole <> "QCStaff" And userRole <> "Admin" Then
    '            MessageBox.Show("สิทธิ์ของท่านไม่ได้รับอนุญาตให้เข้าใช้งานเมนูนี้", "Notify", MessageBoxButton.OK, MessageBoxImage.Warning)
    '            Return
    '        End If

    '        Dim staffSelectTestCaseForInputStemContent As New StaffSelectTestCaseForInputStemContent

    '        staffSelectTestCaseForInputStemContent.ShowDialog()
    '    Catch ex As Exception
    '        MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning)
    '    End Try
    'End Sub

    'Private Sub SizeContentButton_Click(sender As Object, e As RoutedEventArgs) Handles SizeContentButton.Click
    '    Try
    '        If userRole <> "DegradationStaff" And userRole <> "QCStaff" And userRole <> "Admin" Then
    '            MessageBox.Show("สิทธิ์ของท่านไม่ได้รับอนุญาตให้เข้าใช้งานเมนูนี้", "Notify", MessageBoxButton.OK, MessageBoxImage.Warning)
    '            Return
    '        End If

    '        Dim staffInputSizeSample As New StaffInputSizeContent

    '        staffInputSizeSample.ShowDialog()
    '    Catch ex As Exception
    '        MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning)
    '    End Try
    'End Sub

    'Private Sub TechnicialButton_Click(sender As Object, e As RoutedEventArgs) Handles TechnicialButton.Click
    '    Try
    '        Dim technicialReport As New TechnicianReport

    '        technicialReport.ShowDialog()
    '    Catch ex As Exception
    '        MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning)
    '    End Try
    'End Sub

    'Private Sub MetroWindow_Loaded(sender As Object, e As RoutedEventArgs)
    '    Try
    '        DisplayNameTextBlock.Text = currentUser
    '    Catch ex As Exception
    '        MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning)
    '    End Try
    'End Sub

    'Private Sub OlderDataInputButton_Click(sender As Object, e As RoutedEventArgs) Handles OlderDataInputButton.Click
    '    Try
    '        If userRole <> "QCStaff" And userRole <> "Admin" Then
    '            MessageBox.Show("สิทธิ์ของท่านไม่ได้รับอนุญาตให้เข้าใช้งานเมนูนี้", "Notify", MessageBoxButton.OK, MessageBoxImage.Warning)
    '            Return
    '        End If

    '        Dim supervisorOlderDataInput As New SupervisorOlderDataInput

    '        supervisorOlderDataInput.ShowDialog()
    '    Catch ex As Exception
    '        MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning)
    '    End Try
    'End Sub
End Class
