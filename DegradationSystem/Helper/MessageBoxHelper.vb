﻿Public Class MessageBoxHelper
    Public Shared Sub Warning(message As String)
        MessageBox.Show(message, "warning!", MessageBoxButton.OK, MessageBoxImage.Warning)
        Return
    End Sub

    Public Shared Sub Danger(message As String)
        MessageBox.Show(message, "danger!", MessageBoxButton.OK, MessageBoxImage.Error)
        Return
    End Sub

    Public Shared Sub Info(message As String)
        MessageBox.Show(message, "info", MessageBoxButton.OK, MessageBoxImage.Information)
        Return
    End Sub

    Public Shared Function Question(message As String) As MessageBoxResult
        Dim result = MessageBox.Show(message, "please confirm!", MessageBoxButton.YesNo, MessageBoxImage.Question)
        If result = MessageBoxResult.Yes Then
            Return MessageBoxResult.Yes
        Else
            Return MessageBoxResult.No
        End If
    End Function

    Public Shared Sub Exception(ex As Exception)
        If ex.InnerException Is Nothing Then
            MessageBox.Show(ex.Message, "warning!", MessageBoxButton.OK, MessageBoxImage.Warning)
        Else
            MessageBox.Show(ex.InnerException.Message, "warning!", MessageBoxButton.OK, MessageBoxImage.Warning)
        End If
        Return
    End Sub
End Class
