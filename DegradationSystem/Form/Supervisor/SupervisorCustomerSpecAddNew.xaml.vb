﻿Imports MahApps.Metro.Controls
Public Class SupervisorCustomerSpecAddNew

    Private _crop As Integer
    Private _customer As String

    Public Property Customer() As String
        Get
            Return _customer
        End Get
        Set(ByVal value As String)
            _customer = value
        End Set
    End Property

    Public Property Crop() As Integer
        Get
            Return _crop
        End Get
        Set(ByVal value As Integer)
            _crop = value
        End Set
    End Property

    Private Sub MetroWindow_Loaded(sender As Object, e As RoutedEventArgs)
        Try
            DisplayNameTextBlock.Text = currentUser

            CropTextBox.Text = _crop
            CustomerTextBox.Text = _customer

            Dim packGradeObj As New PackGrade

            PackGradeComboBox.ItemsSource = packGradeObj.GetPackGrade(_crop).Where(Function(x) x.customer = _customer)
        Catch ex As Exception
            MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning)
        End Try
    End Sub

    Private Sub AddNewCustomerSpecButton_Click(sender As Object, e As RoutedEventArgs) Handles AddNewCustomerSpecButton.Click
        Try
            Dim comboBoxItem As ComboBoxItem = ExcelTypeComboBox.SelectedItem

            If ExcelTypeComboBox.SelectedIndex = -1 Then
                MessageBox.Show("โปรดเลือก Excel form ที่จะใช้ในการ Export ข้อมูลด้วยค่ะ!", "Warning", MessageBoxButton.OK, MessageBoxImage.Warning)
                Return
            End If

            If PackGradeComboBox.SelectedIndex = -1 Then
                MessageBox.Show("โปรดเลือก Pack grade ด้วยค่ะ!", "Warning", MessageBoxButton.OK, MessageBoxImage.Warning)
                Return
            End If

            Dim customerSpec As New CustomerSpecBL

            If customerSpec.IsCustomerSpecification(_crop, _customer, PackGradeComboBox.SelectedValue) = True Then
                MessageBox.Show("สเปกลูกค้าเกรดนี้มีอยู่ในระบบก่อนหน้านี้แล้ว กรุณาตรวจสอบอีกครั้งค่ะ!", "Warning", MessageBoxButton.OK, MessageBoxImage.Warning)
                Return
            End If

            customerSpec.Crop = CropTextBox.Text
            customerSpec.Customer = CustomerTextBox.Text
            customerSpec.PackGrade = PackGradeComboBox.SelectedValue
            customerSpec.TargetCor = TargetCoreTextBox.Text
            customerSpec.TypeOfExcel = comboBoxItem.Content.ToString

            customerSpec.Over1Target = Over1TargetTextBox.Text
            customerSpec.Over1Min = Over1MinTextBox.Text
            customerSpec.Over1Max = Over1MaxTextBox.Text

            customerSpec.Over12Target = Over12TargetTextBox.Text
            customerSpec.Over12Min = Over12MinTextBox.Text
            customerSpec.Over12Max = Over12MaxTextBox.Text

            customerSpec.Over14Target = Over14TargetTextBox.Text
            customerSpec.Over14Min = Over14MinTextBox.Text
            customerSpec.Over14Max = Over14MaxTextBox.Text

            customerSpec.Over18Target = Over18TargetTextBox.Text
            customerSpec.Over18Min = Over18MinTextBox.Text
            customerSpec.Over18Max = Over18MaxTextBox.Text

            customerSpec.TOT12Target = Total12TargetTextBox.Text
            customerSpec.TOT12Min = Total12MinTextBox.Text
            customerSpec.TOT12Max = Total12MaxTextBox.Text

            customerSpec.TOT14Target = Total14TargetTextBox.Text
            customerSpec.TOT14Min = Total14MinTextBox.Text
            customerSpec.TOT14Max = Total14MaxTextBox.Text

            customerSpec.Thru14Target = Thru14TargetTextBox.Text
            customerSpec.Thru14Min = Thru14MinTextBox.Text
            customerSpec.Thru14Max = Thru14MaxTextBox.Text

            customerSpec.PanTarget = PanTargetTextBox.Text
            customerSpec.PanMin = PanMinTextBox.Text
            customerSpec.PanMax = PanMaxTextBox.Text


            customerSpec.StemOBJTarget = OBJTargetTextBox.Text
            customerSpec.StemOBJMin = OBJMinTextBox.Text
            customerSpec.StemOBJMax = OBJMaxTextBox.Text

            customerSpec.Stem7Target = Stem7SlotTargetTextBox.Text
            customerSpec.Stem7Min = Stem7SlotMinTextBox.Text
            customerSpec.Stem7Max = Stem7SlotMaxTextBox.Text

            customerSpec.Stem12Target = Stem12SlotTargetTextBox.Text
            customerSpec.Stem12Min = Stem12SlotMinTextBox.Text
            customerSpec.Stem12Max = Stem12SlotMaxTextBox.Text

            customerSpec.StemFiberTarget = StemFiberTargetTextBox.Text
            customerSpec.StemFiberMin = StemFiberMinTextBox.Text
            customerSpec.StemFiberMax = StemFiberMaxTextBox.Text

            customerSpec.TOTStemTarget = TotalStemTargetTextBox.Text
            customerSpec.TOTStemMin = TotalStemMinTextBox.Text
            customerSpec.TOTStemMax = TotalStemMaxTextBox.Text

            customerSpec.SieveUser = currentUser
            customerSpec.ModifiedDate = Now

            customerSpec.Add(customerSpec)

            MessageBox.Show("บันทึกข้อมูลสำเร็จ!", "Warning", MessageBoxButton.OK, MessageBoxImage.Information)

            Me.Close()
        Catch ex As Exception
            MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning)
        End Try
    End Sub
End Class
