﻿Public Class SupervisorOlderDataInputSelectCaseNoList

    Private _pdNo As String
    Private _caseNo As pd
    Public Property CaseNo() As pd
        Get
            Return _caseNo
        End Get
        Set(ByVal value As pd)
            _caseNo = value
        End Set
    End Property

    Public Property pdno() As String
        Get
            Return _pdNo
        End Get
        Set(ByVal value As String)
            _pdNo = value
        End Set
    End Property


    Private Sub pdListDataBinding()
        Try
            Dim pdObject As New pd

            CaseNumberDataGrid.ItemsSource = Nothing
            CaseNumberDataGrid.ItemsSource = pdObject.GetPdByPdno(_pdNo)
        Catch ex As Exception
            MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning)
        End Try
    End Sub

    Private Sub RefreshButton_Click(sender As Object, e As RoutedEventArgs) Handles RefreshButton.Click
        Try
            pdListDataBinding()
        Catch ex As Exception
            MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning)
        End Try
    End Sub

    Private Sub MetroWindow_Loaded(sender As Object, e As RoutedEventArgs)
        Try
            pdListDataBinding()
        Catch ex As Exception
            MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning)
        End Try
    End Sub

    Private Sub CaseNumberDataGrid_MouseUp(sender As Object, e As MouseButtonEventArgs) Handles CaseNumberDataGrid.MouseUp
        Try
            If CaseNumberDataGrid.SelectedIndex > -1 Then

                _caseNo = CaseNumberDataGrid.SelectedItem

                Me.Close()
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning)
        End Try
    End Sub
End Class
