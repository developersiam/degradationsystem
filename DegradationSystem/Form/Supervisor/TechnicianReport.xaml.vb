﻿Public Class TechnicianReport

    Private Sub MetroWindow_Loaded(sender As Object, e As RoutedEventArgs)
        Try
            DisplayNameTextBlock.Text = currentUser

            FromDatePicker.Text = Now
            ToDatePicker.Text = Now
        Catch ex As Exception
            MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning)
        End Try
    End Sub

    Private Sub FromDatePicker_SelectedDateChanged(sender As Object, e As SelectionChangedEventArgs) Handles FromDatePicker.SelectedDateChanged
        Try
            If ToDatePicker.Text = "" Then
                Return
            End If

            If FromDatePicker.Text <> "" And ToDatePicker.Text <> "" Then
                Dim degradationTestCase As New DegradationTestCase

                CustomerComboBox.IsEnabled = True

                CustomerComboBox.ItemsSource = Nothing
                CustomerComboBox.ItemsSource = degradationTestCase.GetCustomerFromStartAndToDate(FromDatePicker.Text, ToDatePicker.Text).OrderBy(Function(cus) cus.CustomerName)

                PackGradeComboBox.ItemsSource = Nothing

                DisplayDataButton.IsEnabled = False
                ViewTargetButton.IsEnabled = False
                ViewReportButton.IsEnabled = False
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning)
        End Try
    End Sub

    Private Sub ToDatePicker_SelectedDateChanged(sender As Object, e As SelectionChangedEventArgs) Handles ToDatePicker.SelectedDateChanged
        Try
            If FromDatePicker.Text = "" Then
                Return
            End If

            If FromDatePicker.Text <> "" And ToDatePicker.Text <> "" Then
                Dim degradationTestCase As New DegradationTestCase

                CustomerComboBox.IsEnabled = True

                CustomerComboBox.ItemsSource = Nothing
                CustomerComboBox.ItemsSource = degradationTestCase.GetCustomerFromStartAndToDate(FromDatePicker.Text, ToDatePicker.Text).OrderBy(Function(cus) cus.CustomerName)

                PackGradeComboBox.ItemsSource = Nothing

                DisplayDataButton.IsEnabled = False
                ViewTargetButton.IsEnabled = False
                ViewReportButton.IsEnabled = False
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning)
        End Try
    End Sub

    Private Sub CustomerComboBox_SelectionChanged(sender As Object, e As SelectionChangedEventArgs) Handles CustomerComboBox.SelectionChanged
        Try
            If CustomerComboBox.SelectedValue = "" Then
                Return
            End If

            Dim testCase As New DegradationTestCase

            PackGradeComboBox.IsEnabled = True

            DisplayDataButton.IsEnabled = False
            ViewTargetButton.IsEnabled = False
            ViewReportButton.IsEnabled = False

            PackGradeComboBox.ItemsSource = Nothing
            PackGradeComboBox.ItemsSource = testCase.GetPackGradeFromTestCaseByCustomer(CustomerComboBox.SelectedValue, FromDatePicker.Text, ToDatePicker.Text)
        Catch ex As Exception
            MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning)
        End Try
    End Sub

    Private Sub PackGradeComboBox_SelectionChanged(sender As Object, e As SelectionChangedEventArgs) Handles PackGradeComboBox.SelectionChanged
        Try
            If PackGradeComboBox.SelectedValue = "" Then
                Return
            End If

            Dim testCase As New DegradationTestCase

            DisplayDataButton.IsEnabled = True
            ViewTargetButton.IsEnabled = True
            ViewReportButton.IsEnabled = True
        Catch ex As Exception
            MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning)
        End Try
    End Sub

    Private Sub DisplayDataButton_Click(sender As Object, e As RoutedEventArgs) Handles DisplayDataButton.Click
        Try
            Dim testCase As New DegradationTestCase

            CaseNumberDataGrid.ItemsSource = Nothing
            CaseNumberDataGrid.ItemsSource = testCase.GetTestCaseByPackGrade(FromDatePicker.Text, ToDatePicker.Text, CustomerComboBox.SelectedValue, PackGradeComboBox.SelectedValue).OrderByDescending(Function(x) x.Ddate).ThenByDescending(Function(x) x.Ttime)

            ViewTargetButton.IsEnabled = True
            ViewReportButton.IsEnabled = True
        Catch ex As Exception
            MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning)
        End Try
    End Sub

    Private Sub ViewTargetButton_Click(sender As Object, e As RoutedEventArgs) Handles ViewTargetButton.Click
        Try
            Dim supervisorCustomerSpecEdit As New SupervisorCustomerSpecEdit

            Dim customerSpec As New CustomerSpecBL

            customerSpec = customerSpec.GetByPackGrade(Convert.ToDateTime(ToDatePicker.Text).Year, CustomerComboBox.SelectedValue, PackGradeComboBox.SelectedValue)

            supervisorCustomerSpecEdit.CustomerSpec = customerSpec

            supervisorCustomerSpecEdit.ShowDialog()
        Catch ex As Exception
            MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning)
        End Try
    End Sub

    Private Sub ViewReportButton_Click(sender As Object, e As RoutedEventArgs) Handles ViewReportButton.Click
        Try
            Dim lineChartReport As New LineChartReport

            lineChartReport.FromDate = FromDatePicker.Text
            lineChartReport.ToDate = ToDatePicker.Text
            lineChartReport.Customer = CustomerComboBox.SelectedValue
            lineChartReport.PackGrade = PackGradeComboBox.SelectedValue

            lineChartReport.ShowDialog()
        Catch ex As Exception
            MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning)
        End Try
    End Sub
End Class
