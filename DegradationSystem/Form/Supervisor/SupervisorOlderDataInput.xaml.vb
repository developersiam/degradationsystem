﻿Imports DegradationSystemBLL.GeneratedCode

Public Class SupervisorOlderDataInput

    Private Sub CaseNumberButton_Click(sender As Object, e As RoutedEventArgs) Handles CaseNumberButton.Click
        Try
            If PdNoTextBox.Text = "" Then
                MessageBox.Show("โปรดระบุ Production number โดยคลิปที่ช่อง TextBox ด้านบน!", "Information", MessageBoxButton.OK, MessageBoxImage.Warning)
                Return
            End If

            Dim supervisorChooseCaseNo As New SupervisorOlderDataInputSelectCaseNoList

            supervisorChooseCaseNo.pdno = PdNoTextBox.Text

            supervisorChooseCaseNo.ShowDialog()

            If supervisorChooseCaseNo.CaseNo Is Nothing Then
                MessageBox.Show("ไม่พบข้อมูลหมายเลข Case No ที่ท่านเลือก โปรดลองใหม่อีกครั้ง!", "Information", MessageBoxButton.OK, MessageBoxImage.Information)
                Return
            End If

            Dim caseNoObject As New pd

            caseNoObject = supervisorChooseCaseNo.CaseNo

            CaseNoLabel.Content = caseNoObject.caseno
            CropLabel.Content = caseNoObject.crop
            CustomerLabel.Content = caseNoObject.customer
            PackGradeLabel.Content = caseNoObject.grade
            ProductionNumberLabel.Content = caseNoObject.frompdno
            PackingDateLabel.Content = caseNoObject.packingdate
            PackingTimeLabel.Content = caseNoObject.packingtime

            Dim customerSpec As New CustomerSpecBL

            If customerSpec.IsCustomerSpecification(caseNoObject.crop, caseNoObject.customer, caseNoObject.grade) = False Then
                MessageBox.Show("ไม่พบข้อมูลสเปกลูกค้าเกรดนี้ โปรดติดต่อหัวหน้างานเพื่อเพิ่มข้อมูลสเปกลูกค้าเกรดนี้เข้าในระบบ!", "ข้อความแจ้งเตือน", MessageBoxButton.OK, MessageBoxImage.Warning)
                Return
            End If

            customerSpec = customerSpec.GetByPackGrade(caseNoObject.crop, caseNoObject.customer, caseNoObject.grade)

            ExcelTypeLabel.Content = customerSpec.TypeOfExcel
        Catch ex As Exception
            MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning)
        End Try
    End Sub

    Private Sub HoursTextBox_GotFocus(sender As Object, e As RoutedEventArgs) Handles HoursTextBox.GotFocus
        Try
            TimeModeLable.Content = "HH"

            HoursTextBox.Background = Brushes.LightGreen
            MinuteTextBox.Background = Brushes.White
        Catch ex As Exception
            MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning)
        End Try
    End Sub

    Private Sub MinuteTextBox_GotFocus(sender As Object, e As RoutedEventArgs) Handles MinuteTextBox.GotFocus
        Try
            TimeModeLable.Content = "MM"

            HoursTextBox.Background = Brushes.White
            MinuteTextBox.Background = Brushes.LightGreen
        Catch ex As Exception
            MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning)
        End Try
    End Sub

    Private Sub MinusTimeButton_Click(sender As Object, e As RoutedEventArgs) Handles MinusTimeButton.Click
        Try
            If TimeModeLable.Content = "HH" Then

                HoursTextBox.Text = HoursTextBox.Text - 1

                If HoursTextBox.Text.Length < 2 Then
                    HoursTextBox.Text = "0" & HoursTextBox.Text
                End If

            Else

                MinuteTextBox.Text = MinuteTextBox.Text - 10

                If MinuteTextBox.Text.Length < 2 Then
                    MinuteTextBox.Text = "0" & MinuteTextBox.Text
                End If
            End If

            If HoursTextBox.Text < 0 Then
                'MessageBox.Show("ตัวเลขชั่วโมงจะต้องไม่น้อยกว่า 0 โปรดลองใหม่อีกครั้ง", "การแจ้งเตือน", MessageBoxButton.OK, MessageBoxImage.Information)
                HoursTextBox.Text = "23"
                Return
            End If

            If MinuteTextBox.Text < 0 Then
                'MessageBox.Show("ตัวเลขชั่วโมงจะต้องไม่น้อยกว่า 0 โปรดลองใหม่อีกครั้ง", "การแจ้งเตือน", MessageBoxButton.OK, MessageBoxImage.Information)
                MinuteTextBox.Text = "50"
                Return
            End If


        Catch ex As Exception
            MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning)
        End Try
    End Sub

    Private Sub AddTimeButton_Click(sender As Object, e As RoutedEventArgs) Handles AddTimeButton.Click
        Try
            If TimeModeLable.Content = "HH" Then

                HoursTextBox.Text = HoursTextBox.Text + 1

                If HoursTextBox.Text.Length < 2 Then
                    HoursTextBox.Text = "0" & HoursTextBox.Text
                End If
            Else

                MinuteTextBox.Text = MinuteTextBox.Text + 10

                If MinuteTextBox.Text.Length < 2 Then
                    MinuteTextBox.Text = "0" & MinuteTextBox.Text
                End If
            End If

            If HoursTextBox.Text > 23 Then
                'MessageBox.Show("ตัวเลขชั่วโมงจะต้องไม่เกิน 23 โปรดลองใหม่อีกครั้ง", "การแจ้งเตือน", MessageBoxButton.OK, MessageBoxImage.Information)
                HoursTextBox.Text = "00"
                Return
            End If

            If MinuteTextBox.Text > 50 Then
                'MessageBox.Show("ตัวเลขนาทีจะต้องไม่เกิน 40 โปรดลองใหม่อีกครั้ง", "การแจ้งเตือน", MessageBoxButton.OK, MessageBoxImage.Information)
                MinuteTextBox.Text = "00"
                Return
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning)
        End Try
    End Sub

    Private Sub Over1Button_Click(sender As Object, e As RoutedEventArgs) Handles Over1Button.Click
        Try
            Over1Button.Foreground = Brushes.Red
            Over12Button.Foreground = Brushes.Black
            Over14Button.Foreground = Brushes.Black
            Over18Button.Foreground = Brushes.Black
            PanButton.Foreground = Brushes.Black

            SizeLable.Content = "Over 1"

            WeightTextBox.Text = ""
            WeightTextBox.Focus()
            OkButton.IsEnabled = True
        Catch ex As Exception
            MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning)
        End Try
    End Sub

    Private Sub Over12Button_Click(sender As Object, e As RoutedEventArgs) Handles Over12Button.Click
        Try
            Over1Button.Foreground = Brushes.Black
            Over12Button.Foreground = Brushes.Red
            Over14Button.Foreground = Brushes.Black
            Over18Button.Foreground = Brushes.Black
            PanButton.Foreground = Brushes.Black

            SizeLable.Content = "Over 1/2"

            WeightTextBox.Text = ""
            WeightTextBox.Focus()

            OkButton.IsEnabled = True
        Catch ex As Exception
            MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning)
        End Try
    End Sub

    Private Sub Over14Button_Click(sender As Object, e As RoutedEventArgs) Handles Over14Button.Click
        Try
            Over1Button.Foreground = Brushes.Black
            Over12Button.Foreground = Brushes.Black
            Over14Button.Foreground = Brushes.Red
            Over18Button.Foreground = Brushes.Black
            PanButton.Foreground = Brushes.Black

            SizeLable.Content = "Over 1/4"

            WeightTextBox.Text = ""
            WeightTextBox.Focus()

            OkButton.IsEnabled = True
        Catch ex As Exception
            MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning)
        End Try
    End Sub

    Private Sub Over18Button_Click(sender As Object, e As RoutedEventArgs) Handles Over18Button.Click
        Try
            Over1Button.Foreground = Brushes.Black
            Over12Button.Foreground = Brushes.Black
            Over14Button.Foreground = Brushes.Black
            Over18Button.Foreground = Brushes.Red
            PanButton.Foreground = Brushes.Black

            SizeLable.Content = "Over 1/8"

            WeightTextBox.Text = ""
            WeightTextBox.Focus()

            OkButton.IsEnabled = True
        Catch ex As Exception
            MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning)
        End Try
    End Sub

    Private Sub PanButton_Click(sender As Object, e As RoutedEventArgs) Handles PanButton.Click
        Try
            Over1Button.Foreground = Brushes.Black
            Over12Button.Foreground = Brushes.Black
            Over14Button.Foreground = Brushes.Black
            Over18Button.Foreground = Brushes.Black
            PanButton.Foreground = Brushes.Red

            SizeLable.Content = "Pan"

            WeightTextBox.Text = ""
            WeightTextBox.Focus()

            OkButton.IsEnabled = True
        Catch ex As Exception
            MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning)
        End Try
    End Sub

    Private Sub OkButton_Click(sender As Object, e As RoutedEventArgs) Handles OkButton.Click
        Try
            If WeightTextBox.Text = "" Then
                MessageBox.Show("ไม่มีข้อมูลน้ำหนัก ไม่สามารถบันทึกได้!", "Warning", MessageBoxButton.OK, MessageBoxImage.Warning)
                Return
            End If

            If SizeLable.Content = "Over 1" Then
                Over1Label.Content = WeightTextBox.Text
            End If

            If SizeLable.Content = "Over 1/2" Then
                Over12Label.Content = WeightTextBox.Text
            End If

            If SizeLable.Content = "Over 1/4" Then
                Over14Label.Content = WeightTextBox.Text
            End If

            If SizeLable.Content = "Over 1/8" Then
                Over18Label.Content = WeightTextBox.Text
            End If

            If SizeLable.Content = "Pan" Then
                PanLabel.Content = WeightTextBox.Text
            End If


            If Over1Label.Content <> "" And Over12Label.Content <> "" And Over14Label.Content <> "" And Over18Label.Content <> "" And PanLabel.Content <> "" Then
                SendDataButton.IsEnabled = True
            End If

            OkButton.IsEnabled = False

            Over1Button.Foreground = Brushes.Black
            Over12Button.Foreground = Brushes.Black
            Over14Button.Foreground = Brushes.Black
            Over18Button.Foreground = Brushes.Black
            PanButton.Foreground = Brushes.Black

            SizeLable.Content = "Size"

        Catch ex As Exception
            MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning)
        End Try
    End Sub

    Private Sub SendDataButton_Click(sender As Object, e As RoutedEventArgs) Handles SendDataButton.Click
        Try
            '**** Save test case data into the database.
            If IsNothing(CaseNoLabel.Content) Then
                MessageBox.Show("โปรดเลือก Caseno ที่ต้องการบันทึกข้อมูล!", "Warning", MessageBoxButton.OK, MessageBoxImage.Warning)
                Return
            End If

            Dim customerSpecObj As New CustomerSpecBL

            If customerSpecObj.IsCustomerSpecification(CropLabel.Content, CustomerLabel.Content, PackGradeLabel.Content) = False Then
                MessageBox.Show("ไม่พบข้อมูลสเปกลูกค้าเกรดนี้ โปรดติดต่อหัวหน้างานเพื่อเพิ่มข้อมูลสเปกลูกค้าเกรดนี้เข้าในระบบ!")
                Return
            End If

            Dim testCaseObj As New DegradationTestCase

            If testCaseObj.IsTestCase(CropLabel.Content, ProductionNumberLabel.Content, Convert.ToInt16(CaseNoLabel.Content)) = True Then
                MessageBox.Show("Caseno นี้ถูกบันทึกเข้าไปในระบบแล้ว ไม่อนุญาตให้บันทึกซ้ำได้ โปรดเลือก caseno อื่น!", "Warning", MessageBoxButton.OK, MessageBoxImage.Warning)
                Return
            End If

            Dim tTime As TimeSpan = TimeSpan.Parse(HoursTextBox.Text & ":" & MinuteTextBox.Text)

            If testCaseObj.GetTestCaseByPackGrade(PackingDateLabel.Content, PackingDateLabel.Content, CustomerLabel.Content, PackGradeLabel.Content).Where(Function(x) x.Ttime = tTime).Count() > 0 Then
                MessageBox.Show("เวลาในการสุ่มที่ท่านระบุซ้ำกับเวลาที่เคยระบุไปแล้วก่อนหน้านี้ โปรดเปลี่ยนเวลาการเก็บตัวอย่าง แล้วลองบันทึกข้อมูลใหม่อีกครั้ง!", "Warning", MessageBoxButton.OK, MessageBoxImage.Warning)
                Return
            End If

            testCaseObj.InputSizeContent(ProductionNumberLabel.Content, CaseNoLabel.Content, CropLabel.Content, CustomerLabel.Content, PackGradeLabel.Content, tTime, Convert.ToDateTime(PackingDateLabel.Content), Over1Label.Content, Over12Label.Content, Over14Label.Content, Over18Label.Content, PanLabel.Content, currentUser)

            LastSendCaseNoLabel.Content = CaseNoLabel.Content.ToString
            LastSendTestTimeLabel.Content = HoursTextBox.Text & " : " & MinuteTextBox.Text
            LastSendPackingDateLabel.Content = PackingDateLabel.Content
            LastSendProductionNumberLabel.Content = ProductionNumberLabel.Content

            LastSendOver1Label.Content = Over1Label.Content
            LastSendOver12Label.Content = Over12Label.Content
            LastSendOver14Label.Content = Over14Label.Content
            LastSendOver18Label.Content = Over18Label.Content
            LastSendPanLabel.Content = PanLabel.Content
            LastSendSendTimeLabel.Content = Now

            CaseNoLabel.Content = Nothing
            CropLabel.Content = ""
            CustomerLabel.Content = ""
            PackGradeLabel.Content = ""
            ProductionNumberLabel.Content = ""
            ExcelTypeLabel.Content = ""
            PackingDateLabel.Content = ""
            PackingTimeLabel.Content = ""

            Over1Label.Content = ""
            Over12Label.Content = ""
            Over14Label.Content = ""
            Over18Label.Content = ""
            PanLabel.Content = ""

            Over1Button.Foreground = Brushes.Black
            Over12Button.Foreground = Brushes.Black
            Over14Button.Foreground = Brushes.Black
            Over18Button.Foreground = Brushes.Black
            PanButton.Foreground = Brushes.Black

            SizeLable.Content = "Size"

            SendDataButton.IsEnabled = False
        Catch ex As Exception
            MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning)
        End Try
    End Sub

    Private Sub MetroWindow_Loaded(sender As Object, e As RoutedEventArgs)
        Try
            DisplayNameTextBlock.Text = currentUser

            TimeModeLable.Content = "MM"

            HoursTextBox.Background = Brushes.White
            MinuteTextBox.Background = Brushes.LightGreen
        Catch ex As Exception
            MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning)
        End Try
    End Sub

    Private Sub PdNoTextBox_GotFocus(sender As Object, e As RoutedEventArgs) Handles PdNoTextBox.GotFocus
        Try
            Dim supervisorSelectPdSetupList As New SupervisorPdSetupList

            supervisorSelectPdSetupList.ShowDialog()

            If supervisorSelectPdSetupList.pdSetup Is Nothing Then
                MessageBox.Show("ไม่พบข้อมูล Production number ที่ท่านเลือก โปรดลองใหม่อีกครั้ง!", "Information", MessageBoxButton.OK, MessageBoxImage.Warning)
                Return
            End If

            PdNoTextBox.Text = supervisorSelectPdSetupList.pdSetup.pdno

        Catch ex As Exception
            MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning)
        End Try
    End Sub
End Class
