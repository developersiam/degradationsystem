﻿Imports DegradationSystemBLL.GeneratedCode

Public Class SupervisorPdSetupList

    Private _pdSetup As pdSetup
    Public Property pdSetup() As pdSetup
        Get
            Return _pdSetup
        End Get
        Set(ByVal value As pdSetup)
            _pdSetup = value
        End Set
    End Property


    Private Sub MetroWindow_Loaded(sender As Object, e As RoutedEventArgs)
        Try
            Dim pdsetupObject As New pdSetup

            PdSetupListDataGrid.ItemsSource = pdsetupObject.GetPdSetupByCrop(crop)

        Catch ex As Exception
            MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning)
        End Try
    End Sub

    Private Sub RefreshButton_Click(sender As Object, e As RoutedEventArgs) Handles RefreshButton.Click
        Try
            Dim pdsetupObject As New pdSetup

            PdSetupListDataGrid.ItemsSource = pdsetupObject.GetPdSetupByCrop(crop)

        Catch ex As Exception
            MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning)
        End Try
    End Sub

    Private Sub PdSetupListDataGrid_MouseUp(sender As Object, e As MouseButtonEventArgs) Handles PdSetupListDataGrid.MouseUp
        Try
            If PdSetupListDataGrid.SelectedIndex > -1 Then

                _pdSetup = PdSetupListDataGrid.SelectedItem

                Me.Close()
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning)
        End Try
    End Sub

End Class
