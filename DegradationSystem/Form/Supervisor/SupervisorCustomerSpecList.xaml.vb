﻿Public Class SupervisorCustomerSpecList

    Sub CustomerSpecDataBinding()
        Try
            Dim customerSpecOBJ As New CustomerSpecBL
            Dim customerSpecList As New List(Of CustomerSpecBL)

            CustomerSpecDataGrid.ItemsSource = Nothing

            customerSpecList = customerSpecOBJ.GetByCustomer(CropTextBox.Text, CustomerComboBox.SelectedValue)

            If customerSpecList.Count < 1 Then
                MessageBox.Show("ไม่พบรายการข้อมูล", "Information", MessageBoxButton.OK, MessageBoxImage.Information)
                Return
            End If

            CustomerSpecDataGrid.ItemsSource = customerSpecList
        Catch ex As Exception
            MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning)
        End Try
    End Sub

    Private Sub MetroWindow_Loaded(sender As Object, e As RoutedEventArgs)
        Try
            DisplayNameTextBlock.Text = currentUser

            CropTextBox.Text = Now.Year

            Dim packgradeObj As New PackGrade
            Dim customerFromPackGradeList As New List(Of Customer)

            customerFromPackGradeList = packgradeObj.GetCustomer(CropTextBox.Text)

            If customerFromPackGradeList.Count > 0 Then

                CustomerComboBox.ItemsSource = customerFromPackGradeList
                CustomerComboBox.IsEnabled = True

            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning)
        End Try
    End Sub

    Private Sub CropTextBox_KeyUp(sender As Object, e As KeyEventArgs) Handles CropTextBox.KeyUp
        Try
            If CropTextBox.Text.Length = 4 Then
                Dim packgradeObj As New PackGrade

                CustomerComboBox.ItemsSource = packgradeObj.GetCustomer(CropTextBox.Text)
            Else
                CustomerComboBox.ItemsSource = Nothing
                DisplayDataButton.IsEnabled = False
                AddNewSpecButton.IsEnabled = False
            End If

            CustomerSpecDataGrid.ItemsSource = Nothing

        Catch ex As Exception
            MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning)
        End Try
    End Sub

    Private Sub CustomerComboBox_SelectionChanged(sender As Object, e As SelectionChangedEventArgs) Handles CustomerComboBox.SelectionChanged
        Try
            If CropTextBox.Text.Length = 4 And CustomerComboBox.SelectedValue <> "" Then
                DisplayDataButton.IsEnabled = True
                AddNewSpecButton.IsEnabled = True
            Else
                DisplayDataButton.IsEnabled = False
                AddNewSpecButton.IsEnabled = False
            End If

            CustomerSpecDataGrid.ItemsSource = Nothing

        Catch ex As Exception
            MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning)
        End Try
    End Sub

    Private Sub DisplayDataButton_Click(sender As Object, e As RoutedEventArgs) Handles DisplayDataButton.Click
        Try
            CustomerSpecDataBinding()
        Catch ex As Exception
            MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning)
        End Try
    End Sub

    Private Sub AddNewSpecButton_Click(sender As Object, e As RoutedEventArgs) Handles AddNewSpecButton.Click
        Try
            Dim supervisorCustomerSpecAddNew As New SupervisorCustomerSpecAddNew

            supervisorCustomerSpecAddNew.Crop = CropTextBox.Text
            supervisorCustomerSpecAddNew.Customer = CustomerComboBox.SelectedValue

            supervisorCustomerSpecAddNew.ShowDialog()

            CustomerSpecDataBinding()

        Catch ex As Exception
            MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning)
        End Try
    End Sub

    Private Sub CustomerSpecDataGrid_MouseDoubleClick(sender As Object, e As MouseButtonEventArgs) Handles CustomerSpecDataGrid.MouseDoubleClick
        Try
            If CustomerSpecDataGrid.SelectedIndex > -1 Then
                Dim supervisorCustomerSpecEdit As New SupervisorCustomerSpecEdit

                supervisorCustomerSpecEdit.CustomerSpec = CustomerSpecDataGrid.SelectedItem
                supervisorCustomerSpecEdit.ShowDialog()

                CustomerSpecDataBinding()
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning)
        End Try
    End Sub
End Class
