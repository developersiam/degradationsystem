﻿Imports MahApps.Metro.Controls
Public Class SupervisorCustomerSpecEdit

    Private _customerSpec As CustomerSpecBL
    Public Property CustomerSpec() As CustomerSpecBL
        Get
            Return _customerSpec
        End Get
        Set(ByVal value As CustomerSpecBL)
            _customerSpec = value
        End Set
    End Property

    Sub CustomerSpecDataBinding()
        Try
            CropTextBox.Text = _customerSpec.Crop
            CustomerTextBox.Text = _customerSpec.Customer
            PackGradeTextBox.Text = _customerSpec.PackGrade
            TargetCoreTextBox.Text = _customerSpec.TargetCor

            ExcelTypeComboBox.Items.Cast(Of ComboBoxItem).Where(Function(x) x.Content = _customerSpec.TypeOfExcel.ToString).Single().IsSelected = True
            'cmbYears.Items.Cast<ComboBoxItem>().Where(cmbi => (string) cmbi.Tag == Datetime.Today.AddYears(1).Year.ToString()).Select(a => a).Single().IsSelected = true;

            Over1TargetTextBox.Text = _customerSpec.Over1Target
            Over1MinTextBox.Text = _customerSpec.Over1Min
            Over1MaxTextBox.Text = _customerSpec.Over1Max

            Over12TargetTextBox.Text = _customerSpec.Over12Target
            Over12MinTextBox.Text = _customerSpec.Over12Min
            Over12MaxTextBox.Text = _customerSpec.Over12Max

            Over14TargetTextBox.Text = _customerSpec.Over14Target
            Over14MinTextBox.Text = _customerSpec.Over14Min
            Over14MaxTextBox.Text = _customerSpec.Over14Max

            Over18TargetTextBox.Text = _customerSpec.Over18Target
            Over18MinTextBox.Text = _customerSpec.Over18Min
            Over18MaxTextBox.Text = _customerSpec.Over18Max

            Total12TargetTextBox.Text = _customerSpec.TOT12Target
            Total12MinTextBox.Text = _customerSpec.TOT12Min
            Total12MaxTextBox.Text = _customerSpec.TOT12Max

            Total14TargetTextBox.Text = _customerSpec.TOT14Target
            Total14MinTextBox.Text = _customerSpec.TOT14Min
            Total14MaxTextBox.Text = _customerSpec.TOT14Max

            Thru14TargetTextBox.Text = _customerSpec.Thru14Target
            Thru14MinTextBox.Text = _customerSpec.Thru14Min
            Thru14MaxTextBox.Text = _customerSpec.Thru14Max

            PanTargetTextBox.Text = _customerSpec.PanTarget
            PanMinTextBox.Text = _customerSpec.PanMin
            PanMaxTextBox.Text = _customerSpec.PanMax


            OBJTargetTextBox.Text = _customerSpec.StemOBJTarget
            OBJMinTextBox.Text = _customerSpec.StemOBJMin
            OBJMaxTextBox.Text = _customerSpec.StemOBJMax

            Stem7SlotTargetTextBox.Text = _customerSpec.Stem7Target
            Stem7SlotMinTextBox.Text = _customerSpec.Stem7Min
            Stem7SlotMaxTextBox.Text = _customerSpec.Stem7Max

            Stem12SlotTargetTextBox.Text = _customerSpec.Stem12Target
            Stem12SlotMinTextBox.Text = _customerSpec.Stem12Min
            Stem12SlotMaxTextBox.Text = _customerSpec.Stem12Max

            StemFiberTargetTextBox.Text = _customerSpec.StemFiberTarget
            StemFiberMinTextBox.Text = _customerSpec.StemFiberMin
            StemFiberMaxTextBox.Text = _customerSpec.StemFiberMax

            TotalStemTargetTextBox.Text = _customerSpec.TOTStemTarget
            TotalStemMinTextBox.Text = _customerSpec.TOTStemMin
            TotalStemMaxTextBox.Text = _customerSpec.TOTStemMax
        Catch ex As Exception
            MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning)
        End Try
    End Sub

    Private Sub MetroWindow_Loaded(sender As Object, e As RoutedEventArgs)
        Try
            DisplayNameTextBlock.Text = currentUser

            CustomerSpecDataBinding()
        Catch ex As Exception
            MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning)
        End Try
    End Sub

    Private Sub SaveCustomerSpecButton_Click(sender As Object, e As RoutedEventArgs) Handles SaveCustomerSpecButton.Click
        Try
            If userRole <> "Admin" And userRole <> "QCStaff" Then
                MessageBox.Show("สิทธิ์ของท่านไม่ได้รับอนุญาตให้เข้าใช้งานเมนูนี้", "Notify", MessageBoxButton.OK, MessageBoxImage.Warning)
                Return
            End If

            If ExcelTypeComboBox.SelectedIndex = -1 Then
                MessageBox.Show("โปรดเลือก Excel Type", "Warning", MessageBoxButton.OK, MessageBoxImage.Warning)
                Return
            End If

            If PackGradeTextBox.Text = "" Then
                MessageBox.Show("โปรดระบุ Pack Grade", "Warning", MessageBoxButton.OK, MessageBoxImage.Warning)
                Return
            End If

            Dim customerSpec As New CustomerSpecBL

            Dim excelTypeComboBoxItem As ComboBoxItem = ExcelTypeComboBox.SelectedItem

            _customerSpec.Crop = CropTextBox.Text
            _customerSpec.Customer = CustomerTextBox.Text
            _customerSpec.PackGrade = PackGradeTextBox.Text
            _customerSpec.TargetCor = TargetCoreTextBox.Text
            _customerSpec.TypeOfExcel = excelTypeComboBoxItem.Content.ToString

            _customerSpec.Over1Target = Over1TargetTextBox.Text
            _customerSpec.Over1Min = Over1MinTextBox.Text
            _customerSpec.Over1Max = Over1MaxTextBox.Text

            _customerSpec.Over12Target = Over12TargetTextBox.Text
            _customerSpec.Over12Min = Over12MinTextBox.Text
            _customerSpec.Over12Max = Over12MaxTextBox.Text

            _customerSpec.Over14Target = Over14TargetTextBox.Text
            _customerSpec.Over14Min = Over14MinTextBox.Text
            _customerSpec.Over14Max = Over14MaxTextBox.Text

            _customerSpec.Over18Target = Over18TargetTextBox.Text
            _customerSpec.Over18Min = Over18MinTextBox.Text
            _customerSpec.Over18Max = Over18MaxTextBox.Text

            _customerSpec.TOT12Target = Total12TargetTextBox.Text
            _customerSpec.TOT12Min = Total12MinTextBox.Text
            _customerSpec.TOT12Max = Total12MaxTextBox.Text

            _customerSpec.TOT14Target = Total14TargetTextBox.Text
            _customerSpec.TOT14Min = Total14MinTextBox.Text
            _customerSpec.TOT14Max = Total14MaxTextBox.Text

            _customerSpec.Thru14Target = Thru14TargetTextBox.Text
            _customerSpec.Thru14Min = Thru14MinTextBox.Text
            _customerSpec.Thru14Max = Thru14MaxTextBox.Text

            _customerSpec.PanTarget = PanTargetTextBox.Text
            _customerSpec.PanMin = PanMinTextBox.Text
            _customerSpec.PanMax = PanMaxTextBox.Text


            _customerSpec.StemOBJTarget = OBJTargetTextBox.Text
            _customerSpec.StemOBJMin = OBJMinTextBox.Text
            _customerSpec.StemOBJMax = OBJMaxTextBox.Text

            _customerSpec.Stem7Target = Stem7SlotTargetTextBox.Text
            _customerSpec.Stem7Min = Stem7SlotMinTextBox.Text
            _customerSpec.Stem7Max = Stem7SlotMaxTextBox.Text

            _customerSpec.Stem12Target = Stem12SlotTargetTextBox.Text
            _customerSpec.Stem12Min = Stem12SlotMinTextBox.Text
            _customerSpec.Stem12Max = Stem12SlotMaxTextBox.Text

            _customerSpec.StemFiberTarget = StemFiberTargetTextBox.Text
            _customerSpec.StemFiberMin = StemFiberMinTextBox.Text
            _customerSpec.StemFiberMax = StemFiberMaxTextBox.Text

            _customerSpec.TOTStemTarget = TotalStemTargetTextBox.Text
            _customerSpec.TOTStemMin = TotalStemMinTextBox.Text
            _customerSpec.TOTStemMax = TotalStemMaxTextBox.Text


            customerSpec.Edit(_customerSpec)

            Dim messageBoxResult = MessageBox.Show("บันทึกข้อมูลสำเร็จ!", "Information", MessageBoxButton.OK, MessageBoxImage.Information)

            CustomerSpecDataBinding()
        Catch ex As Exception
            MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning)
        End Try
    End Sub

    Private Sub DeleteButton_Click(sender As Object, e As RoutedEventArgs) Handles DeleteButton.Click
        Try
            If userRole <> "Admin" And userRole <> "QCStaff" Then
                MessageBox.Show("สิทธิ์ของท่านไม่ได้รับอนุญาตให้เข้าใช้งานเมนูนี้", "Notify", MessageBoxButton.OK, MessageBoxImage.Warning)
                Return
            End If

            If MessageBox.Show("ท่านต้องการลบข้อมูล Spec นี้ใช่หรือไม่?", "Warning", MessageBoxButton.YesNo, MessageBoxImage.Warning) = MessageBoxResult.No Then
                Return
            End If

            Dim degradationTestCase As New DegradationTestCase

            If degradationTestCase.GetTestCaseByCustomer(CropTextBox.Text, CustomerTextBox.Text).Where(Function(x) x.PackGrade = PackGradeTextBox.Text).Count() > 0 Then
                MessageBox.Show("สเปกลูกค้านี้มี Test Case ที่ใช้งานข้อมูลนี้อยู่จึงไม่สามารถลบข้อมูลสเปกนี้ออกจากระบบได้", "Warning", MessageBoxButton.OK, MessageBoxImage.Warning)
                Return
            End If

            Dim customerSpec As New CustomerSpecBL

            customerSpec.Delete(CropTextBox.Text, CustomerTextBox.Text, PackGradeTextBox.Text)

            MessageBox.Show("ลบข้อมูลสำเร็จ!", "Information", MessageBoxButton.OK, MessageBoxImage.Information)

            Me.Close()
        Catch ex As Exception
            MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning)
        End Try
    End Sub

    Private Sub RefreshButton_Click(sender As Object, e As RoutedEventArgs) Handles RefreshButton.Click
        Try
            CustomerSpecDataBinding()
        Catch ex As Exception
            MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning)
        End Try
    End Sub
End Class
