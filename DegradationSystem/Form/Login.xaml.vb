﻿Public Class Login

    Private Sub ClearButton_Click(sender As Object, e As RoutedEventArgs) Handles ClearButton.Click
        Try
            UsernameTextBox.Text = ""
            PasswordTextBox.Password = ""
            UsernameTextBox.Focus()
        Catch ex As Exception
            MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning)
        End Try
    End Sub

    Private Sub SubmitButton_Click(sender As Object, e As RoutedEventArgs) Handles SubmitButton.Click
        Try
            If UsernameTextBox.Text = "" Then
                MessageBox.Show("โปรดใส่ชื่อผู้ใช้ Username", "Warning", MessageBoxButton.OK, MessageBoxImage.Warning)
                UsernameTextBox.Focus()
                Return
            End If

            If PasswordTextBox.Password.Length < 1 Then
                MessageBox.Show("โปรดใส่รหัสผ่าน Password", "Warning", MessageBoxButton.OK, MessageBoxImage.Warning)
                PasswordTextBox.Focus()
                Return
            End If

            Dim userAccountObj As New UserAccount

            If userAccountObj.GetAll().Where(Function(x) x.Username = UsernameTextBox.Text And x.Password = PasswordTextBox.Password).Count < 1 Then
                MessageBox.Show("ไม่พบข้อมูลบัญชีผู้ใช้นี้ โปรดตรวจสอบชื่อผู้ใช้และรหัสผ่านแล้วลองใหม่อีกครั้ง", "Warning", MessageBoxButton.OK, MessageBoxImage.Warning)
                UsernameTextBox.Focus()
                Return
            End If

            userAccountObj = userAccountObj.GetSingle(UsernameTextBox.Text)

            currentUser = userAccountObj.Username
            userRole = userAccountObj.UserRole
            crop = Now.Year

            NavigationService.Navigate(New MainMenu)

        Catch ex As Exception
            MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning)
        End Try
    End Sub

    Private Sub UsernameTextBox_TextChanged(sender As Object, e As TextChangedEventArgs) Handles UsernameTextBox.TextChanged
        Try
            If UsernameTextBox.Text.Length > 0 And PasswordTextBox.Password.Length > 0 Then
                SubmitButton.IsEnabled = True
            Else
                SubmitButton.IsEnabled = False
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning)
        End Try
    End Sub

    Private Sub Login_Loaded(sender As Object, e As RoutedEventArgs) Handles MyBase.Loaded, MyBase.Loaded
        Try
            UsernameTextBox.Focus()
        Catch ex As Exception
            MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning)
        End Try
    End Sub

    Private Sub PasswordTextBox_PasswordChanged(sender As Object, e As RoutedEventArgs) Handles PasswordTextBox.PasswordChanged
        Try
            If UsernameTextBox.Text.Length > 0 And PasswordTextBox.Password.Length > 0 Then
                SubmitButton.IsEnabled = True
            Else
                SubmitButton.IsEnabled = False
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning)
        End Try
    End Sub
End Class
