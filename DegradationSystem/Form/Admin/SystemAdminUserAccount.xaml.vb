﻿Imports MahApps.Metro.Controls

Public Class SystemAdminUserAccount

    Sub UserAccountDataBinding()
        Try
            Dim userAccountObj As New UserAccount

            UserAccountDataGrid.ItemsSource = userAccountObj.GetAll
        Catch ex As Exception
            MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning)
        End Try
    End Sub

    Private Sub MetroWindow_Loaded(sender As Object, e As RoutedEventArgs)
        Try
            DisplayNameTextBlock.Text = currentUser

            UserAccountDataBinding()
        Catch ex As Exception
            MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning)
        End Try
    End Sub

    Private Sub AddButton_Click(sender As Object, e As RoutedEventArgs) Handles AddButton.Click
        Try
            If UsernameTextBox.Text = "" Then
                MessageBox.Show("Please input username!", "Warning", MessageBoxButton.OK, MessageBoxImage.Warning)
                Return
            End If

            If PasswordPasswordBox.Password = "" Then
                MessageBox.Show("Please input password!", "Warning", MessageBoxButton.OK, MessageBoxImage.Warning)
                Return
            End If

            If UserRoleComboBox.SelectedIndex = -1 Then
                MessageBox.Show("Please select user role!", "Warning", MessageBoxButton.OK, MessageBoxImage.Warning)
                Return
            End If

            Dim userAccountObj As New UserAccount

            Dim comboBoxItem As New ComboBoxItem
            comboBoxItem = UserRoleComboBox.SelectedItem

            userAccountObj.Username = UsernameTextBox.Text
            userAccountObj.Password = PasswordPasswordBox.Password
            userAccountObj.UserRole = comboBoxItem.Content.ToString
            userAccountObj.Remark = RemarkTextBox.Text
            userAccountObj.UserStatus = UserStatusCheckBox.IsChecked
            userAccountObj.ModifiedDate = Now

            userAccountObj.Add(userAccountObj)

            UserAccountDataBinding()

            UsernameTextBox.Text = ""
            PasswordPasswordBox.Password = ""
            UserRoleComboBox.SelectedIndex = -1
            RemarkTextBox.Text = ""
            UserStatusCheckBox.IsChecked = True
        Catch ex As Exception
            MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning)
        End Try
    End Sub

    Private Sub EditButton_Click(sender As Object, e As RoutedEventArgs) Handles EditButton.Click
        Try
            If UsernameTextBox.Text = "" Then
                MessageBox.Show("Please input username!", "Warning", MessageBoxButton.OK, MessageBoxImage.Warning)
                Return
            End If

            If PasswordPasswordBox.Password = "" Then
                MessageBox.Show("Please input password!", "Warning", MessageBoxButton.OK, MessageBoxImage.Warning)
                Return
            End If

            If UserRoleComboBox.SelectedIndex = -1 Then
                MessageBox.Show("Please select user role!", "Warning", MessageBoxButton.OK, MessageBoxImage.Warning)
                Return
            End If

            Dim userAccountObj As New UserAccount

            Dim comboBoxItem As New ComboBoxItem
            comboBoxItem = UserRoleComboBox.SelectedItem

            userAccountObj.Username = UsernameTextBox.Text
            userAccountObj.Password = PasswordPasswordBox.Password
            userAccountObj.UserRole = comboBoxItem.Content.ToString
            userAccountObj.Remark = RemarkTextBox.Text
            userAccountObj.UserStatus = UserStatusCheckBox.IsChecked
            userAccountObj.ModifiedDate = Now

            userAccountObj.Edit(userAccountObj)

            UserAccountDataBinding()

            AddButton.IsEnabled = True
            EditButton.IsEnabled = False

            UsernameTextBox.Text = ""
            PasswordPasswordBox.Password = ""
            UserRoleComboBox.SelectedIndex = -1
            RemarkTextBox.Text = ""
            UserStatusCheckBox.IsChecked = True

        Catch ex As Exception
            MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning)
        End Try
    End Sub

    Private Sub DeleteButton_Click(sender As Object, e As RoutedEventArgs)
        Try
            If UserAccountDataGrid.SelectedIndex > -1 Then

                If MessageBox.Show("ท่านต้องการลบข้อมูลนี้ใช่หรือไม่?", "Warning", MessageBoxButton.YesNo, MessageBoxImage.Question) = MessageBoxResult.No Then
                    Return
                End If

                Dim userAccountObj As New UserAccount

                userAccountObj = UserAccountDataGrid.SelectedItem

                userAccountObj.Delete(userAccountObj.Username)

                UserAccountDataBinding()
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning)
        End Try
    End Sub

    Private Sub UserAccountDataGrid_MouseUp(sender As Object, e As MouseButtonEventArgs) Handles UserAccountDataGrid.MouseUp
        Try
            If UserAccountDataGrid.SelectedIndex > -1 Then

                Dim userAccountObj As New UserAccount

                userAccountObj = UserAccountDataGrid.SelectedItem

                UsernameTextBox.Text = userAccountObj.Username
                PasswordPasswordBox.Password = userAccountObj.Password

                UserRoleComboBox.Items.Cast(Of ComboBoxItem).Where(Function(x) x.Content = userAccountObj.UserRole).Single().IsSelected = True

                RemarkTextBox.Text = userAccountObj.Remark
                UserStatusCheckBox.IsChecked = userAccountObj.UserStatus

                AddButton.IsEnabled = False
                EditButton.IsEnabled = True

            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning)
        End Try
    End Sub

    Private Sub ClearButton_Click(sender As Object, e As RoutedEventArgs) Handles ClearButton.Click
        Try
            AddButton.IsEnabled = True
            EditButton.IsEnabled = False

            UsernameTextBox.Text = ""
            PasswordPasswordBox.Password = ""
            UserRoleComboBox.SelectedIndex = -1
            RemarkTextBox.Text = ""
            UserStatusCheckBox.IsChecked = True

        Catch ex As Exception
            MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning)
        End Try
    End Sub
End Class
