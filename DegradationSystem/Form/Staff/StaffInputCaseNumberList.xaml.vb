﻿Imports DegradationSystemBLL
Imports DomainModel
Imports MahApps.Metro.Controls.Dialogs

Public Class StaffInputCaseNumberList

    Private _selecedItem As pd
    Public Property SelectedItem() As pd
        Get
            Return _selecedItem
        End Get
        Set(ByVal value As pd)
            _selecedItem = value
        End Set
    End Property

    Sub CaseNoBinding()
        Try
            pdDataGrid.ItemsSource = Nothing
            pdDataGrid.ItemsSource = BLService.pdBL().GetByPdSetupDef()
        Catch ex As Exception
            MessageBoxHelper.Exception(ex)
        End Try
    End Sub

    Private Sub MetroWindow_Loaded(sender As Object, e As RoutedEventArgs)
        Try
            DisplayNameTextBlock.Text = currentUser
            CaseNoBinding()
        Catch ex As Exception
            MessageBoxHelper.Exception(ex)
        End Try
    End Sub

    Private Sub RefreshButton_Click(sender As Object, e As RoutedEventArgs) Handles RefreshButton.Click
        Try
            CaseNoBinding()
        Catch ex As Exception
            MessageBoxHelper.Exception(ex)
        End Try
    End Sub

    Private Sub CaseNumberDataGrid_MouseUp(sender As Object, e As MouseButtonEventArgs) Handles pdDataGrid.MouseUp
        Try
            If pdDataGrid.SelectedIndex < 0 Then
                Return
            End If

            _selecedItem = pdDataGrid.SelectedItem
            Me.Close()
        Catch ex As Exception
            MessageBoxHelper.Exception(ex)
        End Try
    End Sub

End Class
