﻿Imports MahApps.Metro.Controls
Imports System.IO.Ports
Imports System.Windows.Threading

Public Class StaffInputStemContent

    Private testCase As DegradationTestCase

    Public Property ReceivedTestCase() As DegradationTestCase
        Get
            Return testCase
        End Get
        Set(ByVal value As DegradationTestCase)
            testCase = value
        End Set
    End Property


    Dim serialPort As New SerialPort
    Dim receivedData As String

    'Delagate เพื่อใช้ในการชี้ตำแหน่งของ Function หรือ Sub
    Private Delegate Sub UpdateTextBox(ByVal recvData As String)

    'Sub สร้างไว้เพื่อใช้ในการ update textbox ที่ต้องการ
    Private Sub UpdateTextBoxHandler(ByVal recvData As String)
        Try
            WeightTextBox.Text = recvData.ToString
        Catch ex As Exception
            MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning)
        End Try
    End Sub

    Private Sub serialPort_DataReceived(sender As Object, e As IO.Ports.SerialDataReceivedEventArgs)
        Try
            Dim indata As String = serialPort.ReadLine

            Dim rString = Right(indata, 20)

            rString = rString.Replace(" ", "")
            rString = rString.Replace("g", "")
            rString = rString.Replace(vbCr, "")
            rString = rString.Replace("D", "")
            rString = rString.Replace("N", "")

            Dispatcher.Invoke(DispatcherPriority.Send, New UpdateTextBox(AddressOf UpdateTextBoxHandler), rString)
        Catch ex As Exception
            MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning)
        End Try
    End Sub

    Private Sub MetroWindow_Loaded(sender As Object, e As RoutedEventArgs)
        Try
            DisplayNameTextBlock.Text = currentUser

            Dim customerSpec As New CustomerSpecBL

            customerSpec = customerSpec.GetByPackGrade(testCase.Crop, testCase.Customer, testCase.PackGrade)

            ExcelTypeLabel.Content = customerSpec.TypeOfExcel

            CaseNoLabel.Content = testCase.CaseNo
            TestDateLabel.Content = testCase.Ddate
            TestTimeLabel.Content = testCase.Ttime
            ProductionNumberLabel.Content = testCase.Pdno
            CustomerLabel.Content = testCase.Customer
            GradeLabel.Content = testCase.PackGrade
            Over1Label.Content = testCase.SizeOver1
            Over12Label.Content = testCase.SizeOver12
            Over14Label.Content = testCase.SizeOver14
            Over18Label.Content = testCase.SizeOver18
            PanLabel.Content = testCase.SizePan
            SendTimeLabel.Content = testCase.ModifiedDate

            StemOBJTestCaseInfoLabel.Content = IIf(testCase.StemOBJ Is Nothing, "", testCase.StemOBJ)
            Stem7SlotTestCaseInfoLabel.Content = IIf(testCase.Stem7Slot Is Nothing, "", testCase.Stem7Slot)
            Stem12SlotTestCaseInfoLabel.Content = IIf(testCase.Stem12Slot Is Nothing, "", testCase.Stem12Slot)
            Stem12MeshTestCaseInfoLabel.Content = IIf(testCase.Stem12Mesh Is Nothing, "", testCase.Stem12Mesh)
            FiberThruTestCaseInfoLabel.Content = IIf(testCase.StemFiber Is Nothing, "", testCase.StemFiber)
            SampleWeightTestCaseInfoLabel.Content = IIf(testCase.SampleWeightG Is Nothing, "", testCase.SampleWeightG)
            SampleWeightPickUpTestCaseInfoLabel.Content = IIf(testCase.SampleWeightPickup Is Nothing, "", testCase.SampleWeightPickup)

            If ExcelTypeLabel.Content = "1" Then
                SampleWeightTextBox.IsEnabled = False
                SampleWeightPickUpTextBox.IsEnabled = False

                Stem12MeshButton.IsEnabled = False
            Else
                SampleWeightTextBox.IsEnabled = True
                SampleWeightPickUpTextBox.IsEnabled = True

                Stem12MeshButton.IsEnabled = True
            End If


            ComportNameLabel.Content = My.Settings.Comport
            BaudRateLabel.Content = My.Settings.BaudRate
            DataBitsLabel.Content = My.Settings.DataBits
            StopBitsLabel.Content = My.Settings.StopBits
            ParityLabel.Content = My.Settings.Parity


            '*******************  Get a list of serial port names. **********************

            Dim ports As String() = IO.Ports.SerialPort.GetPortNames()

            If ports.Count < 1 Then
                MessageBox.Show("ไม่พบ COM Port ที่ใช้ต่อเครื่องชั่งดิจิตอล โปรดตรวจสอบการต่ออุปกรณ์อีกครั้ง หรือติดต่อแผนก IT", "warning", MessageBoxButton.OK, MessageBoxImage.Warning)
                Return
            End If

            If serialPort.IsOpen = True Then
                serialPort.Close()
            End If

            With serialPort
                .PortName = My.Settings.Comport
                .BaudRate = My.Settings.BaudRate
                .Parity = IIf(My.Settings.Parity = "None", System.IO.Ports.Parity.None, IIf(My.Settings.Parity = "Odd", System.IO.Ports.Parity.Odd, IIf(My.Settings.Parity = "Even", System.IO.Ports.Parity.Even, IIf(My.Settings.Parity = "Mark", System.IO.Ports.Parity.Mark, System.IO.Ports.Parity.Space))))
                .DataBits = My.Settings.DataBits
                .StopBits = IIf(My.Settings.StopBits = "None", System.IO.Ports.StopBits.None, IIf(My.Settings.StopBits = "One", System.IO.Ports.StopBits.One, IIf(My.Settings.StopBits = "Two", System.IO.Ports.StopBits.Two, System.IO.Ports.StopBits.OnePointFive)))
                .Handshake = Handshake.None
                .RtsEnable = True
            End With

            AddHandler serialPort.DataReceived, AddressOf serialPort_DataReceived

            serialPort.Open()
        Catch ex As Exception
            MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning)
        End Try
    End Sub

    Private Sub StemOBJButton_Click(sender As Object, e As RoutedEventArgs) Handles StemOBJButton.Click
        Try
            StemOBJButton.Foreground = Brushes.Red
            Stem7SlotButton.Foreground = Brushes.Black
            Stem12SlotButton.Foreground = Brushes.Black
            Stem12MeshButton.Foreground = Brushes.Black
            FiberThruButton.Foreground = Brushes.Black

            SizeLable.Content = "OBJ"
            OkButton.IsEnabled = True
            WeightTextBox.Focus()
        Catch ex As Exception
            MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning)
        End Try
    End Sub

    Private Sub Stem7SlotButton_Click(sender As Object, e As RoutedEventArgs) Handles Stem7SlotButton.Click
        Try
            StemOBJButton.Foreground = Brushes.Black
            Stem7SlotButton.Foreground = Brushes.Red
            Stem12SlotButton.Foreground = Brushes.Black
            Stem12MeshButton.Foreground = Brushes.Black
            FiberThruButton.Foreground = Brushes.Black

            SizeLable.Content = "7 SLOT"
            OkButton.IsEnabled = True
            WeightTextBox.Focus()
        Catch ex As Exception
            MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning)
        End Try
    End Sub

    Private Sub Stem12SlotButton_Click(sender As Object, e As RoutedEventArgs) Handles Stem12SlotButton.Click
        Try
            StemOBJButton.Foreground = Brushes.Black
            Stem7SlotButton.Foreground = Brushes.Black
            Stem12SlotButton.Foreground = Brushes.Red
            Stem12MeshButton.Foreground = Brushes.Black
            FiberThruButton.Foreground = Brushes.Black

            SizeLable.Content = "12 SLOT"
            OkButton.IsEnabled = True
            WeightTextBox.Focus()
        Catch ex As Exception
            MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning)
        End Try
    End Sub

    Private Sub Stem12MeshButton_Click(sender As Object, e As RoutedEventArgs) Handles Stem12MeshButton.Click
        Try
            StemOBJButton.Foreground = Brushes.Black
            Stem7SlotButton.Foreground = Brushes.Black
            Stem12SlotButton.Foreground = Brushes.Black
            Stem12MeshButton.Foreground = Brushes.Red
            FiberThruButton.Foreground = Brushes.Black

            SizeLable.Content = "12 MESH"
            OkButton.IsEnabled = True
            WeightTextBox.Focus()
        Catch ex As Exception
            MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning)
        End Try
    End Sub

    Private Sub FiberThruButton_Click(sender As Object, e As RoutedEventArgs) Handles FiberThruButton.Click
        Try
            StemOBJButton.Foreground = Brushes.Black
            Stem7SlotButton.Foreground = Brushes.Black
            Stem12SlotButton.Foreground = Brushes.Black
            Stem12MeshButton.Foreground = Brushes.Black
            FiberThruButton.Foreground = Brushes.Red

            SizeLable.Content = "FIBER THRU"
            OkButton.IsEnabled = True
            WeightTextBox.Focus()
        Catch ex As Exception
            MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning)
        End Try
    End Sub

    Private Sub SaveButton_Click(sender As Object, e As RoutedEventArgs) Handles OkButton.Click
        Try
            If WeightTextBox.Text = "" Then
                MessageBox.Show("ไม่มีข้อมูลน้ำหนัก ไม่สามารถบันทึกได้!", "Warning", MessageBoxButton.OK, MessageBoxImage.Warning)
                Return
            End If

            If SizeLable.Content = "OBJ" Then
                StemOBJLabel.Content = WeightTextBox.Text
            End If

            If SizeLable.Content = "7 SLOT" Then
                Stem7SlotLabel.Content = WeightTextBox.Text
            End If

            If SizeLable.Content = "12 SLOT" Then
                Stem12SlotLabel.Content = WeightTextBox.Text
            End If

            If SizeLable.Content = "12 MESH" Then
                Stem12MeshLabel.Content = WeightTextBox.Text
            End If

            If SizeLable.Content = "FIBER THRU" Then
                FiberThruLabel.Content = WeightTextBox.Text
            End If

            If ExcelTypeLabel.Content = "1" And _
                StemOBJLabel.Content <> "" And _
                Stem7SlotLabel.Content <> "" And _
                Stem12SlotLabel.Content <> "" And _
                FiberThruLabel.Content <> "" Then

                SampleWeightTextBox.Text = Convert.ToDouble(StemOBJLabel.Content) + Convert.ToDouble(Stem7SlotLabel.Content) + Convert.ToDouble(Stem12SlotLabel.Content) + Convert.ToDouble(FiberThruLabel.Content)
                SaveDataButton.IsEnabled = True
            End If

            If ExcelTypeLabel.Content = "2" And _
                StemOBJLabel.Content <> "" And _
                Stem7SlotLabel.Content <> "" And _
                Stem12SlotLabel.Content <> "" And _
                Stem12MeshLabel.Content <> "" And _
                FiberThruLabel.Content <> "" And _
                SampleWeightTextBox.Text <> "" And _
                SampleWeightPickUpTextBox.Text <> "" Then
                SaveDataButton.IsEnabled = True
            End If

            StemOBJButton.Foreground = Brushes.Black
            Stem7SlotButton.Foreground = Brushes.Black
            Stem12SlotButton.Foreground = Brushes.Black
            Stem12MeshButton.Foreground = Brushes.Black
            FiberThruButton.Foreground = Brushes.Black

            OkButton.IsEnabled = False

            WeightTextBox.Text = ""
            WeightTextBox.Focus()

            SizeLable.Content = "Size"

        Catch ex As Exception
            MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning)
        End Try
    End Sub

    Private Sub SaveDataButton_Click(sender As Object, e As RoutedEventArgs) Handles SaveDataButton.Click
        Try
            Dim customerSpecObj As New CustomerSpecBL

            If customerSpecObj.GetByPackGrade(testCase.Crop, testCase.Customer, testCase.PackGrade).TypeOfExcel Is Nothing Then
                MessageBox.Show("ไม่พบข้อมูล Excel Form ในสเปกของลูกค้าเกรดนี้ โปรดระบุ Excel form ที่ใช้สำหรับลูกค้าเกรดนี้!")
                Return
            End If

            Dim testCaseObj As New DegradationTestCase

            testCaseObj.InputStemContent(testCase.Crop, _
                                         testCase.Pdno, _
                                         testCase.CaseNo, _
                                         Convert.ToDouble(StemOBJLabel.Content), _
                                         Convert.ToDouble(Stem7SlotLabel.Content), _
                                         Convert.ToDouble(Stem12SlotLabel.Content), _
                                         Convert.ToDouble(IIf(Stem12MeshLabel.Content = "", 0, Stem12MeshLabel.Content)), _
                                         Convert.ToDouble(FiberThruLabel.Content), _
                                         Convert.ToDouble(SampleWeightTextBox.Text), _
                                         Convert.ToDouble(IIf(SampleWeightPickUpTextBox.Text = "", 0, SampleWeightPickUpTextBox.Text)), _
                                         currentUser)

            Me.Close()
        Catch ex As Exception
            MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning)
        End Try
    End Sub

    Private Sub SampleWeightTextBox_KeyUp(sender As Object, e As KeyEventArgs) Handles SampleWeightTextBox.KeyUp
        Try
            If ExcelTypeLabel.Content = "2" And _
                StemOBJLabel.Content <> "" And _
                Stem7SlotLabel.Content <> "" And _
                Stem12SlotLabel.Content <> "" And _
                Stem12MeshLabel.Content <> "" And _
                FiberThruLabel.Content <> "" And _
                SampleWeightTextBox.Text <> "" And _
                SampleWeightPickUpTextBox.Text <> "" Then
                SaveDataButton.IsEnabled = True
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning)
        End Try
    End Sub

    Private Sub SampleWeightPickUpTextBox_KeyUp(sender As Object, e As KeyEventArgs) Handles SampleWeightPickUpTextBox.KeyUp
        Try
            If ExcelTypeLabel.Content = "2" And _
                StemOBJLabel.Content <> "" And _
                Stem7SlotLabel.Content <> "" And _
                Stem12SlotLabel.Content <> "" And _
                Stem12MeshLabel.Content <> "" And _
                FiberThruLabel.Content <> "" And _
                SampleWeightTextBox.Text <> "" And _
                SampleWeightPickUpTextBox.Text <> "" Then
                SaveDataButton.IsEnabled = True
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning)
        End Try
    End Sub

    Private Sub DigitalScaleCheckBox_Click(sender As Object, e As RoutedEventArgs) Handles DigitalScaleCheckBox.Click
        Try
            If DigitalScaleCheckBox.IsChecked = False Then

                DigitalScaleCheckBox.IsChecked = False

                If serialPort.IsOpen = True Then
                    serialPort.Close()
                End If

                WeightTextBox.Text = ""
                WeightTextBox.IsReadOnly = False
                WeightTextBox.Focus()

            Else

                DigitalScaleCheckBox.IsChecked = True

                If serialPort.IsOpen = False Then
                    serialPort.Open()
                End If

                WeightTextBox.Text = ""
                WeightTextBox.IsReadOnly = True

            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning)
        End Try
    End Sub

    Private Sub MetroWindow_Closing(sender As Object, e As ComponentModel.CancelEventArgs)
        Try
            If serialPort.IsOpen = True Then
                serialPort.Close()
                serialPort.Dispose()
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning)
        End Try
    End Sub
End Class
