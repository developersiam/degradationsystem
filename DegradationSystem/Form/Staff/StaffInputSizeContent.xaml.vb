﻿Imports MahApps.Metro.Controls
Imports System.IO.Ports
Imports System.Windows.Threading
Imports System.Globalization
Imports DegradationSystemBLL.Model
Imports DegradationSystemBLL

Public Class StaffInputSizeContent

    Dim serialPort As New SerialPort
    Dim receivedData As String
    Dim _caseNo As New pd
    Dim _pdsetup As New m_pdsetup

    'Delagate เพื่อใช้ในการชี้ตำแหน่งของ Function หรือ Sub
    Private Delegate Sub UpdateTextBox(ByVal recvData As String)

    Private _fx As Short

    Public Property SizeFunction() As Short
        Get
            Return _fx
        End Get
        Set(ByVal value As Short)
            _fx = value
        End Set
    End Property

    Public Sub StaffInputSizeContent()
        _caseNo = Nothing
        _pdsetup = Nothing
    End Sub
    Private Sub ClearForm()
        CaseNoSearchTextBox.Text = Nothing
        _caseNo.crop = Nothing

        CustomerLabel.Content = ""
        PackGradeLabel.Content = ""
        pdnoLabel.Content = ""
        ExcelTypeLabel.Content = ""
        PackingDateLabel.Content = ""
        PackingTimeLabel.Content = ""

        Over1Label.Content = ""
        Over12Label.Content = ""
        Over14Label.Content = ""
        Over18Label.Content = ""
        PanLabel.Content = ""

        Over1Button.Foreground = Brushes.Black
        Over12Button.Foreground = Brushes.Black
        Over14Button.Foreground = Brushes.Black
        Over18Button.Foreground = Brushes.Black
        PanButton.Foreground = Brushes.Black

        Over1Label.Background = Brushes.PowderBlue
        Over12Label.Background = Brushes.PowderBlue
        Over14Label.Background = Brushes.PowderBlue
        Over18Label.Background = Brushes.PowderBlue
        PanLabel.Background = Brushes.PowderBlue

        SendDataButton.IsEnabled = False
        OkButton.IsEnabled = False
    End Sub

    'Sub สร้างไว้เพื่อใช้ในการ update textbox ที่ต้องการ
    Private Sub UpdateTextBoxHandler(ByVal recvData As String)
        Try
            Dim newString As String = recvData
            Dim splitString As String()

            newString = newString.Replace(vbCr, "")
            newString = newString.Replace(" ", "")

            newString = newString.Replace("NETWEIGHT", "")
            newString = newString.Replace("TARE", ",")

            splitString = newString.Split(",")

            Dim netWeight As String = splitString(0)
            Dim tareWeight As String = splitString(1)

            DigitalScaleDetectLabel.Content = "NET WEIGHT: " & netWeight & "  ;  TARE: " & tareWeight

            WeightTextBox.Text = netWeight * 1000
        Catch ex As Exception
            MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning)
        End Try
    End Sub

    Private Sub serialPort_DataReceived(sender As Object, e As IO.Ports.SerialDataReceivedEventArgs)
        Try
            Dim sp As SerialPort = CType(sender, SerialPort)
            Dim indata As String = sp.ReadLine

            Dim iret As Integer
            iret = InStr(1, indata, vbCr)

            If (iret <> 0) Then
                Dispatcher.Invoke(DispatcherPriority.Send, New UpdateTextBox(AddressOf UpdateTextBoxHandler), indata)
                indata = ""
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning)
        End Try
    End Sub

    Private Sub Grid_Loaded(sender As Object, e As RoutedEventArgs)
        Try
            CaseNoSearchTextBox.Focus()
            DisplayNameTextBlock.Text = currentUser

            If currentUser <> "Pensri" Then
                DigitalScaleCheckBox.Visibility = Visibility.Hidden
            End If

            TimeModeLable.Content = "MM"

            HoursTextBox.Background = Brushes.White
            MinuteTextBox.Background = Brushes.LightGreen

            '*******************  Get a list of serial port names. **********************

            Dim ports As String() = IO.Ports.SerialPort.GetPortNames()

            If ports.Count < 1 Then
                MessageBox.Show("ไม่พบ COM Port ที่ใช้ต่อเครื่องชั่งดิจิตอล โปรดตรวจสอบการต่ออุปกรณ์อีกครั้ง หรือติดต่อแผนก IT", "warning", MessageBoxButton.OK, MessageBoxImage.Warning)
                Return
            End If

            If serialPort.IsOpen = True Then
                serialPort.Close()
            End If

            ComportNameLabel.Content = My.Settings.Comport
            BaudRateLabel.Content = My.Settings.BaudRate
            DataBitsLabel.Content = My.Settings.DataBits
            StopBitsLabel.Content = My.Settings.StopBits
            ParityLabel.Content = My.Settings.Parity

            With serialPort
                .PortName = My.Settings.Comport
                .BaudRate = My.Settings.BaudRate
                .Parity = IIf(My.Settings.Parity = "None", System.IO.Ports.Parity.None, IIf(My.Settings.Parity = "Odd", System.IO.Ports.Parity.Odd, IIf(My.Settings.Parity = "Even", System.IO.Ports.Parity.Even, IIf(My.Settings.Parity = "Mark", System.IO.Ports.Parity.Mark, System.IO.Ports.Parity.Space))))
                .DataBits = My.Settings.DataBits
                .StopBits = IIf(My.Settings.StopBits = "None", System.IO.Ports.StopBits.None, IIf(My.Settings.StopBits = "One", System.IO.Ports.StopBits.One, IIf(My.Settings.StopBits = "Two", System.IO.Ports.StopBits.Two, System.IO.Ports.StopBits.OnePointFive)))
                .Handshake = Handshake.None
                .RtsEnable = True
            End With

            AddHandler serialPort.DataReceived, AddressOf serialPort_DataReceived

            serialPort.Open()

        Catch ex As Exception
            MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning)
        End Try
    End Sub

    Private Sub AddTimeButton_Click(sender As Object, e As RoutedEventArgs) Handles AddTimeButton.Click
        Try
            If TimeModeLable.Content = "HH" Then

                HoursTextBox.Text = HoursTextBox.Text + 1

                If HoursTextBox.Text.Length < 2 Then
                    HoursTextBox.Text = "0" & HoursTextBox.Text
                End If
            Else

                MinuteTextBox.Text = MinuteTextBox.Text + 10

                If MinuteTextBox.Text.Length < 2 Then
                    MinuteTextBox.Text = "0" & MinuteTextBox.Text
                End If
            End If

            If HoursTextBox.Text > 23 Then
                'MessageBox.Show("ตัวเลขชั่วโมงจะต้องไม่เกิน 23 โปรดลองใหม่อีกครั้ง", "การแจ้งเตือน", MessageBoxButton.OK, MessageBoxImage.Information)
                HoursTextBox.Text = "00"
                Return
            End If

            If MinuteTextBox.Text > 50 Then
                'MessageBox.Show("ตัวเลขนาทีจะต้องไม่เกิน 40 โปรดลองใหม่อีกครั้ง", "การแจ้งเตือน", MessageBoxButton.OK, MessageBoxImage.Information)
                MinuteTextBox.Text = "00"
                Return
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning)
        End Try
    End Sub

    Private Sub MinusTimeButton_Click(sender As Object, e As RoutedEventArgs) Handles MinusTimeButton.Click
        Try
            If TimeModeLable.Content = "HH" Then

                HoursTextBox.Text = HoursTextBox.Text - 1

                If HoursTextBox.Text.Length < 2 Then
                    HoursTextBox.Text = "0" & HoursTextBox.Text
                End If

            Else

                MinuteTextBox.Text = MinuteTextBox.Text - 10

                If MinuteTextBox.Text.Length < 2 Then
                    MinuteTextBox.Text = "0" & MinuteTextBox.Text
                End If
            End If

            If HoursTextBox.Text < 0 Then
                'MessageBox.Show("ตัวเลขชั่วโมงจะต้องไม่น้อยกว่า 0 โปรดลองใหม่อีกครั้ง", "การแจ้งเตือน", MessageBoxButton.OK, MessageBoxImage.Information)
                HoursTextBox.Text = "23"
                Return
            End If

            If MinuteTextBox.Text < 0 Then
                'MessageBox.Show("ตัวเลขชั่วโมงจะต้องไม่น้อยกว่า 0 โปรดลองใหม่อีกครั้ง", "การแจ้งเตือน", MessageBoxButton.OK, MessageBoxImage.Information)
                MinuteTextBox.Text = "50"
                Return
            End If


        Catch ex As Exception
            MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning)
        End Try
    End Sub

    Private Sub HoursTextBox_GotFocus(sender As Object, e As RoutedEventArgs) Handles HoursTextBox.GotFocus
        Try
            TimeModeLable.Content = "HH"

            HoursTextBox.Background = Brushes.LightGreen
            MinuteTextBox.Background = Brushes.White
        Catch ex As Exception
            MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning)
        End Try
    End Sub

    Private Sub MinuteTextBox_GotFocus(sender As Object, e As RoutedEventArgs) Handles MinuteTextBox.GotFocus
        Try
            TimeModeLable.Content = "MM"

            HoursTextBox.Background = Brushes.White
            MinuteTextBox.Background = Brushes.LightGreen
        Catch ex As Exception
            MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning)
        End Try
    End Sub

    Private Sub Over1Button_Click(sender As Object, e As RoutedEventArgs) Handles Over1Button.Click
        Try
            If _caseNo.caseno Is Nothing Or _pdsetup.pdno Is Nothing Then
                MessageBox.Show("โปรดเลือก caseno ที่จะบันทุกข้อมูล", "warning", MessageBoxButton.OK, MessageBoxImage.Warning)
                CaseNoSearchTextBox.Clear()
                CaseNoSearchTextBox.Focus()
                Return
            End If

            Over1Button.Foreground = Brushes.Red
            Over12Button.Foreground = Brushes.Black
            Over14Button.Foreground = Brushes.Black
            Over18Button.Foreground = Brushes.Black
            PanButton.Foreground = Brushes.Black

            Over1Label.Background = Brushes.LightSalmon
            Over12Label.Background = Brushes.PowderBlue
            Over14Label.Background = Brushes.PowderBlue
            Over18Label.Background = Brushes.PowderBlue
            PanLabel.Background = Brushes.PowderBlue

            SizeFunction = 1

            WeightTextBox.Text = ""
            WeightTextBox.Focus()
            OkButton.IsEnabled = True
        Catch ex As Exception
            MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning)
        End Try
    End Sub

    Private Sub Over12Button_Click(sender As Object, e As RoutedEventArgs) Handles Over12Button.Click
        Try
            If _caseNo.caseno Is Nothing Or _pdsetup.pdno Is Nothing Then
                MessageBox.Show("โปรดเลือก caseno ที่จะบันทุกข้อมูล", "warning", MessageBoxButton.OK, MessageBoxImage.Warning)
                CaseNoSearchTextBox.Clear()
                CaseNoSearchTextBox.Focus()
                Return
            End If

            Over1Button.Foreground = Brushes.Black
            Over12Button.Foreground = Brushes.Red
            Over14Button.Foreground = Brushes.Black
            Over18Button.Foreground = Brushes.Black
            PanButton.Foreground = Brushes.Black

            Over1Label.Background = Brushes.PowderBlue
            Over12Label.Background = Brushes.LightSalmon
            Over14Label.Background = Brushes.PowderBlue
            Over18Label.Background = Brushes.PowderBlue
            PanLabel.Background = Brushes.PowderBlue

            SizeFunction = 2

            WeightTextBox.Text = ""
            WeightTextBox.Focus()

            OkButton.IsEnabled = True
        Catch ex As Exception
            MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning)
        End Try
    End Sub

    Private Sub Over14Button_Click(sender As Object, e As RoutedEventArgs) Handles Over14Button.Click
        Try
            If _caseNo.caseno Is Nothing Or _pdsetup.pdno Is Nothing Then
                MessageBox.Show("โปรดเลือก caseno ที่จะบันทุกข้อมูล", "warning", MessageBoxButton.OK, MessageBoxImage.Warning)
                CaseNoSearchTextBox.Clear()
                CaseNoSearchTextBox.Focus()
                Return
            End If

            Over1Button.Foreground = Brushes.Black
            Over12Button.Foreground = Brushes.Black
            Over14Button.Foreground = Brushes.Red
            Over18Button.Foreground = Brushes.Black
            PanButton.Foreground = Brushes.Black

            Over1Label.Background = Brushes.PowderBlue
            Over12Label.Background = Brushes.PowderBlue
            Over14Label.Background = Brushes.LightSalmon
            Over18Label.Background = Brushes.PowderBlue
            PanLabel.Background = Brushes.PowderBlue

            SizeFunction = 3

            WeightTextBox.Text = ""
            WeightTextBox.Focus()

            OkButton.IsEnabled = True
        Catch ex As Exception
            MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning)
        End Try
    End Sub

    Private Sub Over18Button_Click(sender As Object, e As RoutedEventArgs) Handles Over18Button.Click
        Try
            If _caseNo.caseno Is Nothing Or _pdsetup.pdno Is Nothing Then
                MessageBox.Show("โปรดเลือก caseno ที่จะบันทุกข้อมูล", "warning", MessageBoxButton.OK, MessageBoxImage.Warning)
                CaseNoSearchTextBox.Clear()
                CaseNoSearchTextBox.Focus()
                Return
            End If

            Over1Button.Foreground = Brushes.Black
            Over12Button.Foreground = Brushes.Black
            Over14Button.Foreground = Brushes.Black
            Over18Button.Foreground = Brushes.Red
            PanButton.Foreground = Brushes.Black

            Over1Label.Background = Brushes.PowderBlue
            Over12Label.Background = Brushes.PowderBlue
            Over14Label.Background = Brushes.PowderBlue
            Over18Label.Background = Brushes.LightSalmon
            PanLabel.Background = Brushes.PowderBlue

            SizeFunction = 4

            WeightTextBox.Text = ""
            WeightTextBox.Focus()

            OkButton.IsEnabled = True
        Catch ex As Exception
            MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning)
        End Try
    End Sub

    Private Sub PanButton_Click(sender As Object, e As RoutedEventArgs) Handles PanButton.Click
        Try
            If _caseNo.caseno Is Nothing Or _pdsetup.pdno Is Nothing Then
                MessageBox.Show("โปรดเลือก caseno ที่จะบันทุกข้อมูล", "warning", MessageBoxButton.OK, MessageBoxImage.Warning)
                CaseNoSearchTextBox.Clear()
                CaseNoSearchTextBox.Focus()
                Return
            End If

            Over1Button.Foreground = Brushes.Black
            Over12Button.Foreground = Brushes.Black
            Over14Button.Foreground = Brushes.Black
            Over18Button.Foreground = Brushes.Black
            PanButton.Foreground = Brushes.Red

            Over1Label.Background = Brushes.PowderBlue
            Over12Label.Background = Brushes.PowderBlue
            Over14Label.Background = Brushes.PowderBlue
            Over18Label.Background = Brushes.PowderBlue
            PanLabel.Background = Brushes.LightSalmon

            SizeFunction = 5

            WeightTextBox.Text = ""
            WeightTextBox.Focus()

            OkButton.IsEnabled = True
        Catch ex As Exception
            MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning)
        End Try
    End Sub

    Private Sub SendDataButton_Click(sender As Object, e As RoutedEventArgs) Handles SendDataButton.Click
        Try
            If _caseNo.caseno Is Nothing Or _pdsetup.pdno Is Nothing Then
                MessageBox.Show("โปรดเลือก caseno ที่จะบันทุกข้อมูล", "warning", MessageBoxButton.OK, MessageBoxImage.Warning)
                Return
            End If

            '**** Save test case data into the database.
            If CaseNoSearchTextBox.Text Is Nothing Or CaseNoSearchTextBox.Text = "" Then
                MessageBox.Show("โปรดเลือก Caseno ที่ต้องการบันทึกข้อมูล!", "Warning", MessageBoxButton.OK, MessageBoxImage.Warning)
                Return
            End If

            Dim customerSpecObj As New CustomerSpecBL

            If customerSpecObj.IsCustomerSpecification(_caseNo.crop, CustomerLabel.Content, PackGradeLabel.Content) = False Then
                MessageBox.Show("ไม่พบข้อมูลสเปกลูกค้าเกรดนี้ โปรดติดต่อหัวหน้างานเพื่อเพิ่มข้อมูลสเปกลูกค้าเกรดนี้เข้าในระบบ!")
                Return
            End If

            Dim testCaseObj As New DegradationTestCase

            If testCaseObj.IsTestCase(_caseNo.crop, pdnoLabel.Content, Convert.ToInt16(CaseNoSearchTextBox.Text)) = True Then
                MessageBox.Show("Caseno นี้ถูกบันทึกเข้าไปในระบบแล้ว ไม่อนุญาตให้บันทึกซ้ำได้ โปรดเลือก caseno อื่น!", "Warning", MessageBoxButton.OK, MessageBoxImage.Warning)
                Return
            End If

            Dim tTime As TimeSpan = TimeSpan.Parse(HoursTextBox.Text & ":" & MinuteTextBox.Text)

            If testCaseObj.GetTestCaseByPackGrade(PackingDateLabel.Content, PackingDateLabel.Content, CustomerLabel.Content, PackGradeLabel.Content).Where(Function(x) x.Ttime = tTime).Count() > 0 Then
                MessageBox.Show("เวลาในการสุ่มที่ท่านระบุซ้ำกับเวลาที่เคยระบุไปแล้วก่อนหน้านี้ โปรดเปลี่ยนเวลาการเก็บตัวอย่าง แล้วลองบันทึกข้อมูลใหม่อีกครั้ง!", "Warning", MessageBoxButton.OK, MessageBoxImage.Warning)
                Return
            End If

            testCaseObj.InputSizeContent(pdnoLabel.Content, CaseNoSearchTextBox.Text, _caseNo.crop, CustomerLabel.Content, PackGradeLabel.Content, tTime, Now, Over1Label.Content, Over12Label.Content, Over14Label.Content, Over18Label.Content, PanLabel.Content, currentUser)

            LastSendCaseNoLabel.Content = CaseNoSearchTextBox.Text.ToString
            LastSendTestTimeLabel.Content = HoursTextBox.Text & " : " & MinuteTextBox.Text
            LastSendPackingDateLabel.Content = PackingDateLabel.Content
            LastSendPdnoLabel.Content = pdnoLabel.Content

            LastSendOver1Label.Content = Over1Label.Content
            LastSendOver12Label.Content = Over12Label.Content
            LastSendOver14Label.Content = Over14Label.Content
            LastSendOver18Label.Content = Over18Label.Content
            LastSendPanLabel.Content = PanLabel.Content
            LastSendSendTimeLabel.Content = Now

            ClearForm()
        Catch ex As Exception
            MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning)
        End Try
    End Sub

    Private Sub DigitalScaleCheckBox_Click(sender As Object, e As RoutedEventArgs) Handles DigitalScaleCheckBox.Click
        Try
            If DigitalScaleCheckBox.IsChecked = False Then

                DigitalScaleCheckBox.IsChecked = False

                If serialPort.IsOpen = True Then
                    serialPort.Close()
                End If

                WeightTextBox.Text = ""
                WeightTextBox.IsReadOnly = False
                WeightTextBox.Focus()

            Else

                DigitalScaleCheckBox.IsChecked = True

                If serialPort.IsOpen = False Then
                    serialPort.Open()
                End If

                WeightTextBox.Text = ""
                WeightTextBox.IsReadOnly = True

            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning)
        End Try
    End Sub

    Private Sub MetroWindow_Closing(sender As Object, e As ComponentModel.CancelEventArgs)
        Try
            If serialPort.IsOpen = True Then
                serialPort.Close()
                Me.serialPort.Dispose()
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning)
        End Try
    End Sub

    Private Sub CaseNoSearchTextBox_KeyUp(sender As Object, e As KeyEventArgs)
        Try
            If e.Key <> Key.Enter Then
                Return
            End If

            If CaseNoSearchTextBox.Text = "" Then
                MessageBox.Show("โปรดกรอกเลข case no ที่ต้องการบันทึกข้อมูล", "การแจ้งเตือน", MessageBoxButton.OK, MessageBoxImage.Warning)
                Return
            End If

            If _pdsetup.pdno Is Nothing Then
                MessageBox.Show("โปรดเลือก pdno ที่ต้องการบันทึกข้อมูล", "การแจ้งเตือน", MessageBoxButton.OK, MessageBoxImage.Warning)
                Return
            End If

            _caseNo = BusinessService.pdBL.GetPdByCaseNo(_pdsetup.pdno, CaseNoSearchTextBox.Text)

            If _caseNo Is Nothing Then
                MessageBox.Show("ไม่พบข้อมูล case no #" + CaseNoSearchTextBox.Text + " นี้ในระบบ", "การแจ้งเตือน", MessageBoxButton.OK, MessageBoxImage.Warning)
                Return
            End If

            Dim time As String() = _caseNo.packingtime.ToString().Split(".")

            CaseNoSearchTextBox.Text = _caseNo.caseno
            _caseNo.crop = _caseNo.crop
            CustomerLabel.Content = _caseNo.customer
            PackGradeLabel.Content = _caseNo.grade
            pdnoLabel.Content = _caseNo.frompdno
            PackingDateLabel.Content = _caseNo.packingdate
            PackingTimeLabel.Content = time(1)

            Dim customerSpec As New CustomerSpecBL

            If customerSpec.IsCustomerSpecification(_caseNo.crop, _caseNo.customer, _caseNo.grade) = False Then
                MessageBox.Show("ไม่พบข้อมูลสเปกลูกค้าเกรดนี้ โปรดติดต่อหัวหน้างานเพื่อเพิ่มข้อมูลสเปกลูกค้าเกรดนี้เข้าในระบบ!", "ข้อความแจ้งเตือน", MessageBoxButton.OK, MessageBoxImage.Warning)
                Return
            End If

            customerSpec = customerSpec.GetByPackGrade(_caseNo.crop, _caseNo.customer, _caseNo.grade)

            ExcelTypeLabel.Content = customerSpec.TypeOfExcel
        Catch ex As Exception
            MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning)
        End Try
    End Sub

    Private Sub newGradeButton_Click(sender As Object, e As RoutedEventArgs)
        Try
            _pdsetup = StaffSelectProduction.ReturnModel

            If _pdsetup Is Nothing Then
                MessageBox.Show("โปรดเลือก pdno ที่ต้องการจะบันทึกข้อมูลก่อนทุกครั้ง", "warning", MessageBoxButton.OK, MessageBoxImage.Warning)
                CaseNoSearchTextBox.Clear()
                CaseNoSearchTextBox.Focus()
                Return
            End If

            pdnoLabel.Content = _pdsetup.pdno
            PackGradeLabel.Content = _pdsetup.packedgrade
            typeLabel.Content = _pdsetup.type
            CustomerLabel.Content = _pdsetup.customer
        Catch ex As Exception
            MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning)
        End Try
    End Sub

    Private Sub OkButton_Click(sender As Object, e As RoutedEventArgs)
        Try
            If _caseNo Is Nothing Then
                MessageBox.Show("โปรดเลือก caseno ที่จะบันทุกข้อมูล", "warning", MessageBoxButton.OK, MessageBoxImage.Warning)
                Return
            End If

            If WeightTextBox.Text = "" Then
                MessageBox.Show("ไม่มีข้อมูลน้ำหนัก ไม่สามารถบันทึกได้!", "Warning", MessageBoxButton.OK, MessageBoxImage.Warning)
                Return
            End If

            If _pdsetup Is Nothing Then
                MessageBox.Show("ไม่สามารถบันทึกได้ โปรดเลือก pdno ที่ต้องการจะบันทึกข้อมูลก่อนทุกครั้ง", "warning", MessageBoxButton.OK, MessageBoxImage.Warning)
                Return
            End If

            If SizeFunction = 1 Then
                Over1Label.Content = WeightTextBox.Text
            End If

            If SizeFunction = 2 Then
                Over12Label.Content = WeightTextBox.Text
            End If

            If SizeFunction = 3 Then
                Over14Label.Content = WeightTextBox.Text
            End If

            If SizeFunction = 4 Then
                Over18Label.Content = WeightTextBox.Text
            End If

            If SizeFunction = 5 Then
                PanLabel.Content = WeightTextBox.Text
            End If

            If Over1Label.Content <> "" And
                Over12Label.Content <> "" And
                Over14Label.Content <> "" And
                Over18Label.Content <> "" And
                PanLabel.Content <> "" Then
                SendDataButton.IsEnabled = True
            End If

            SendDataButton.IsEnabled = True
        Catch ex As Exception
            MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning)
        End Try
    End Sub
End Class
