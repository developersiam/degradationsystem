﻿Imports DegradationSystemBLL
Imports DegradationSystemBLL.Model

Public Class StaffSelectProduction
    Shared _retunModel As m_pdsetup

    Public Shared Function ReturnModel() As m_pdsetup
        Dim window As New StaffSelectProduction
        window.ShowDialog()
        Return _retunModel
    End Function

    Private Sub Window_Loaded(sender As Object, e As RoutedEventArgs)
        Try
            ProductionDataGrid.ItemsSource = Nothing
            ProductionDataGrid.ItemsSource = BusinessService.pdSetupBL().GetPdSetupByDate().OrderBy(Function(x) x.date)
        Catch ex As Exception
            MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning)
        End Try
    End Sub

    Private Sub ProductionDataGrid_MouseDoubleClick(sender As Object, e As MouseButtonEventArgs)
        Try
            If ProductionDataGrid.SelectedIndex < 0 Then
                Return
            End If

            _retunModel = ProductionDataGrid.SelectedItem

            If _retunModel Is Nothing Then
                MessageBox.Show("โปรดเลือก pdno ที่ต้องการจะบันทึกข้อมูล", "warning", MessageBoxButton.OK, MessageBoxImage.Warning)
                Return
            End If

            Me.Close()
        Catch ex As Exception
            MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning)
        End Try
    End Sub
End Class
