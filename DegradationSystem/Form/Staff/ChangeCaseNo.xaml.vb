﻿Imports DegradationSystemBLL
Imports DomainModel

Public Class ChangeCaseNo

    Private _pdNo As String
    Private _caseNo As Integer
    Private _newTestCase As pd

    Public Property CaseNo() As Integer
        Get
            Return _caseNo
        End Get
        Set(ByVal value As Integer)
            _caseNo = value
        End Set
    End Property

    Public Property NewTestCase() As pd
        Get
            Return _newTestCase
        End Get
        Set(ByVal value As pd)
            _newTestCase = value
        End Set
    End Property

    Public Property PdNo() As String
        Get
            Return _pdNo
        End Get
        Set(ByVal value As String)
            _pdNo = value
        End Set
    End Property

    Private Sub TestCaseBinding()
        Try
            Dim model = BLService.DegradationBL().GetByCaseNo(_pdNo, _caseNo)

            CaseNoLabel.Content = model.CaseNo
            TestTimeLabel.Content = model.Ttime
            PackingDateLabel.Content = model.Ddate
            pdnoLabel.Content = model.Pdno
            Over1Label.Content = model.Over1
            Over12Label.Content = model.Over1_2
            Over14Label.Content = model.Over1_4
            Over18Label.Content = model.Over1_8
            PanLabel.Content = model.Pan
        Catch ex As Exception
            MessageBoxHelper.Exception(ex)
        End Try
    End Sub

    Private Sub MetroWindow_Loaded(sender As Object, e As RoutedEventArgs)
        Try
            TestCaseBinding()
        Catch ex As Exception
            MessageBoxHelper.Exception(ex)
        End Try
    End Sub

    Private Sub CaseNoTextBox_GotFocus(sender As Object, e As RoutedEventArgs) Handles CaseNoTextBox.GotFocus
        Try
            _newTestCase = Nothing
            Dim window As New StaffInputCaseNumberList
            window.ShowDialog()

            If window.SelectedItem Is Nothing Then
                Return
            End If

            _newTestCase = window.SelectedItem

            'ต้องแก้ไขได้เฉพาะภายใน pdno เดียวกันเท่านั้น
            If _pdNo <> _newTestCase.frompdno Then
                MessageBoxHelper.Warning("หมายเลข Case No. ที่ท่านเลือกนี้เป็นหมายเลขที่อยู่ต่าง Production Number ระบบไม่อนุญาตให้ใช้หมายเลขนี้ได้" & vbNewLine &
                                "โปรดเลือกหมายเลข Case No. ที่อยู่ภายใน Production Number เดียวกันหรือติดต่อแผนกไอทีเพื่อแจ้งปัญหานี้!")
                Return
            End If

            CaseNoTextBox.Text = _newTestCase.caseno
            SaveButton.IsEnabled = True
        Catch ex As Exception
            MessageBoxHelper.Exception(ex)
        End Try
    End Sub

    Private Sub SaveButton_Click(sender As Object, e As RoutedEventArgs) Handles SaveButton.Click
        Try
            If MessageBoxHelper.Question("ท่านต้องการแก้ไขข้อมูลนี้ใช่หรือไม่?") = MessageBoxResult.No Then
                Return
            End If

            If String.IsNullOrEmpty(CaseNoTextBox.Text) Then
                MessageBoxHelper.Warning("โปรดระบุหมายเลข Case No. ลงในช่องบันทึกข้อมูล!")
                Return
            End If

            BLService.DegradationBL().ChangeCaseNo(_pdNo, _caseNo, _newTestCase.caseno, currentUser)
            MessageBoxHelper.Info("บันทึกข้อมูลสำเร็จ!")
            Me.Close()
        Catch ex As Exception
            MessageBoxHelper.Exception(ex)
        End Try
    End Sub

End Class
