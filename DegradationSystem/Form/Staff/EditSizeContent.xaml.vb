﻿Public Class StaffEditSizeContent
    
    Private _pdNo As String
    Private _caseNo As Integer

    Public Property CaseNo() As Integer
        Get
            Return _caseNo
        End Get
        Set(ByVal value As Integer)
            _caseNo = value
        End Set
    End Property

    Public Property PdNo() As String
        Get
            Return _pdNo
        End Get
        Set(ByVal value As String)
            _pdNo = value
        End Set
    End Property

    Private Sub TestCaseBinding()
        Try

            Dim _testCaseObject As New DegradationTestCase

            _testCaseObject = _testCaseObject.GetTestCaseByCase(_pdNo, _caseNo)

            TestCaseCaseNoLabel.Content = _testCaseObject.CaseNo
            TestCaseTestTimeLabel.Content = _testCaseObject.Ttime
            TestCasePackingDateLabel.Content = _testCaseObject.Ddate
            TestCaseProductionNumberLabel.Content = _testCaseObject.Pdno
            TestCaseOver1Label.Content = _testCaseObject.SizeOver1
            TestCaseOver12Label.Content = _testCaseObject.SizeOver12
            TestCaseOver14Label.Content = _testCaseObject.SizeOver14
            TestCaseOver18Label.Content = _testCaseObject.SizeOver18
            TestCasePanLabel.Content = _testCaseObject.SizePan

        Catch ex As Exception
            MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning)
        End Try
    End Sub

    Private Sub MetroWindow_Loaded(sender As Object, e As RoutedEventArgs)
        Try
            TestCaseBinding()
        Catch ex As Exception
            MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning)
        End Try
    End Sub

    Private Sub Over1Button_Click(sender As Object, e As RoutedEventArgs) Handles Over1Button.Click
        Try
            Over1Button.Foreground = Brushes.Red
            Over12Button.Foreground = Brushes.Black
            Over14Button.Foreground = Brushes.Black
            Over18Button.Foreground = Brushes.Black
            PanButton.Foreground = Brushes.Black

            SizeLable.Content = "Over 1"

            WeightTextBox.Text = TestCaseOver1Label.Content
            WeightTextBox.Focus()
            WeightTextBox.IsReadOnly = False

            OkButton.IsEnabled = True
        Catch ex As Exception
            MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning)
        End Try
    End Sub

    Private Sub Over12Button_Click(sender As Object, e As RoutedEventArgs) Handles Over12Button.Click
        Try
            Over1Button.Foreground = Brushes.Black
            Over12Button.Foreground = Brushes.Red
            Over14Button.Foreground = Brushes.Black
            Over18Button.Foreground = Brushes.Black
            PanButton.Foreground = Brushes.Black

            SizeLable.Content = "Over 1/2"

            WeightTextBox.Text = TestCaseOver12Label.Content
            WeightTextBox.Focus()
            WeightTextBox.IsReadOnly = False

            OkButton.IsEnabled = True
        Catch ex As Exception
            MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning)
        End Try
    End Sub

    Private Sub Over14Button_Click(sender As Object, e As RoutedEventArgs) Handles Over14Button.Click
        Try
            Over1Button.Foreground = Brushes.Black
            Over12Button.Foreground = Brushes.Black
            Over14Button.Foreground = Brushes.Red
            Over18Button.Foreground = Brushes.Black
            PanButton.Foreground = Brushes.Black

            SizeLable.Content = "Over 1/4"

            WeightTextBox.Text = TestCaseOver14Label.Content
            WeightTextBox.Focus()
            WeightTextBox.IsReadOnly = False

            OkButton.IsEnabled = True
        Catch ex As Exception
            MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning)
        End Try
    End Sub

    Private Sub Over18Button_Click(sender As Object, e As RoutedEventArgs) Handles Over18Button.Click
        Try
            Over1Button.Foreground = Brushes.Black
            Over12Button.Foreground = Brushes.Black
            Over14Button.Foreground = Brushes.Black
            Over18Button.Foreground = Brushes.Red
            PanButton.Foreground = Brushes.Black

            SizeLable.Content = "Over 1/8"

            WeightTextBox.Text = TestCaseOver18Label.Content
            WeightTextBox.Focus()
            WeightTextBox.IsReadOnly = False

            OkButton.IsEnabled = True
        Catch ex As Exception
            MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning)
        End Try
    End Sub

    Private Sub PanButton_Click(sender As Object, e As RoutedEventArgs) Handles PanButton.Click
        Try
            Over1Button.Foreground = Brushes.Black
            Over12Button.Foreground = Brushes.Black
            Over14Button.Foreground = Brushes.Black
            Over18Button.Foreground = Brushes.Black
            PanButton.Foreground = Brushes.Red

            SizeLable.Content = "Pan"

            WeightTextBox.Text = TestCasePanLabel.Content
            WeightTextBox.Focus()
            WeightTextBox.IsReadOnly = False

            OkButton.IsEnabled = True
        Catch ex As Exception
            MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning)
        End Try
    End Sub

    Private Sub SaveButton_Click(sender As Object, e As RoutedEventArgs) Handles SaveButton.Click
        Try
            '**** Save test case data into the database.

            If MessageBox.Show("ท่านต้องการบันทึกการแก้ไขข้อมูลนี้ใช่หรือไม่?", "Confirmation", MessageBoxButton.YesNo, MessageBoxImage.Question) = MessageBoxResult.No Then
                Return
            End If

            Dim _testCaseObject As New DegradationTestCase

            _testCaseObject = _testCaseObject.GetTestCaseByCase(_pdNo, _caseNo)

            _testCaseObject.ChangeSizeContent(_testCaseObject.Crop,
                                                 _testCaseObject.Customer,
                                                 _testCaseObject.PackGrade,
                                                 _testCaseObject.Pdno,
                                                 _testCaseObject.CaseNo,
                                                 Over1Label.Content,
                                                 Over12Label.Content,
                                                 Over14Label.Content,
                                                 Over18Label.Content,
                                                 PanLabel.Content,
                                                 currentUser)

            MessageBox.Show("บันทึกการแก้ไขข้อมูลสำเร็จ!", "Information", MessageBoxButton.OK, MessageBoxImage.Information)

            Over1Label.Content = ""
            Over12Label.Content = ""
            Over14Label.Content = ""
            Over18Label.Content = ""
            PanLabel.Content = ""

            Over1Button.Foreground = Brushes.Black
            Over12Button.Foreground = Brushes.Black
            Over14Button.Foreground = Brushes.Black
            Over18Button.Foreground = Brushes.Black
            PanButton.Foreground = Brushes.Black

            SizeLable.Content = "Size"

            SaveButton.IsEnabled = False

            TestCaseBinding()
        Catch ex As Exception
            MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning)
        End Try
    End Sub

    Private Sub OkButton_Click(sender As Object, e As RoutedEventArgs) Handles OkButton.Click
        Try
            If WeightTextBox.Text = "" Then
                MessageBox.Show("ไม่มีข้อมูลน้ำหนัก ไม่สามารถบันทึกได้!", "Warning", MessageBoxButton.OK, MessageBoxImage.Warning)
                Return
            End If

            If SizeLable.Content = "Over 1" Then
                Over1Label.Content = WeightTextBox.Text
            End If

            If SizeLable.Content = "Over 1/2" Then
                Over12Label.Content = WeightTextBox.Text
            End If

            If SizeLable.Content = "Over 1/4" Then
                Over14Label.Content = WeightTextBox.Text
            End If

            If SizeLable.Content = "Over 1/8" Then
                Over18Label.Content = WeightTextBox.Text
            End If

            If SizeLable.Content = "Pan" Then
                PanLabel.Content = WeightTextBox.Text
            End If


            If Over1Label.Content <> "" And Over12Label.Content <> "" And Over14Label.Content <> "" And Over18Label.Content <> "" And PanLabel.Content <> "" Then
                SaveButton.IsEnabled = True
            End If

            OkButton.IsEnabled = False

            Over1Button.Foreground = Brushes.Black
            Over12Button.Foreground = Brushes.Black
            Over14Button.Foreground = Brushes.Black
            Over18Button.Foreground = Brushes.Black
            PanButton.Foreground = Brushes.Black

            SizeLable.Content = "Size"

        Catch ex As Exception
            MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning)
        End Try
    End Sub

    Private Sub Grid_Loaded(sender As Object, e As RoutedEventArgs)

    End Sub
End Class
