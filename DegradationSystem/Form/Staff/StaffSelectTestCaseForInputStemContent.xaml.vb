﻿Imports Microsoft.Office.Interop
Imports Microsoft.Office.Interop.Excel
Imports System.Globalization

Public Class StaffSelectTestCaseForInputStemContent

    Private Sub MetroWindow_Loaded(sender As Object, e As RoutedEventArgs)
        Try
            DisplayNameTextBlock.Text = currentUser

            FromDatePicker.Text = Now
            ToDatePicker.Text = Now
        Catch ex As Exception
            MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning)
        End Try
    End Sub

    Private Sub FromDatePicker_SelectedDateChanged(sender As Object, e As SelectionChangedEventArgs) Handles FromDatePicker.SelectedDateChanged
        Try
            If ToDatePicker.Text = "" Then
                Return
            End If

            If FromDatePicker.Text <> "" And ToDatePicker.Text <> "" Then
                Dim degradationTestCase As New DegradationTestCase

                CustomerComboBox.IsEnabled = True

                CustomerComboBox.ItemsSource = Nothing
                CustomerComboBox.ItemsSource = degradationTestCase.GetCustomerFromStartAndToDate(FromDatePicker.Text, ToDatePicker.Text).OrderBy(Function(cus) cus.CustomerName)

                PackGradeComboBox.ItemsSource = Nothing

                DisplayDataButton.IsEnabled = False
                ExportButton.IsEnabled = False
                ViewTargetButton.IsEnabled = False
                ViewReportButton.IsEnabled = False
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning)
        End Try
    End Sub

    Private Sub ToDatePicker_SelectedDateChanged(sender As Object, e As SelectionChangedEventArgs) Handles ToDatePicker.SelectedDateChanged
        Try
            If FromDatePicker.Text = "" Then
                Return
            End If

            If FromDatePicker.Text <> "" And ToDatePicker.Text <> "" Then
                Dim degradationTestCase As New DegradationTestCase

                CustomerComboBox.IsEnabled = True

                CustomerComboBox.ItemsSource = Nothing
                CustomerComboBox.ItemsSource = degradationTestCase.GetCustomerFromStartAndToDate(FromDatePicker.Text, ToDatePicker.Text).OrderBy(Function(cus) cus.CustomerName)

                PackGradeComboBox.ItemsSource = Nothing

                DisplayDataButton.IsEnabled = False
                ExportButton.IsEnabled = False
                ViewTargetButton.IsEnabled = False
                ViewReportButton.IsEnabled = False
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning)
        End Try
    End Sub

    Private Sub CustomerComboBox_SelectionChanged(sender As Object, e As SelectionChangedEventArgs) Handles CustomerComboBox.SelectionChanged
        Try
            If CustomerComboBox.SelectedValue = "" Then
                Return
            End If

            Dim testCase As New DegradationTestCase

            PackGradeComboBox.IsEnabled = True

            DisplayDataButton.IsEnabled = False
            ExportButton.IsEnabled = False
            ViewTargetButton.IsEnabled = False
            ViewReportButton.IsEnabled = False

            PackGradeComboBox.ItemsSource = Nothing
            PackGradeComboBox.ItemsSource = testCase.GetPackGradeFromTestCaseByCustomer(CustomerComboBox.SelectedValue, FromDatePicker.Text, ToDatePicker.Text)
        Catch ex As Exception
            MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning)
        End Try
    End Sub

    Private Sub PackGradeComboBox_SelectionChanged(sender As Object, e As SelectionChangedEventArgs) Handles PackGradeComboBox.SelectionChanged
        Try
            If PackGradeComboBox.SelectedValue = "" Then
                Return
            End If

            DisplayDataButton.IsEnabled = True
            ViewTargetButton.IsEnabled = True
            ViewReportButton.IsEnabled = True

            ExportButton.IsEnabled = False
        Catch ex As Exception
            MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning)
        End Try
    End Sub

    Private Sub DisplayDataButton_Click(sender As Object, e As RoutedEventArgs) Handles DisplayDataButton.Click
        Try
            Dim testCase As New DegradationTestCase

            CaseNumberDataGrid.ItemsSource = Nothing
            CaseNumberDataGrid.ItemsSource = testCase.GetTestCaseByPackGrade(FromDatePicker.Text, ToDatePicker.Text, CustomerComboBox.SelectedValue, PackGradeComboBox.SelectedValue).OrderByDescending(Function(x) x.Ddate).ThenByDescending(Function(x) x.Ttime)

            ExportButton.IsEnabled = False

            ViewTargetButton.IsEnabled = True
        Catch ex As Exception
            MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning)
        End Try
    End Sub

    Private Sub CaseNumberDataGrid_SelectionChanged(sender As Object, e As SelectionChangedEventArgs) Handles CaseNumberDataGrid.SelectionChanged
        Try
            If CaseNumberDataGrid.SelectedItems.Count > 0 Then
                ExportButton.IsEnabled = True
            End If

            If CaseNumberDataGrid.SelectedItems.Count < 1 Then
                SelectedRowsLable.Content = 0
                Return
            End If

            SelectedRowsLable.Content = CaseNumberDataGrid.SelectedItems.Count

        Catch ex As Exception
            MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning)
        End Try
    End Sub

    Private Sub CaseNumberDataGrid_MouseDoubleClick(sender As Object, e As MouseButtonEventArgs) Handles CaseNumberDataGrid.MouseDoubleClick
        Try
            If CaseNumberDataGrid.SelectedIndex > -1 Then

                Dim staffInputStemContent As New StaffInputStemContent
                Dim testCaseObj As New DegradationTestCase

                testCaseObj = CaseNumberDataGrid.SelectedItem

                staffInputStemContent.ReceivedTestCase = testCaseObj

                staffInputStemContent.ShowDialog()

                CaseNumberDataGrid.ItemsSource = Nothing
                CaseNumberDataGrid.ItemsSource = testCaseObj.GetTestCaseByPackGrade(FromDatePicker.Text, ToDatePicker.Text, CustomerComboBox.SelectedValue, PackGradeComboBox.SelectedValue).OrderByDescending(Function(x) x.Ddate).ThenByDescending(Function(x) x.Ttime)

                ExportButton.IsEnabled = False
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning)
        End Try
    End Sub

    Private Sub ExportButton_Click(sender As Object, e As RoutedEventArgs) Handles ExportButton.Click
        Try

            'MessageBox.Show(CaseNumberDataGrid.SelectedItems.Count())
            If CaseNumberDataGrid.SelectedItems.Count < 1 Then
                MessageBox.Show("โปรดเลือก Test case ที่ต้องการส่งออกเป็นไฟล์ Excel ก่อนค่ะ!", "Warning", MessageBoxButton.OK, MessageBoxImage.Warning)
                Return
            End If

            '**************** ตรวจสอบ User ในกรณีส่งออกข้อมูลเป็น Excel จะต้องเลือกช่วงเวลาเป็นภายในปีเดียวกันเท่านั้น ***************
            'If FromDatePicker.Text <> ToDatePicker.Text Then
            '    MessageBox.Show("ในการส่งออกไฟล์ excel จะต้องเป็นกลุ่มของ Test case ที่อยู่ภายในวันเดียวกันเท่านั้น!", "Warning", MessageBoxButton.OK, MessageBoxImage.Warning)
            '    Return
            'End If

            Dim excelApplication As New Excel.Application
            Dim workBook As Excel.Workbook
            Dim workSheet As Excel.Worksheet
            Dim range As Excel.Range
            Dim cell As String

            Dim customerSpec As New CustomerSpecBL

            customerSpec = customerSpec.GetByPackGrade(Convert.ToDateTime(FromDatePicker.Text).Year, CustomerComboBox.SelectedValue, PackGradeComboBox.SelectedValue)

            If customerSpec.TypeOfExcel = "1" Then

                workBook = excelApplication.Workbooks.Open("C:\DegForm\DegradationForm.xls")
                workSheet = workBook.ActiveSheet
                excelApplication.Visible = True

                Dim packGrade As New PackGrade

                packGrade = packGrade.GetPackGrade(Convert.ToDateTime(ToDatePicker.Text).Year).Where(Function(x) x.customer = CustomerComboBox.SelectedValue And x.packedgrade = PackGradeComboBox.SelectedValue).Single()

                cell = "C5"
                range = workSheet.Range(cell)
                range.Font.Name = "Eras Medium iTC"
                range.Font.Size = 11
                workSheet.Range(cell).Value = packGrade.customer

                cell = "F5"
                range = workSheet.Range(cell)
                range.Font.Name = "Eras Medium iTC"
                range.Font.Size = 11
                workSheet.Range(cell).Value = packGrade.packedgrade
                cell = "K5"
                range = workSheet.Range(cell)
                range.Font.Name = "Eras Medium iTC"
                range.Font.Size = 11
                workSheet.Range(cell).Value = packGrade.crop

                cell = "P5"
                range = workSheet.Range(cell)
                range.Font.Name = "Eras Medium iTC"
                range.Font.Size = 11
                workSheet.Range(cell).Value = packGrade.type

                cell = "S5"
                range = workSheet.Range(cell)
                range.Font.Name = "Eras Medium ITC"
                range.Font.Size = 11
                workSheet.Range(cell).Value = packGrade.form

                cell = "Z5"
                range = workSheet.Range(cell)
                range.Font.Name = "Eras Medium iTC"
                range.Font.Size = 11
                workSheet.Range(cell).Value = "DATE : " & ToDatePicker.Text

                Dim i As Integer = 10

                For Each item As DegradationTestCase In CaseNumberDataGrid.SelectedItems

                    'เริ่มDetail ตั้งแต่  B10
                    cell = "A" & i
                    range = workSheet.Range(cell)
                    range.Font.Name = "CordiaUPC"
                    range.Font.Size = 16
                    workSheet.Range(cell).Value = item.Ddate.Day & "/" & item.Ddate.Month & "/" & item.Ddate.Year

                    cell = "B" & i
                    range = workSheet.Range(cell)
                    range.Font.Name = "CordiaUPC"
                    range.Font.Size = 16
                    workSheet.Range(cell).Value = item.Ttime.ToString 'item.Ttime.Hours & ":" & item.Ttime.Minutes

                    cell = "D" & i
                    range = workSheet.Range(cell)
                    range.Font.Name = "CordiaUPC"
                    range.Font.Size = 16
                    workSheet.Range(cell).Value = item.SizeOver1

                    cell = "F" & i
                    range = workSheet.Range(cell)
                    range.Font.Name = "CordiaUPC"
                    range.Font.Size = 16
                    workSheet.Range(cell).Value = item.SizeOver12

                    cell = "i" & i
                    range = workSheet.Range(cell)
                    range.Font.Name = "CordiaUPC"
                    range.Font.Size = 16
                    workSheet.Range(cell).Value = item.SizeOver14

                    cell = "L" & i
                    range = workSheet.Range(cell)
                    range.Font.Name = "CordiaUPC"
                    range.Font.Size = 16
                    workSheet.Range(cell).Value = item.SizeOver18

                    cell = "N" & i
                    range = workSheet.Range(cell)
                    range.Font.Name = "CordiaUPC"
                    range.Font.Size = 16
                    workSheet.Range(cell).Value = item.SizePan

                    cell = "Q" & i
                    range = workSheet.Range(cell)
                    range.Font.Name = "CordiaUPC"
                    range.Font.Size = 16
                    workSheet.Range(cell).Value = item.StemOBJ

                    cell = "S" & i
                    range = workSheet.Range(cell)
                    range.Font.Name = "CordiaUPC"
                    range.Font.Size = 16
                    workSheet.Range(cell).Value = item.Stem7Slot

                    cell = "U" & i
                    range = workSheet.Range(cell)
                    range.Font.Name = "CordiaUPC"
                    range.Font.Size = 16
                    workSheet.Range(cell).Value = item.Stem12Slot

                    cell = "W" & i
                    range = workSheet.Range(cell)
                    range.Font.Name = "CordiaUPC"
                    range.Font.Size = 16
                    workSheet.Range(cell).Value = item.StemFiber

                    cell = "Z" & i
                    range = workSheet.Range(cell)
                    range.Font.Name = "CordiaUPC"
                    range.Font.Size = 16
                    workSheet.Range(cell).Value = item.CaseNo

                    i = i + 1
                Next
            Else
                workBook = excelApplication.Workbooks.Open("C:\DegForm\Degradation2.xlsx")
                workSheet = workBook.ActiveSheet
                excelApplication.Visible = True

                Dim packGrade As New PackGrade

                packGrade = packGrade.GetPackGrade(Convert.ToDateTime(ToDatePicker.Text).Year).Where(Function(x) x.customer = CustomerComboBox.SelectedValue And x.packedgrade = PackGradeComboBox.SelectedValue).Single()

                cell = "C5"
                range = workSheet.Range(cell)
                range.Font.Name = "Eras Medium iTC"
                range.Font.Size = 11
                workSheet.Range(cell).Value = packGrade.customer

                cell = "G5"
                range = workSheet.Range(cell)
                range.Font.Name = "Eras Medium iTC"
                range.Font.Size = 11
                workSheet.Range(cell).Value = packGrade.packedgrade

                cell = "L5"
                range = workSheet.Range(cell)
                range.Font.Name = "Eras Medium iTC"
                range.Font.Size = 11
                workSheet.Range(cell).Value = packGrade.crop

                cell = "Q5"
                range = workSheet.Range(cell)
                range.Font.Name = "Eras Medium iTC"
                range.Font.Size = 11
                workSheet.Range(cell).Value = packGrade.type

                cell = "U5"
                range = workSheet.Range(cell)
                range.Font.Name = "Eras Medium iTC"
                range.Font.Size = 11
                workSheet.Range(cell).Value = packGrade.form

                cell = "AF5"
                range = workSheet.Range(cell)
                range.Font.Name = "Eras Medium iTC"
                range.Font.Size = 11
                workSheet.Range(cell).Value = "DATE : " & ToDatePicker.Text

                Dim i As Integer = 10

                For Each item As DegradationTestCase In CaseNumberDataGrid.SelectedItems

                    'เริ่มDetail ตั้งแต่  B10

                    cell = "A" & i
                    range = workSheet.Range(cell)
                    range.Font.Name = "CordiaUPC"
                    range.Font.Size = 16
                    workSheet.Range(cell).Value = item.Ddate.Day & "/" & item.Ddate.Month & "/" & item.Ddate.Year

                    cell = "B" & i
                    range = workSheet.Range(cell)
                    range.Font.Name = "CordiaUPC"
                    range.Font.Size = 16
                    workSheet.Range(cell).Value = item.Ttime.Hours & ":" & item.Ttime.Minutes

                    cell = "C" & i
                    range = workSheet.Range(cell)
                    range.Font.Name = "CordiaUPC"
                    range.Font.Size = 16
                    workSheet.Range(cell).Value = item.SampleWeightPickup

                    cell = "E" & i
                    range = workSheet.Range(cell)
                    range.Font.Name = "CordiaUPC"
                    range.Font.Size = 16
                    workSheet.Range(cell).Value = item.SizeOver1

                    cell = "G" & i
                    range = workSheet.Range(cell)
                    range.Font.Name = "CordiaUPC"
                    range.Font.Size = 16
                    workSheet.Range(cell).Value = item.SizeOver12

                    cell = "J" & i
                    range = workSheet.Range(cell)
                    range.Font.Name = "CordiaUPC"
                    range.Font.Size = 16
                    workSheet.Range(cell).Value = item.SizeOver14

                    cell = "M" & i
                    range = workSheet.Range(cell)
                    range.Font.Name = "CordiaUPC"
                    range.Font.Size = 16
                    workSheet.Range(cell).Value = item.SizeOver18

                    cell = "O" & i
                    range = workSheet.Range(cell)
                    range.Font.Name = "CordiaUPC"
                    range.Font.Size = 16
                    workSheet.Range(cell).Value = item.SizePan

                    cell = "R" & i
                    range = workSheet.Range(cell)
                    range.Font.Name = "CordiaUPC"
                    range.Font.Size = 16
                    workSheet.Range(cell).Value = item.StemSampleWeight

                    cell = "S" & i
                    range = workSheet.Range(cell)
                    range.Font.Name = "CordiaUPC"
                    range.Font.Size = 16
                    workSheet.Range(cell).Value = item.StemOBJ

                    cell = "U" & i
                    range = workSheet.Range(cell)
                    range.Font.Name = "CordiaUPC"
                    range.Font.Size = 16
                    workSheet.Range(cell).Value = item.Stem7Slot

                    cell = "W" & i
                    range = workSheet.Range(cell)
                    range.Font.Name = "CordiaUPC"
                    range.Font.Size = 16
                    workSheet.Range(cell).Value = item.Stem12Slot

                    cell = "Y" & i
                    range = workSheet.Range(cell)
                    range.Font.Name = "CordiaUPC"
                    range.Font.Size = 16
                    workSheet.Range(cell).Value = item.Stem12Mesh

                    cell = "AA" & i
                    range = workSheet.Range(cell)
                    range.Font.Name = "CordiaUPC"
                    range.Font.Size = 16
                    workSheet.Range(cell).Value = item.StemFiberThru12Mesh

                    cell = "AF" & i
                    range = workSheet.Range(cell)
                    range.Font.Name = "CordiaUPC"
                    range.Font.Size = 16
                    workSheet.Range(cell).Value = item.CaseNo

                    i = i + 1
                Next
            End If

            workSheet = Nothing
            workBook = Nothing
            excelApplication = Nothing

        Catch ex As Exception
            MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning)
        End Try
    End Sub

    Private Sub ViewTargetButton_Click(sender As Object, e As RoutedEventArgs) Handles ViewTargetButton.Click
        Try
            Dim supervisorCustomerSpecEdit As New SupervisorCustomerSpecEdit

            Dim customerSpec As New CustomerSpecBL

            customerSpec = customerSpec.GetByPackGrade(Convert.ToDateTime(ToDatePicker.Text).Year, CustomerComboBox.SelectedValue, PackGradeComboBox.SelectedValue)

            supervisorCustomerSpecEdit.CustomerSpec = customerSpec

            supervisorCustomerSpecEdit.ShowDialog()
        Catch ex As Exception
            MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning)
        End Try
    End Sub

    Private Sub ViewReportButton_Click(sender As Object, e As RoutedEventArgs) Handles ViewReportButton.Click
        Try
            Dim lineChartReport As New LineChartReport

            lineChartReport.FromDate = FromDatePicker.Text
            lineChartReport.ToDate = ToDatePicker.Text
            lineChartReport.Customer = CustomerComboBox.SelectedValue
            lineChartReport.PackGrade = PackGradeComboBox.SelectedValue

            lineChartReport.ShowDialog()
        Catch ex As Exception
            MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning)
        End Try
    End Sub

    Private Sub ChageTestCaseInfoButton_Click(sender As Object, e As RoutedEventArgs)
        Try
            'Create new windows.
            Dim staffEditTestCaseInfoMenu As New StaffEditTestCaseMenu


            'Get Test case information from database.
            Dim _testCaseObject As New DegradationTestCase

            _testCaseObject = CaseNumberDataGrid.SelectedItem


            'Send parameter to new destination windows.
            staffEditTestCaseInfoMenu.PdNo = _testCaseObject.Pdno
            staffEditTestCaseInfoMenu.CaseNo = _testCaseObject.CaseNo

            'Open windows.
            staffEditTestCaseInfoMenu.ShowDialog()

            CaseNumberDataGrid.ItemsSource = Nothing
            CaseNumberDataGrid.ItemsSource = _testCaseObject.GetTestCaseByPackGrade(FromDatePicker.Text, ToDatePicker.Text, CustomerComboBox.SelectedValue, PackGradeComboBox.SelectedValue).OrderByDescending(Function(x) x.Ddate).ThenByDescending(Function(x) x.Ttime)

            ExportButton.IsEnabled = False
        Catch ex As Exception
            MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning)
        End Try
    End Sub

    Private Sub DeleteTestCaseButton_Click(sender As Object, e As RoutedEventArgs)
        Try
            If MessageBox.Show("ท่านต้องการลบข้อมูล Test case นี้ใช่หรือไม่?", "Warning", MessageBoxButton.YesNo, MessageBoxImage.Warning) = MessageBoxResult.No Then
                Return
            End If

            Dim testCaseObj As New DegradationTestCase

            testCaseObj = CaseNumberDataGrid.SelectedItem

            testCaseObj.DeleteTestCase(testCaseObj.Crop, testCaseObj.Pdno, testCaseObj.CaseNo)

            MessageBox.Show("ลบข้อมูลสำเร็จ!", "Information", MessageBoxButton.OK, MessageBoxImage.Information)

            CaseNumberDataGrid.ItemsSource = Nothing
            CaseNumberDataGrid.ItemsSource = testCaseObj.GetTestCaseByPackGrade(FromDatePicker.Text, ToDatePicker.Text, CustomerComboBox.SelectedValue, PackGradeComboBox.SelectedValue).OrderByDescending(Function(x) x.Ddate).ThenByDescending(Function(x) x.Ttime)

            ExportButton.IsEnabled = False

            ViewTargetButton.IsEnabled = True

        Catch ex As Exception
            MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning)
        End Try
    End Sub

End Class
