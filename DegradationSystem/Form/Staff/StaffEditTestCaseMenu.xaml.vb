﻿Public Class StaffEditTestCaseMenu

    Private _pdNo As String
    Private _caseNo As Integer

    Public Property CaseNo() As Integer
        Get
            Return _caseNo
        End Get
        Set(ByVal value As Integer)
            _caseNo = value
        End Set
    End Property

    Public Property PdNo() As String
        Get
            Return _pdNo
        End Get
        Set(ByVal value As String)
            _pdNo = value
        End Set
    End Property

    Private Sub EditSizeContentButton_Click(sender As Object, e As RoutedEventArgs) Handles EditSizeContentButton.Click
        Try
            Dim staffEditSizeContent As New StaffEditSizeContent

            staffEditSizeContent.PdNo = _pdNo
            staffEditSizeContent.CaseNo = _caseNo

            staffEditSizeContent.ShowDialog()
        Catch ex As Exception
            MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning)
        End Try
    End Sub

    Private Sub MetroWindow_Loaded(sender As Object, e As RoutedEventArgs)
        Try
            DisplayNameTextBlock.Text = currentUser
        Catch ex As Exception
            MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning)
        End Try
    End Sub

    Private Sub EditTestTimeButton_Click(sender As Object, e As RoutedEventArgs) Handles EditTestTimeButton.Click
        Try
            Dim staffEditTestTime As New StaffEditTestTime

            staffEditTestTime.PdNo = _pdNo
            staffEditTestTime.CaseNo = _caseNo

            staffEditTestTime.Show()
        Catch ex As Exception
            MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning)
        End Try
    End Sub

    Private Sub ChangeCaseNumberButton_Click(sender As Object, e As RoutedEventArgs) Handles ChangeCaseNumberButton.Click
        Try
            Dim staffChangeCaseNo As New ChangeCaseNo

            staffChangeCaseNo.PdNo = _pdNo
            staffChangeCaseNo.CaseNo = _caseNo

            staffChangeCaseNo.ShowDialog()
        Catch ex As Exception
            MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning)
        End Try
    End Sub
End Class
