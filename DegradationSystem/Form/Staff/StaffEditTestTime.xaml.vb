﻿Public Class StaffEditTestTime

    Private _pdNo As String
    Private _caseNo As Integer

    Public Property CaseNo() As Integer
        Get
            Return _caseNo
        End Get
        Set(ByVal value As Integer)
            _caseNo = value
        End Set
    End Property

    Public Property PdNo() As String
        Get
            Return _pdNo
        End Get
        Set(ByVal value As String)
            _pdNo = value
        End Set
    End Property

    Private Sub TestCaseInformationDataBinding()
        Try

            Dim _testCaseObject As New DegradationTestCase

            _testCaseObject = _testCaseObject.GetTestCaseByCase(_pdNo, _caseNo)

            TestCaseCaseNoLabel.Content = _testCaseObject.CaseNo
            TestCaseTestTimeLabel.Content = _testCaseObject.Ttime
            TestCasePackingDateLabel.Content = _testCaseObject.Ddate
            TestCaseProductionNumberLabel.Content = _testCaseObject.Pdno
            TestCaseOver1Label.Content = _testCaseObject.SizeOver1
            TestCaseOver12Label.Content = _testCaseObject.SizeOver12
            TestCaseOver14Label.Content = _testCaseObject.SizeOver14
            TestCaseOver18Label.Content = _testCaseObject.SizeOver18
            TestCasePanLabel.Content = _testCaseObject.SizePan

            HoursTextBox.Text = _testCaseObject.Ttime.Hours
            MinuteTextBox.Text = _testCaseObject.Ttime.Minutes

            If HoursTextBox.Text.Length < 2 Then
                HoursTextBox.Text = "0" & HoursTextBox.Text
            End If

            If MinuteTextBox.Text.Length < 2 Then
                MinuteTextBox.Text = "0" & MinuteTextBox.Text
            End If

        Catch ex As Exception
            MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning)
        End Try
    End Sub

    Private Sub MetroWindow_Loaded(sender As Object, e As RoutedEventArgs)
        Try
            DisplayNameTextBlock.Text = currentUser

            TimeModeLable.Content = "MM"

            HoursTextBox.Background = Brushes.White
            MinuteTextBox.Background = Brushes.LightGreen

            TestCaseInformationDataBinding()
        Catch ex As Exception
            MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning)
        End Try
    End Sub

    Private Sub SaveButton_Click(sender As Object, e As RoutedEventArgs) Handles SaveButton.Click
        Try
            If MessageBox.Show("ท่านต้องการบันทึกข้อมูลที่ได้ทำการแก้ไขนี้ใช่หรือไม่?", "Confirmation", MessageBoxButton.YesNo, MessageBoxImage.Question) = MessageBoxResult.No Then
                Return
            End If

            Dim testCaseObject As New DegradationTestCase

            testCaseObject = testCaseObject.GetTestCaseByCase(_pdNo, _caseNo)

            Dim tTime As TimeSpan = TimeSpan.Parse(HoursTextBox.Text & ":" & MinuteTextBox.Text)

            testCaseObject.ChangeTestTime(testCaseObject.Crop, testCaseObject.Customer, testCaseObject.PackGrade, testCaseObject.Pdno, testCaseObject.CaseNo, tTime, currentUser)

            TestCaseInformationDataBinding()

            MessageBox.Show("บันทึกการแก้ไขข้อมูลสำเร็จ!", "Information", MessageBoxButton.OK, MessageBoxImage.Information)
        Catch ex As Exception
            MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning)
        End Try
    End Sub

    Private Sub AddTimeButton_Click(sender As Object, e As RoutedEventArgs) Handles AddTimeButton.Click
        Try
            If TimeModeLable.Content = "HH" Then

                HoursTextBox.Text = HoursTextBox.Text + 1

                If HoursTextBox.Text.Length < 2 Then
                    HoursTextBox.Text = "0" & HoursTextBox.Text
                End If
            Else

                MinuteTextBox.Text = MinuteTextBox.Text + 10

                If MinuteTextBox.Text.Length < 2 Then
                    MinuteTextBox.Text = "0" & MinuteTextBox.Text
                End If
            End If

            If HoursTextBox.Text > 23 Then
                'MessageBox.Show("ตัวเลขชั่วโมงจะต้องไม่เกิน 23 โปรดลองใหม่อีกครั้ง", "การแจ้งเตือน", MessageBoxButton.OK, MessageBoxImage.Information)
                HoursTextBox.Text = "00"
                Return
            End If

            If MinuteTextBox.Text > 50 Then
                'MessageBox.Show("ตัวเลขนาทีจะต้องไม่เกิน 40 โปรดลองใหม่อีกครั้ง", "การแจ้งเตือน", MessageBoxButton.OK, MessageBoxImage.Information)
                MinuteTextBox.Text = "00"
                Return
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning)
        End Try
    End Sub

    Private Sub MinusTimeButton_Click(sender As Object, e As RoutedEventArgs) Handles MinusTimeButton.Click
        Try
            If TimeModeLable.Content = "HH" Then

                HoursTextBox.Text = HoursTextBox.Text - 1

                If HoursTextBox.Text.Length < 2 Then
                    HoursTextBox.Text = "0" & HoursTextBox.Text
                End If

            Else

                MinuteTextBox.Text = MinuteTextBox.Text - 10

                If MinuteTextBox.Text.Length < 2 Then
                    MinuteTextBox.Text = "0" & MinuteTextBox.Text
                End If
            End If

            If HoursTextBox.Text < 0 Then
                'MessageBox.Show("ตัวเลขชั่วโมงจะต้องไม่น้อยกว่า 0 โปรดลองใหม่อีกครั้ง", "การแจ้งเตือน", MessageBoxButton.OK, MessageBoxImage.Information)
                HoursTextBox.Text = "23"
                Return
            End If

            If MinuteTextBox.Text < 0 Then
                'MessageBox.Show("ตัวเลขชั่วโมงจะต้องไม่น้อยกว่า 0 โปรดลองใหม่อีกครั้ง", "การแจ้งเตือน", MessageBoxButton.OK, MessageBoxImage.Information)
                MinuteTextBox.Text = "50"
                Return
            End If


        Catch ex As Exception
            MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning)
        End Try
    End Sub

    Private Sub HoursTextBox_GotFocus(sender As Object, e As RoutedEventArgs) Handles HoursTextBox.GotFocus
        Try
            TimeModeLable.Content = "HH"

            HoursTextBox.Background = Brushes.LightGreen
            MinuteTextBox.Background = Brushes.White
        Catch ex As Exception
            MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning)
        End Try
    End Sub

    Private Sub MinuteTextBox_GotFocus(sender As Object, e As RoutedEventArgs) Handles MinuteTextBox.GotFocus
        Try
            TimeModeLable.Content = "MM"

            HoursTextBox.Background = Brushes.White
            MinuteTextBox.Background = Brushes.LightGreen
        Catch ex As Exception
            MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning)
        End Try
    End Sub

End Class
