﻿Imports Microsoft.Reporting.WinForms

Public Class LineChartReport
    Private _fromDate As Date
    Private _toDate As Date
    Private _customer As String
    Private _packGrade As String

    Public Property PackGrade() As String
        Get
            Return _packGrade
        End Get
        Set(ByVal value As String)
            _packGrade = value
        End Set
    End Property

    Public Property Customer() As String
        Get
            Return _customer
        End Get
        Set(ByVal value As String)
            _customer = value
        End Set
    End Property

    Public Property ToDate() As Date
        Get
            Return _toDate
        End Get
        Set(ByVal value As Date)
            _toDate = value
        End Set
    End Property

    Public Property FromDate() As Date
        Get
            Return _fromDate
        End Get
        Set(ByVal value As Date)
            _fromDate = value
        End Set
    End Property


    Private Sub Window_Loaded(sender As Object, e As RoutedEventArgs)
        Try
            DegradationReportViewer.Reset()

            Dim reportDataSource As New ReportDataSource

            Dim sp_Sieve_Report_DegradationTestCaseDetailsDataTable As New StecDBMSDataSet.sp_Sieve_Report_DegradationTestCaseDetailsDataTable

            Dim sp_Sieve_Report_DegradationTestCaseDetailsTableAdapter As New StecDBMSDataSetTableAdapters.sp_Sieve_Report_DegradationTestCaseDetailsTableAdapter

            sp_Sieve_Report_DegradationTestCaseDetailsTableAdapter.Fill(sp_Sieve_Report_DegradationTestCaseDetailsDataTable, _customer, _packGrade, _fromDate, _toDate)

            reportDataSource.Name = "sp_Sieve_Report_DegradationTestCaseDetailsDataSet"
            reportDataSource.Value = sp_Sieve_Report_DegradationTestCaseDetailsDataTable

            DegradationReportViewer.LocalReport.DataSources.Add(reportDataSource)

            DegradationReportViewer.LocalReport.ReportEmbeddedResource = "DegradationSystem.RPTDEG01.rdlc"

            DegradationReportViewer.RefreshReport()
        Catch ex As Exception
            MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning)
        End Try
    End Sub
End Class
